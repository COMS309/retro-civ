class hex{
    
    constructor(){
        this.type=Math.floor(Math.random()*5);
        if(this.type==0){
            this.name="desert";
            this.color="yellow";
            this.distance=1;
        }else if(this.type==1){
            this.name="grassland";
            this.color="lawngreen";
            this.distance=1;
        }else if(this.type==2){
            this.name="forest";
            this.color="forestgreen";
            this.distance=2;
        }else if(this.type==3){
            this.name="mountain";
            this.color="mediumpurple";
            this.distance=5;
        }else{
            this.name="water";
            this.color="dodgerblue";
            this.distance=600;
        }
        this.units=[];
        this.building=[];
    }
    getName(){
        return this.name;
    }
    getColor(){
        return this.color;
    }
    getDistance(){
        return this.distance;
    }
    addUnit(unit){
        this.units.push(unit);
    }
    
}


class unit{
    constructor(hex,board){
        this.location=hex;
        this.board=board;
        this.range=4;
        hex.addUnit.this;
    }
    getBoard(){
        return this.board;
    }
    
}
var width = 1000;
var height = 1000;

class board{
    constructor(radius){
        this.r=radius;
        //this.hexNum=3*this.r*(this.r-1)+1;
        this.Hexes=[];
        for(var i=0;i<2*this.r-1;i++){
            for(var j=i;j<this.r-1;j++){
                this.Hexes.push(null);
            }
            for(var k=0;k<i && i<this.r;k++){
                this.Hexes.push(new hex());
            }
            for(var l=0;l<this.r;l++){
                this.Hexes.push(new hex());
            }
            for(var m=2*this.r-2;m>i && i>this.r-1;m--){
                this.Hexes.push(new hex());
            }
            for(var n=i;n>this.r-1;n--){
                this.Hexes.push(null);
            }
        }
        this.size=50;
        this.w=this.size*2;
        this.h=Math.trunc(Math.sqrt(3)/2*this.w);
    }
    
    getCenter(hex){
        var x=this.getX(hex)-this.r+1;
        var y=this.getY(hex)-this.r+1;
        var dx=width/2+x*3/4*this.w;
        var dy=height/2+y*this.h+x*1/2*this.h;
        return [dx,dy];
    }
    
    getVertices(hex){
        var center=this.getCenter(hex);
        var p1=[center[0]-this.w/2,center[1]];
        var p2=[center[0]-this.w/4,center[1]+this.h/2];
        var p3=[center[0]+this.w/4,center[1]+this.h/2];
        var p4=[center[0]+this.w/2,center[1]];
        var p5=[center[0]+this.w/4,center[1]-this.h/2];
        var p6=[center[0]-this.w/4,center[1]-this.h/2];
            return [p1,p2,p3,p4,p5,p6];
    }
    
    
   
    
    draw(){
         var svg = d3.select('body').append('svg')
    .attr('width', width)
    .attr('height', height);
    var unitspots=[];
    for(var i=0;i<this.Hexes.length;i++){
        if(this.Hexes[i]!=null){
            var hex=this.Hexes[i];
            if(hex.units.length!=0){
                unitspots.push(hex);
            }
        var elements = svg.append("polyline") //draw elements
                .attr("points",this.getVertices(hex))
                .style("fill", hex.getColor())
                .attr('class', 'cell')
                .attr('id',i)
                .attr('stroke','black')
                .attr('stroke-width','3px');
    }
    }
    for(var i=0;i<unitspots.length;i++){
        var center=this.getCenter(unitspots[i]);
        var elements = svg.append("circle") //draw elements
                .attr("cx",center[0])
                .attr("cy",center[1])
                .attr("r",this.size/2)
                .style("fill", "orange")
                .attr('class','cell')
                .attr('class', 'soldier')
                .attr('id',i)
                .attr('stroke','black')
                .attr('stroke-width','3px'); 
    }
    
    $("#svg").ready(function(){    
            $(".cell").on('mouseover',function(){
                this.setAttribute("stroke-width","8px");
                
                
                
            });
            $(".soldier").on('click',function(){
                var Overlay=this.id//.findDistance(this.hex,this.range);
                this.drawOverlay(this,Overlay);
                
                
                
            });
            $(".cell").on('mouseout',function(){
                this.setAttribute("stroke-width","3px");
              
                
            });
            
    
    });
    
    }
    
    drawOverlay(unit,overlay){
        this.unit=unit;
        this.overlay=overlay;
        for(var i=0;i<overlay.length;i++){
             if(overlay[i]!=null){
            var hex=overlay[i];
            
        var elements = svg.append("polyline") //draw elements
                .attr("points",this.getVertices(hex))
                .style("fill", "white")
                .style("opacity","0.3")
                .attr('class', 'cover')
                .attr('id',i)
                
    }
        }
    }
    
    getX(hex){
       return Math.trunc(this.Hexes.indexOf(hex)/(2*this.r-1)); 
    }
    getY(hex){
       return this.Hexes.indexOf(hex)%(2*this.r-1); 
    }
    getHex(x,y){
        return this.Hexes[x*(2*this.r-1)+y];
    }
    getHexByIndex(index){
        return this.Hexes[index];
    }
    
    getNeighbors(hex){
        var x=this.getX(hex);
        var y=this.getY(hex);
        var neighbors=[];
        if(this.getHex(x-1,y)!=null){
            neighbors.push(this.getHex(x-1,y));
        }
        if(this.getHex(x+1,y-1)!=null){
            neighbors.push(this.getHex(x+1,y-1));
        }
        if(this.getHex(x,y+1)!=null){
            neighbors.push(this.getHex(x,y+1));
        }
        if(this.getHex(x,y-1)!=null){
            neighbors.push(this.getHex(x,y+1));
        }
        if(this.getHex(x+1,y)!=null){
            neighbors.push(this.getHex(x+1,y));
        }
        if(this.getHex(x-1,y+1)!=null){
            neighbors.push(this.getHex(x-1,y+1));
        }
        return neighbors;
    }
    
    findDistance(hex,range){
        this.range=range;
    var checked=[];
    for(var i=0;i<Math.pow(2*this.r-1,2);i++){
        checked.push(null);
    }
    this.distance(checked,0,hex,range);
    return checked;
    }
    distance(checked,d,hex,range){
        this.range=range;
        var toCheck=this.getNeighbors(hex);
        var index=0;
        var newD=0;
        var nextCheck=[];
        for(var i=0;i<toCheck.length;i++){
            index=this.Hexes.indexOf(toCheck[i]);
            newD= d+toCheck[i].getDistance();
            if(checked[index]!=null){
                if(checked[index][0]>newD)
                {
                    checked[index]=[newD,hex];
                    nextCheck.push(toCheck[i]);
                }else{
                    return;
                }
            }
                checked[index]=[newD,hex];
                nextCheck.push(toCheck[i]);
            
        }
        for(var i=0;i<nextCheck.length;i++){
            index=this.Hexes.indexOf(nextCheck[i]);
            if(checked[index][0]<range)
            {
                this.distance(checked,checked[index][0],nextCheck[i],range);
            }
        }
        
    }
    
}







var scene=new board(5);
/*console.log(scene.getCenter(scene.Hexes[2]));
console.log(scene.getCenter(scene.Hexes[7]));
console.log(scene.getCenter(scene.Hexes[12]));
console.log(scene.getNeighbors(scene.Hexes[12]));
console.log(scene.findDistance(scene.Hexes[2],4));*/
var Clark=new unit(scene.Hexes[7],scene)
scene.Hexes[7].addUnit(Clark);
scene.draw();