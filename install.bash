#!/bin/bash
echo "Install gulp: npm install --g gulp-cli"
npm install -g gulp-cli
echo "Install tsd manager: npm install -g tsd"
npm install -g tsd
echo "Install nodemon: npm install -g nodemon"
npm install -g nodemon
