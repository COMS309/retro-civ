New User Setup
==============

Web development can use a complex array of frameworks and tools that need to be interconnected.
To increase the approachability to frameworks such as node.js, angular.js, and grunt.js, this platform was
developed which includes the resources necessary to get started working on web applications

This guide walks the user through installing prerequisite software and seting up the build toolchain.

Install Prerequisite Software
=============================

Version Contrl
--------------

git: https://msysgit.github.io/  
NOTE: MAKE SURE TO UNCHECK SHELL/WINDOWS EXPLORER INTEGRATION when installing git. IMHO tortoisegit provides a better shell

If you'd like a gui for git I suggest  
tortoisegit: https://code.google.com/p/tortoisegit/

IDE's
-----

Prefered  
VSCode: https://code.visualstudio.com/  
NOTE: When Installing check both boxes mentioning "Open With Code"

Alternative  
Notepad++: https://notepad-plus-plus.org/

Toolchain
---------

nodejs: https://nodejs.org/  
The latest version ~5.4 currently works

ruby: https://rubyinstaller.org  
version 2.2.x works

PATH Setup
==========
		
Check if the following programs are in the path
        
### Nodejs    
CMD: node --version  
PATH: %PROGRAMFILES%/nodejs;%APPDATA%/npm;

### git  
CMD: git --version
PATH: %PROGRAMFILES%/Git/bin;

### ruby

~~~bash  
ruby -v
~~~
PATH C:/Ruby22-x64/bin;

### Instructions to add things to path
* goto: ControlPanel/System and Security/System/advanced system settings/Advanced/Environment Variables/
* under user variables edit or create the Path Variable.  add the correct path to the end of variable value:
* preceed with semicolon if another path is before it and doesn't have a semicolon
* Example:  if path was 'C:/python27' it should be 'C:/python27;%PROGRAMFILES%/nodejs;%APPDATA%/npm;' when you are done"
* Restart cmd prompt
* run check again (EX: node --version)

### Troubleshooting
The installer might not install to %PROGRAMFILES% but instead install to %PROGRAMFILES(x86)%  
Navigate to find the actual location and update the path accordingly


Toolchain Setup
===============

Run the folling commands to install node and gem packages

CMD:
* npm install -g npm@latest             /*updates Node Packet Manager to the latest version (for software dependencies)*/
* gem install sass                      /*sass compiler for web styling*/
* npm install -g bower grunt-cli        /*Automation tools*/
* bower --version && grunt --version"   /*Verify they are installed*/

REPO SETUP
==========

Navigate to the folder above where you want the code to be in  
Right Click on Git Clone  
Paste the HTTP link to this repository (something.git)  
NOTE: This link should be online  
Click OK  
You might have to log in  

navigate to the repository folder in the command line  
CMD:  
* npm install  
* bower install  

Navigate to the repository folder in windows explorer  
Right click on the repo folder and select 'Open with Code'  

In VSCODE replace the files in client/bower_componenets with those in client/bower_replace

Running the Website
===================

Navigate to the repo folder in the command line and run the following  
CMD: grunt serve

This will compile the code, launch the webserver, and open a browser window  
Any changes to the code will update the server and be visible after refreshing the browser

To close the server run the following  
CMD: CTRL+C

PROMPT: Stop the Batch Process? Y/N  
RESPONSE: Y