'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.REDUCERS = undefined;

var _redux = require('redux');

var _action_type = require('./action_type.js');

var _hex = require('./sub/hex.js');

var _civ = require('./sub/civ.js');

var _turn = require('./sub/turn.js');

var _tech_tree = require('./sub/tech_tree.js');

var name = function name() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? "game" : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case "SET GAME.name":
            return action.name;
        default:
            return state;
    }
};

var init = function init() {
    return {
        name: name(undefined, { type: null }),
        turn: (0, _turn.turn)(undefined, { type: null }),
        hexes: (0, _hex.hexes)(undefined, { type: null }),
        civs: (0, _civ.civs)(undefined, { type: null }),
        TECH_TREE: _tech_tree.TECH_TREE
    };
};

var state_manager = function state_manager() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? init() : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.RESET:
            return init();
        default:
            return {
                name: name(state.name, action),
                turn: (0, _turn.turn)(state.turn, action, state.civs ? state.civs.length : 0),
                hexes: (0, _hex.hexes)(state.hexes, action),
                civs: (0, _civ.civs)(state.civs, action),
                TECH_TREE: _tech_tree.TECH_TREE
            };
    }
};

var REDUCERS = exports.REDUCERS = {
    GAME: state_manager
};