"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.expectFunction = exports.flatten = exports.isString = exports.idFree = exports.idLookup = exports.getIndex = undefined;

var _error = require("./error.js");

var _chai = require("chai");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var getIndex = exports.getIndex = function getIndex(list, val) {
    if (!list) list = [];
    var name_list = list.map(function (item) {
        return item.name;
    });
    return name_list.indexOf(val);
};

var idLookup = exports.idLookup = function idLookup(list, val, err_msg) {
    var index = getIndex(list, val);
    if (index === -1) {
        throw new _error.LookupError(err_msg + " > \"" + val + "\" not found in list " + list);
    }
    return list[index];
};
var idFree = exports.idFree = function idFree(list, val, err_msg) {
    var index = getIndex(list, val);
    if (index !== -1) {
        throw new _error.ExistsError(err_msg + " > \"" + val + "\" found in list " + list);
    }
    return;
};

var isString = exports.isString = function isString(obj) {
    if (typeof obj === "string" || obj instanceof String) {
        return true;
    }
    return false;
};

var flatten = exports.flatten = function flatten(obj) {
    if (isString(obj)) {
        return obj;
    }
    var arr = [];
    Object.keys(obj).map(function (item) {
        var f = flatten(obj[item]);
        if (isString(f)) {
            arr = [].concat(_toConsumableArray(arr), [f]);
        } else {
            arr = [].concat(_toConsumableArray(arr), _toConsumableArray(f));
        }
    });
    return arr;
};

var expectFunction = exports.expectFunction = function expectFunction(fn) {
    return {
        to: {
            throw: function _throw(err) {
                var name = "";
                try {
                    fn();
                } catch (e) {
                    name = e.name;
                }
                (0, _chai.expect)(name).to.equal(err.name);
            },
            not: {
                throw: function _throw(err) {
                    var name = "";
                    try {
                        fn();
                    } catch (e) {
                        name = e.name;
                    }
                    (0, _chai.expect)(name).to.not.equal(err.name);
                }
            }
        }
    };
};