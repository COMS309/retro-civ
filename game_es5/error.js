"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DeprecatedActionError = exports.DeprecatedActionError = function (_Error) {
    _inherits(DeprecatedActionError, _Error);

    function DeprecatedActionError() {
        var action = arguments.length <= 0 || arguments[0] === undefined ? "action" : arguments[0];
        var ref = arguments[1];
        var alt = arguments[2];

        _classCallCheck(this, DeprecatedActionError);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DeprecatedActionError).call(this));

        _this.name = "DeprecatedActionError";
        _this.message = "Deprecated Action: " + action + " is used" + (ref ? " in " + ref : "") + ";" + (alt ? " Replace With:" + alt : "");
        return _this;
    }

    return DeprecatedActionError;
}(Error);

var LookupError = exports.LookupError = function (_Error2) {
    _inherits(LookupError, _Error2);

    function LookupError(msg) {
        _classCallCheck(this, LookupError);

        var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(LookupError).call(this));

        _this2.name = "LookupError";
        _this2.mesage = msg;
        return _this2;
    }

    return LookupError;
}(Error);

var ExistsError = exports.ExistsError = function (_Error3) {
    _inherits(ExistsError, _Error3);

    function ExistsError(msg) {
        _classCallCheck(this, ExistsError);

        var _this3 = _possibleConstructorReturn(this, Object.getPrototypeOf(ExistsError).call(this));

        _this3.name = "ExistsError";
        _this3.mesage = msg;
        return _this3;
    }

    return ExistsError;
}(Error);

var CivilizationMismatchError = exports.CivilizationMismatchError = function (_Error4) {
    _inherits(CivilizationMismatchError, _Error4);

    function CivilizationMismatchError(msg) {
        _classCallCheck(this, CivilizationMismatchError);

        var _this4 = _possibleConstructorReturn(this, Object.getPrototypeOf(CivilizationMismatchError).call(this));

        _this4.name = "CivilizationMismatchError";
        _this4.mesage = msg;
        return _this4;
    }

    return CivilizationMismatchError;
}(Error);

var ImplicitActionError = exports.ImplicitActionError = function (_Error5) {
    _inherits(ImplicitActionError, _Error5);

    function ImplicitActionError(action, ref) {
        _classCallCheck(this, ImplicitActionError);

        var _this5 = _possibleConstructorReturn(this, Object.getPrototypeOf(ImplicitActionError).call(this, "Must Explicitly Declare the behavior of \"" + action + "\" in " + ref));

        _this5.name = "ImplicitActionError";
        _this5.mesage = "Must Explicitly Declare the behavior of \"" + action + "\" in " + ref;
        return _this5;
    }

    return ImplicitActionError;
}(Error);