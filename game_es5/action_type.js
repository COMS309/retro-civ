"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var ACTION_TYPE = exports.ACTION_TYPE = {
    GAME: {
        NEW: "NEW      GAME",
        TURN: {
            NEXT: "NEXT     GAME.turn"
        }
    },

    CIV: {
        ADD: "ADD       GAME.civ",
        REMOVE: "REMOVE    GAME.civ"
    },
    HEX: {
        ADD: "ADD       GAME.hex",
        REMOVE: "REMOVE    GAME.hex"
    },
    CITY: {
        BUILD: "BUILD     GAME.city",
        UPGRADE: "UPGRADE   GAME.city",
        DEMOLISH: "DEMOLISH  GAME.city",
        ATTACK: "ATTACK    GAME.city",
        HEAL: "HEAL      GAME.city"
    },
    BUILDING: {
        BUILD: "BUILD     GAME.building",
        UPGRADE: "UPGRADE   GAME.building",
        DEMOLISH: "DEMOLISH  GAME.building",
        ATTACK: "ATTACK    GAME.building",
        HEAL: "HEAL      GAME.building"
    },
    UNIT: {
        BUILD: "BUILD     GAME.unit",
        UPGRADE: "UPGRADE   GAME.unit",
        DEMOLISH: "DEMOLISH  GAME.unit",
        MOVE: "MOVE      GAME.unit",
        TRANSFORM: "TRANSFORM GAME.unit",
        ATTACK: "ATTACK    GAME.unit",
        HEAL: "HEAL      GAME.unit"
    },
    TECH: {
        SET: "SET       GAME.tech.level"
    },
    NAME: {
        SET: "SET       GAME.name"
    },
    RESET: "RESET     GAME"
};