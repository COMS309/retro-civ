'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.building = exports.buildings = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _h_functions = require('./../h_functions.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//CREATORS
var CREATORS = exports.CREATORS = {
    build: function build(hex, civ, building, builder_data) {
        return {
            type: builder_data.build,
            hex: hex,
            civ: civ,
            building: building,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    upgrade: function upgrade(obj_data, builder_data) {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            building: obj_data.name,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    demolish: function demolish(obj_data) {
        return {
            type: _action_type.ACTION_TYPE.BUILDING.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            building: obj_data.name,
            stats: obj_data.stats
        };
    },
    attack: function attack(attack_unit, target_building) {
        return {
            type: _action_type.ACTION_TYPE.BUILDING.ATTACK,
            hex: target_building.hex,
            civ: target_building.civ,
            building: target_building.name,
            damage: attack_unit.stats.attack.damage
        };
    },
    heal: function heal(obj_data) {
        return {
            type: _action_type.ACTION_TYPE.BUILDING.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            building: obj_data.name,
            heal: obj_data.stats.hp.max
        };
    }
};

//REDUCERS
var buildings = exports.buildings = function buildings() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var action = arguments[1];

    var index = -1;
    switch (action.type) {
        case _action_type.ACTION_TYPE.BUILDING.BUILD:
            return [].concat(_toConsumableArray(state), [building(undefined, action)]);
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
            index = (0, _h_functions.getIndex)(state, action.building);
            return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:
            index = (0, _h_functions.getIndex)(state, action.building);
            return [].concat(_toConsumableArray(state.slice(0, index)), [building(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
            index = (0, _h_functions.getIndex)(state, action.building);
            var stats = state[index].stats;
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
            }
            return [].concat(_toConsumableArray(state.slice(0, index)), [building(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        default:
            return state;
    }
};
var building = exports.building = function building() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.BUILDING.BUILD:
            return {
                name: action.building,
                civ: action.civ,
                hex: action.hex,
                obj_type: 'building',
                builder: action.builder,
                level: action.level,
                stats: action.stats
            };
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
            return null;
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
            return Object.assign({}, state, {
                stats: action.stats,
                level: action.level
            });
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
            if (state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0) return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: hp
                    })
                })
            });
        case _action_type.ACTION_TYPE.BUILDING.HEAL:
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: state.stats.hp.max
                    })
                })
            });
        default:
            return state;
    }
};