'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.TECH_TREE = undefined;

var _base = require('./TECH_TREE/base.js');

var _military = require('./TECH_TREE/military.js');

var _city = require('./TECH_TREE/city.js');

var _manufacturing = require('./TECH_TREE/manufacturing.js');

//**NOTE: build cost is usually twice as much as the production or research income**
var TECH_TREE = exports.TECH_TREE = {
    base: _base.base,
    military: _military.military,
    city: _city.city,
    manufacturing: _manufacturing.manufacturing,
    commerce: [],
    research: [],
    colonization: []
};