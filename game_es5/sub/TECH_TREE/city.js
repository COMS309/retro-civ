'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.city = undefined;

var _h_functions = require('./h_functions.js');

var _cities = require('./obj/cities.js');

var level4 = (0, _h_functions.level)({
    name: "King",
    description: "The most powerful man on the land needs an intimidating city to rule from." + " The King is able to create capital cities that spark the envy of his rivals.",
    cost: 6000,
    cities: [_cities.cities.capital.lv0, _cities.cities.metropolis.lv1, _cities.cities.city.lv2, _cities.cities.town.lv3, _cities.cities.village.lv5],
    buildings: [],
    units: []
});

var level3 = (0, _h_functions.level)({
    name: "Prince",
    description: "At the height of power the Prince is able to create grand metropolises." + " These large city centers push productivity to unseen levels.",
    cost: 3000,
    cities: [_cities.cities.metropolis.lv0, _cities.cities.city.lv1, _cities.cities.town.lv2, _cities.cities.village.lv4],
    buildings: [],
    units: []
});

var level2 = (0, _h_functions.level)({
    name: "Baron",
    description: "A booming town needs a leader to guide them. Thats where the Baron steps in." + " With their leadership, the Baron is able to sustain successful cities.",
    cost: 1500,
    cities: [_cities.cities.city.lv0, _cities.cities.town.lv1, _cities.cities.village.lv3],
    buildings: [],
    units: []
});

var level1 = (0, _h_functions.level)({
    name: "Minister",
    description: "A civilization needs to grow somehow. The Minister is able to make" + " towns to provide support for larger communities.",
    cost: 1000,
    cities: [_cities.cities.town.lv0, _cities.cities.village.lv2],
    buildings: [],
    units: []
});

var level0 = (0, _h_functions.level)({
    name: "Settler",
    description: "Stating somewhere is difficult, but the Settler has everything figured out." + " A basic village can be built as the start of a grand civilization.",
    cost: 500,
    cities: [_cities.cities.village.lv1],
    buildings: [],
    units: []
});

var city = exports.city = [level0, level1, level2, level3, level4];