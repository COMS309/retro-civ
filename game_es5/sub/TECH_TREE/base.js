'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.base = undefined;

var _cities = require('./obj/cities.js');

var _buildings = require('./obj/buildings.js');

var _units = require('./obj/units.js');

var base = exports.base = {
    cities: [_cities.cities.village.lv0],
    buildings: [_buildings.buildings.library.lv0],
    units: [_units.units.soldier.lv0]
};