'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.manufacturing = undefined;

var _h_functions = require('./h_functions.js');

var _buildings = require('./obj/buildings.js');

var level4 = (0, _h_functions.level)({
    name: "Innovator",
    description: "The Innovator is a thinker unlike any other who's ingenuity brings architectural" + " dominance to his land. As a testament to the kingdom, monuments can be constructed to rally" + " the population.",
    cost: 6000,
    cities: [],
    buildings: [_buildings.buildings.monument.lv0, _buildings.buildings.port.lv2, _buildings.buildings.treasury.lv3, _buildings.buildings.barraks.lv3, _buildings.buildings.mine.lv4, _buildings.buildings.mill.lv4, _buildings.buildings.farm.lv4, _buildings.buildings.library.lv4],
    units: []
});

var level3 = (0, _h_functions.level)({
    name: "Architect",
    description: "With their cunning intellect, the Architect uses their knowledge of" + " building to create structures much mightier than any other in the land.",
    cost: 3000,
    cities: [],
    buildings: [_buildings.buildings.port.lv1, _buildings.buildings.treasury.lv2, _buildings.buildings.barraks.lv2, _buildings.buildings.mine.lv3, _buildings.buildings.mill.lv3, _buildings.buildings.farm.lv3, _buildings.buildings.library.lv3],
    units: []
});

var level2 = (0, _h_functions.level)({
    name: "Forger",
    description: "Bodies of water have many unclaimed resources at their disposal." + " The Forger is able to create ports to tap into these unclaimed treasures.",
    cost: 1500,
    cities: [],
    buildings: [_buildings.buildings.port.lv0, _buildings.buildings.treasury.lv1, _buildings.buildings.barraks.lv1, _buildings.buildings.mine.lv2, _buildings.buildings.mill.lv2, _buildings.buildings.farm.lv2, _buildings.buildings.library.lv2],
    units: []
});

var level1 = (0, _h_functions.level)({
    name: "Mason",
    description: "A city needs a way to fund and defend itself. The Mason paves way" + " for the creation of barracks and treasuries.",
    cost: 1000,
    cities: [],
    buildings: [_buildings.buildings.treasury.lv0, _buildings.buildings.barraks.lv0, _buildings.buildings.mine.lv1, _buildings.buildings.mill.lv1, _buildings.buildings.farm.lv1, _buildings.buildings.library.lv1],
    units: []
});

var level0 = (0, _h_functions.level)({
    name: "Craftsman",
    description: "The Craftsman makes sure his city has all its basic needs. Using" + " ,mines, farmland, and mills to collect resources, and libraries for" + " extensive research, the Craftsman sets their city on the path of success.",
    cost: 500,
    cities: [],
    buildings: [_buildings.buildings.mine.lv0, _buildings.buildings.mill.lv0, _buildings.buildings.farm.lv0],
    units: []
});

var manufacturing = exports.manufacturing = [level0, level1, level2, level3, level4];