'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.military = undefined;

var _h_functions = require('./h_functions.js');

var _units = require('./obj/units.js');

var level4 = (0, _h_functions.level)({
    name: "General",
    description: "As the highest rank on the battlefield, the General deserves fear and respect." + " With the Warlord an army becomes nothing less than unstoppable.",
    cost: 6000,
    cities: [],
    buildings: [],
    units: [_units.units.warlord.lv0, _units.units.horseman.lv1, _units.units.archer.lv2, _units.units.soldier.lv3, _units.units.freelancer.lv4]
});

var level3 = (0, _h_functions.level)({
    name: "Colonel",
    description: "Speed is key. The Colonel can use horseback men to strike quickly on" + " ill-prepared opponents.",
    cost: 3000,
    cities: [],
    buildings: [],
    units: [_units.units.horseman.lv0, _units.units.archer.lv1, _units.units.soldier.lv2, _units.units.freelancer.lv3]
});

var level2 = (0, _h_functions.level)({
    name: "Commander",
    description: "Traditional hand on hand combat isn't the only way to win a fight. The " + " Commander is able to call upon archers to clear dangers from a safe distance.",
    cost: 1500,
    cities: [],
    buildings: [],
    units: [_units.units.archer.lv0, _units.units.soldier.lv1, _units.units.freelancer.lv2]
});

var level1 = (0, _h_functions.level)({
    name: "Major",
    description: "The Major has a command on the battlefield, able to rally his loyal troops. " + "Now well trained and equiped soldiers are at the ready.",
    cost: 1000,
    cities: [],
    buildings: [],
    units: [_units.units.soldier.lv0, _units.units.freelancer.lv1]
});

var level0 = (0, _h_functions.level)({
    name: "Captain",
    description: "The lowest ranking military leader, the Captain sets the stage for " + "conquest and capture of foreign lands with freelance soldiers.",
    cost: 500,
    cities: [],
    buildings: [],
    units: [_units.units.freelancer.lv0]
});

var military = exports.military = [level0, level1, level2, level3, level4];