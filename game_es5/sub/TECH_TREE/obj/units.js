'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.units = undefined;

var _archer = require('./units/archer.js');

var _freelancer = require('./units/freelancer.js');

var _horseman = require('./units/horseman.js');

var _soldier = require('./units/soldier.js');

var _warlord = require('./units/warlord.js');

var units = exports.units = {
    archer: _archer.archer,
    freelancer: _freelancer.freelancer,
    horseman: _horseman.horseman,
    soldier: _soldier.soldier,
    warlord: _warlord.warlord
};