'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.cities = undefined;

var _capital = require('./cities/capital.js');

var _city = require('./cities/city.js');

var _metropolis = require('./cities/metropolis.js');

var _town = require('./cities/town.js');

var _village = require('./cities/village.js');

var cities = exports.cities = {
    capital: _capital.capital,
    city: _city.city,
    metropolis: _metropolis.metropolis,
    town: _town.town,
    village: _village.village
};