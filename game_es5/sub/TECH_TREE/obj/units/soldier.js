'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.soldier = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 1,
    speed: 2,
    attack: { range: 1, damage: 50, accuracy: 0.5 },
    defense: { block: 25, accuracy: 0.5 },
    hp: { max: 100, current: 100 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 200
},
//lv1
{
    range: 2,
    speed: 2,
    attack: { range: 1, damage: 40, accuracy: 0.5 },
    defense: { block: 25, accuracy: 0.5 },
    hp: { max: 50, current: 50 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 120
},
//lv2
{
    range: 2,
    speed: 2,
    attack: { range: 1, damage: 60, accuracy: 0.5 },
    defense: { block: 38, accuracy: 0.5 },
    hp: { max: 75, current: 75 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 160
},
//lv3
{
    range: 2,
    speed: 2,
    attack: { range: 1, damage: 75, accuracy: 0.5 },
    defense: { block: 50, accuracy: 0.5 },
    hp: { max: 100, current: 100 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 200
},
//lv4
{
    range: 2,
    speed: 2,
    attack: { range: 1, damage: 90, accuracy: 0.5 },
    defense: { block: 63, accuracy: 0.5 },
    hp: { max: 125, current: 125 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 240
}];

var soldier = exports.soldier = (0, _h_functions.obj_generate)("unit", "soldier", 5, _action_type.ACTION_TYPE.UNIT, stats);