'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.warlord = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 1,
    speed: 1,
    attack: { range: 2, damage: 200, accuracy: 0.5 },
    defense: { block: 300, accuracy: 0.25 },
    hp: { max: 300, current: 300 },
    abilities: { mountaineering: false },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 500
}];

var warlord = exports.warlord = (0, _h_functions.obj_generate)("unit", "warlord", 1, _action_type.ACTION_TYPE.UNIT, stats);