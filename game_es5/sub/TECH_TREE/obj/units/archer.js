'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.archer = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 2,
    speed: 2,
    attack: { range: 3, damage: 60, accuracy: 0.5 },
    defense: { block: 30, accuracy: 0.5 },
    hp: { max: 60, current: 60 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 150
},
//lv1
{
    range: 2,
    speed: 2,
    attack: { range: 3, damage: 75, accuracy: 0.5 },
    defense: { block: 38, accuracy: 0.5 },
    hp: { max: 75, current: 75 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 200
},
//lv2
{
    range: 2,
    speed: 2,
    attack: { range: 3, damage: 90, accuracy: 0.5 },
    defense: { block: 45, accuracy: 0.5 },
    hp: { max: 90, current: 90 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 250
}];

var archer = exports.archer = (0, _h_functions.obj_generate)("unit", "archer", 3, _action_type.ACTION_TYPE.UNIT, stats);