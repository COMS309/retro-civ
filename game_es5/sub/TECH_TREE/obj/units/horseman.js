'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.horseman = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 3,
    speed: 4,
    attack: { range: 1, damage: 75, accuracy: 0.5 },
    defense: { block: 50, accuracy: 0.5 },
    hp: { max: 100, current: 100 },
    abilities: { mountaineering: false },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 250
},
//lv1
{
    range: 3,
    speed: 4,
    attack: { range: 1, damage: 90, accuracy: 0.5 },
    defense: { block: 63, accuracy: 0.5 },
    hp: { max: 125, current: 125 },
    abilities: { mountaineering: false },
    production: 0,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 300
}];

var horseman = exports.horseman = (0, _h_functions.obj_generate)("unit", "horseman", 2, _action_type.ACTION_TYPE.UNIT, stats);