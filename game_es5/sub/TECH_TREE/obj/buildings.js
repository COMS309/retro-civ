'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.buildings = undefined;

var _barraks = require('./buildings/barraks.js');

var _farm = require('./buildings/farm.js');

var _library = require('./buildings/library.js');

var _mill = require('./buildings/mill.js');

var _mine = require('./buildings/mine.js');

var _monument = require('./buildings/monument.js');

var _port = require('./buildings/port.js');

var _treasury = require('./buildings/treasury.js');

var buildings = exports.buildings = {
    barraks: _barraks.barraks,
    farm: _farm.farm,
    library: _library.library,
    mill: _mill.mill,
    mine: _mine.mine,
    monument: _monument.monument,
    port: _port.port,
    treasury: _treasury.treasury
};