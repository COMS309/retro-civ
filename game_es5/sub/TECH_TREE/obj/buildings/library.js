'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.library = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 20, accuracy: 0.5 },
    hp: { max: 40, current: 40 },
    abilities: { mountaineering: false },
    production: 0,
    research: 50,
    population: 1,
    economy: 1,
    build_remaining: 100
}, {
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 40, accuracy: 0.5 },
    hp: { max: 80, current: 80 },
    abilities: { mountaineering: false },
    production: 0,
    research: 100,
    population: 1,
    economy: 1,
    build_remaining: 200
},
//lv2: +20 hp, +50 r/p, +100 build
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 60, accuracy: 0.5 },
    hp: { max: 120, current: 120 },
    abilities: { mountaineering: false },
    production: 0,
    research: 150,
    population: 1,
    economy: 1,
    build_remaining: 300
},
//lv3: +20 hp, +50 r/p, +100 build
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 80, accuracy: 0.5 },
    hp: { max: 160, current: 160 },
    abilities: { mountaineering: false },
    production: 0,
    research: 200,
    population: 1,
    economy: 1,
    build_remaining: 400
},
//lv4: +20 hp, +50 r/p, +100 build
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 100, accuracy: 0.5 },
    hp: { max: 200, current: 200 },
    abilities: { mountaineering: false },
    production: 0,
    research: 250,
    population: 1,
    economy: 1,
    build_remaining: 500
}];

var library = exports.library = (0, _h_functions.obj_generate)("building", "library", 5, _action_type.ACTION_TYPE.BUILDING, stats);