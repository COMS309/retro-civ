'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.port = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 45, accuracy: 0.5 },
    hp: { max: 90, current: 90 },
    abilities: { mountaineering: false },
    production: 225,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 450
},
//lv1
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 60, accuracy: 0.5 },
    hp: { max: 120, current: 120 },
    abilities: { mountaineering: false },
    production: 300,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 600
},
//lv2
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 75, accuracy: 0.5 },
    hp: { max: 150, current: 150 },
    abilities: { mountaineering: false },
    production: 250,
    research: 0,
    population: 1,
    economy: 1,
    build_remaining: 750
}];

var port = exports.port = (0, _h_functions.obj_generate)("building", "port", 3, _action_type.ACTION_TYPE.BUILDING, stats);