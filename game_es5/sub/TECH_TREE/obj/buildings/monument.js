'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.monument = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0
{
    range: 0,
    speed: 0,
    attack: { range: 0, damage: 0, accuracy: 0.0 },
    defense: { block: 75, accuracy: 0.5 },
    hp: { max: 150, current: 150 },
    abilities: { mountaineering: false },
    production: 375,
    research: 375,
    population: 1.25,
    economy: 1.25,
    build_remaining: 1000
}];

var monument = exports.monument = (0, _h_functions.obj_generate)("building", "monument", 1, _action_type.ACTION_TYPE.BUILDING, stats);