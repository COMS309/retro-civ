'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.capital = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0: base
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 300, accuracy: 0.5 },
    defense: { block: 250, accuracy: 0.5 },
    hp: { max: 500, current: 500 },
    abilities: { mountaineering: false },
    production: 500,
    research: 500,
    population: 3.0,
    economy: 3.0,
    build_remaining: 1000
}];

var capital = exports.capital = (0, _h_functions.obj_generate)("city", "capitol", 1, _action_type.ACTION_TYPE.CITY, stats);