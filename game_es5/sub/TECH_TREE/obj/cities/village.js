'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.village = undefined;

var _h_functions = require('./../../h_functions.js');

var _action_type = require('./../../../../action_type.js');

var stats = [
//lv0: base
{
    range: 1,
    speed: 0,
    attack: { range: 1, damage: 50, accuracy: 0.5 },
    defense: { block: 50, accuracy: 0.5 },
    hp: { max: 100, current: 100 },
    abilities: { mountaineering: true },
    production: 0,
    research: 0,
    population: 0,
    economy: 0,
    build_remaining: 200
},
//lv1: base
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 100, accuracy: 0.5 },
    defense: { block: 50, accuracy: 0.5 },
    hp: { max: 100, current: 100 },
    abilities: { mountaineering: false },
    production: 100,
    research: 100,
    population: 1.0,
    economy: 1.0,
    build_remaining: 200
},
//lv2: 150 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 150, accuracy: 0.5 },
    defense: { block: 75, accuracy: 0.5 },
    hp: { max: 150, current: 150 },
    abilities: { mountaineering: false },
    production: 150,
    research: 150,
    population: 1.1,
    economy: 1.1,
    build_remaining: 300
},
//lv3: 200 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 200, accuracy: 0.5 },
    defense: { block: 100, accuracy: 0.5 },
    hp: { max: 200, current: 200 },
    abilities: { mountaineering: false },
    production: 200,
    research: 200,
    population: 1.2,
    economy: 1.2,
    build_remaining: 400
},
//lv4: 250 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 250, accuracy: 0.5 },
    defense: { block: 125, accuracy: 0.5 },
    hp: { max: 250, current: 250 },
    abilities: { mountaineering: false },
    production: 250,
    research: 250,
    population: 1.3,
    economy: 1.3,
    build_remaining: 500
},
//lv5: 300 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
{
    range: 0,
    speed: 0,
    attack: { range: 2, damage: 300, accuracy: 0.5 },
    defense: { block: 150, accuracy: 0.5 },
    hp: { max: 300, current: 300 },
    abilities: { mountaineering: false },
    production: 300,
    research: 300,
    population: 1.4,
    economy: 1.4,
    build_remaining: 600
}];

var village = exports.village = (0, _h_functions.obj_generate)("city", "village", 6, _action_type.ACTION_TYPE.CITY, stats);