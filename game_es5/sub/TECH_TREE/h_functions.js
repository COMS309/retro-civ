"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//HELPER FUNCTIONS
var level = exports.level = function level(_ref) {
    var name = _ref.name;
    var description = _ref.description;
    var cost = _ref.cost;
    var _ref$cities = _ref.cities;
    var cities = _ref$cities === undefined ? [] : _ref$cities;
    var _ref$buildings = _ref.buildings;
    var buildings = _ref$buildings === undefined ? [] : _ref$buildings;
    var _ref$units = _ref.units;
    var units = _ref$units === undefined ? [] : _ref$units;

    return {
        name: name,
        description: description,
        cost: cost,
        cities: cities,
        buildings: buildings,
        units: units
    };
};

var obj_generate = exports.obj_generate = function obj_generate(obj_type, obj_class, levels, obj_actions, stats) {
    var arr = {};
    for (var i = 0; i < levels; i++) {
        Object.assign(arr, _defineProperty({}, "lv" + i, {
            obj: obj_type,
            builder: obj_class,
            level: i,
            build: obj_actions.BUILD,
            upgrade: obj_actions.UPGRADE,
            stats: stats[i]
        }));
    }
    return arr;
};