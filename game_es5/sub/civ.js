'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.civs = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _h_functions = require('./../h_functions.js');

var _city = require('./city.js');

var _building = require('./building.js');

var _unit = require('./unit.js');

var _tech = require('./tech.js');

var _builders = require('./builders.js');

var _coins = require('./coins.js');

var _research = require('./research.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//CREATORS
var CREATORS = exports.CREATORS = {
    add: function add(civ) {
        return {
            type: _action_type.ACTION_TYPE.CIV.ADD,
            production_capacity: 100,
            civ: civ
        };
    },
    remove: function remove(civ) {
        return {
            type: _action_type.ACTION_TYPE.CIV.REMOVE,
            civ: civ
        };
    }
};

//REDUCERS
var civs = exports.civs = function civs() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var action = arguments[1];

    var civ_index = -1;
    switch (action.type) {
        case _action_type.ACTION_TYPE.CIV.ADD:
            return [].concat(_toConsumableArray(state), [civ(undefined, action)]);
        case _action_type.ACTION_TYPE.CIV.REMOVE:
            civ_index = (0, _h_functions.getIndex)(state, action.civ);
            return [].concat(_toConsumableArray(state.slice(0, civ_index)), _toConsumableArray(state.slice(civ_index + 1)));
        //Select appropriate hex
        case _action_type.ACTION_TYPE.GAME.TURN.NEXT:
            if (!action.civ) {
                return state;
            }
        //otherwise do like the rest of the fine actions below.
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
        case _action_type.ACTION_TYPE.CITY.ATTACK:
        case _action_type.ACTION_TYPE.CITY.HEAL:

        case _action_type.ACTION_TYPE.BUILDING.BUILD:
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:

        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
        case _action_type.ACTION_TYPE.UNIT.MOVE:
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
        case _action_type.ACTION_TYPE.UNIT.HEAL:

        case _action_type.ACTION_TYPE.TECH.SET:
            civ_index = (0, _h_functions.getIndex)(state, action.civ);
            if (state.length == 1) return [civ(state[0], action)];
            return [].concat(_toConsumableArray(state.slice(0, civ_index)), [civ(state[civ_index], action)], _toConsumableArray(state.slice(civ_index + 1)));
        default:
            return state;
    }
};

var civ = function civ() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.CIV.ADD:
            return {
                name: action.civ,
                cities: (0, _city.cities)(undefined, action),
                buildings: (0, _building.buildings)(undefined, action),
                units: (0, _unit.units)(undefined, action),
                builders: (0, _builders.builders)(undefined, action),
                techs: (0, _tech.techs)(undefined, action),
                coins: (0, _coins.coins)(undefined, action),
                research: (0, _research.research)(undefined, action)
            };
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
        case _action_type.ACTION_TYPE.CITY.ATTACK:
        case _action_type.ACTION_TYPE.CITY.HEAL:
            return Object.assign({}, state, {
                cities: (0, _city.cities)(state.cities, action),
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        case _action_type.ACTION_TYPE.BUILDING.BUILD:
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:
            return Object.assign({}, state, {
                buildings: (0, _building.buildings)(state.buildings, action),
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
        case _action_type.ACTION_TYPE.UNIT.HEAL:
            return Object.assign({}, state, {
                units: (0, _unit.units)(state.units, action),
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        case _action_type.ACTION_TYPE.UNIT.MOVE:
            //repeat action. once to add. once to remove
            console.log("civ");
            var u = (0, _unit.units)(state.units, action);
            return Object.assign({}, state, { units: (0, _unit.units)(u, action) });
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
            return Object.assign({}, state, {
                cities: (0, _city.cities)(state.cities, action),
                units: (0, _unit.units)(state.units, action),
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        case _action_type.ACTION_TYPE.TECH.SET:
            return Object.assign({}, state, {
                techs: (0, _tech.techs)(state.techs, action),
                builders: (0, _builders.builders)(state.builders, action),
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        case _action_type.ACTION_TYPE.GAME.TURN.NEXT:
            return Object.assign({}, state, {
                coins: (0, _coins.coins)(state.coins, action),
                research: (0, _research.research)(state.research, action)
            });
        default:
            return state;
    }
};