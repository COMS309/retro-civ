'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.techs = exports.tech_tree = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _tech_tree = require('./tech_tree.js');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//CREATORS
var CREATORS = exports.CREATORS = {
    set: function set(civ, tech, level, cost) {
        return {
            type: _action_type.ACTION_TYPE.TECH.SET,
            civ: civ,
            tech: tech,
            level: level,
            cost: cost
        };
    }
};

//REDUCERS
var tech_tree = exports.tech_tree = function tech_tree(state, action) {
    return _tech_tree.TECH_TREE;
};

var tech_init = {
    military: -1,
    manufacturing: -1,
    commerce: -1,
    research: -1,
    city: -1,
    colonization: -1
};

var techs = exports.techs = function techs() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? tech_init : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.TECH.SET:
            return Object.assign({}, state,
            //NOTE: this should be replaced with something similar to how hexes lookup the correct hex and civs lookup the correct civ.
            //That would get rid of the tech_init part as another reducer would set each category to 0.  This should work for now though.
            _defineProperty({}, action.tech, action.level));
        default:
            return state;
    }
};