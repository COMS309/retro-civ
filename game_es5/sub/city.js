'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.city = exports.cities = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _h_functions = require('./../h_functions.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//CREATORS
var CREATORS = exports.CREATORS = {
    build: function build(hex, civ, city, builder_data) {
        return {
            type: builder_data.build,
            hex: hex,
            civ: civ,
            city: city,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    upgrade: function upgrade(obj_data, builder_data) {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            city: obj_data.name,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    demolish: function demolish(obj_data) {
        return {
            type: _action_type.ACTION_TYPE.CITY.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            city: obj_data.name,
            stats: obj_data.stats
        };
    },
    attack: function attack(attack_unit, target_city) {
        return {
            type: _action_type.ACTION_TYPE.CITY.ATTACK,
            hex: target_city.hex,
            civ: target_city.civ,
            city: target_city.name,
            damage: attack_unit.stats.attack.damage
        };
    },
    heal: function heal(obj_data) {
        return {
            type: _action_type.ACTION_TYPE.CITY.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            city: obj_data.name,
            heal: obj_data.stats.hp.max
        };
    }
};

//REDUCERS
var cities = exports.cities = function cities() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var action = arguments[1];

    var index = -1;
    switch (action.type) {
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
            return [].concat(_toConsumableArray(state), [city(undefined, action)]);
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
            index = (0, _h_functions.getIndex)(state, action.city);
            return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.CITY.HEAL:
            index = (0, _h_functions.getIndex)(state, action.city);
            return [].concat(_toConsumableArray(state.slice(0, index)), [city(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.CITY.ATTACK:
            index = (0, _h_functions.getIndex)(state, action.city);
            var stats = state[index].stats;
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
            }
            return [].concat(_toConsumableArray(state.slice(0, index)), [city(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        default:
            return state;
    }
};
var city = exports.city = function city() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
            return {
                name: action.city,
                civ: action.civ,
                hex: action.hex,
                obj_type: 'city',
                builder: action.builder,
                level: action.level,
                stats: action.stats
            };
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
            return null;
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
            return Object.assign({}, state, {
                stats: action.stats,
                level: action.level
            });
        case _action_type.ACTION_TYPE.CITY.ATTACK:
            console.log('attack city');
            if (state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0) return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: hp
                    })
                })
            });
        case _action_type.ACTION_TYPE.CITY.HEAL:
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: state.stats.hp.max
                    })
                })
            });
        default:
            return state;
    }
};