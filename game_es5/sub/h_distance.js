'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.basicDistance = exports.findDistance = exports.findRange = exports.distance = exports.Indexer = exports.getNeighbors = undefined;

var _reactRedux = require('react-redux');

var _hex = require('./hex.js');

var r = 0;

var getNeighbors = exports.getNeighbors = function getNeighbors(hexes, hex) {
    var x = hex.x;
    var y = hex.y;
    var z = hex.z;
    var neighbors = [];
    var nx;
    var ny;
    var n;
    if (Math.abs(x + 1 + y) < r && Math.abs(x + 1) < r && Math.abs(y) < r) {
        nx = x + 1;
        ny = y;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    if (Math.abs(x - 1 + y) < r && Math.abs(x - 1) < r && Math.abs(y) < r) {
        nx = x - 1;
        ny = y;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    if (Math.abs(x + (y + 1)) < r && Math.abs(x) < r && Math.abs(y + 1) < r) {
        nx = x;
        ny = y + 1;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    if (Math.abs(x + (y - 1)) < r && Math.abs(x) < r && Math.abs(y - 1) < r) {
        nx = x;
        ny = y - 1;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    if (Math.abs(x + 1 + (y - 1)) < r && Math.abs(x + 1) < r && Math.abs(y - 1) < r) {
        nx = x + 1;
        ny = y - 1;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    if (Math.abs(x - 1 + (y + 1)) < r && Math.abs(x - 1) < r && Math.abs(y + 1) < r) {
        nx = x - 1;
        ny = y + 1;
        n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
        neighbors.push(hexes[n]);
    }
    return neighbors;
};

var Indexer = exports.Indexer = function Indexer(hex) {
    if (hex == null) {
        return;
    }
    var nx = hex.x;
    var ny = hex.y;
    var n = (nx + r - 1) * (2 * r - 1) + (ny + r - 1) + (nx + r) * (nx - r + 1) / 2 - (Math.abs(nx) / 2 + nx / 2) * nx;
    return n;
};

var distance = exports.distance = function distance(checked, d, hex, hexes, range) {
    var toCheck = getNeighbors(hexes, hex);
    var nextCheck = [];
    for (var i = 0; i < toCheck.length; i++) {

        var index = Indexer(toCheck[i]);
        var newD = d + toCheck[i].terrain.distance;
        if (checked[index] != null) {
            if (checked[index][0] > newD) {
                checked[index] = [newD, hex];
                nextCheck.push(toCheck[i]);
            }
        } else {
            checked[index] = [newD, hex];
            nextCheck.push(toCheck[i]);
        }
    }

    for (var i = 0; i < nextCheck.length; i++) {
        index = Indexer(nextCheck[i]);
        if (checked[index][0] < range) {
            distance(checked, checked[index][0], nextCheck[i], hexes, range);
        }
    }
};

var findRange = exports.findRange = function findRange(hex) {
    var range = 0;
    if (hex.units.length > 0) {

        for (var i = 0; i < hex.units.length; i++) {
            if (hex.units[i].stats.range * hex.units[i].stats.speed > range) {
                range = hex.units[i].stats.range * hex.units[i].stats.speed;
            }
        }
    }
    return range;
};

var findDistance = exports.findDistance = function findDistance(hex, hexes) {
    var area = hexes.length;
    r = Math.abs(-1 - Math.sqrt(1 - 4 * (1 - area) / 3)) / 2;
    var range = findRange(hex);
    var checked = [];
    for (var i = 0; i < 3 * (r * (r - 1)) + 1; i++) {
        checked.push(null);
    }
    var index = Indexer(hex);

    checked[index] = [0, hex];
    distance(checked, 0, hex, hexes, range);
    return checked;
};

var basicDistance = exports.basicDistance = function basicDistance(hexA, hexB) {

    var xdif = parseInt(hexA.split("_")[0]) - parseInt(hexB.split("_")[0]);
    var ydif = parseInt(hexA.split("_")[1]) - parseInt(hexB.split("_")[1]);
    var zdif = parseInt(hexA.split("_")[2]) - parseInt(hexB.split("_")[2]);

    return (Math.abs(xdif) + Math.abs(ydif) + Math.abs(zdif)) / 2;
};