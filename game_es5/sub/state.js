"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.state = exports.CREATORS = undefined;

var _action_type = require("./../action_type.js");

//CREATORS
var CREATORS = exports.CREATORS = {
    init: function init() {
        return {
            type: _action_type.ACTION_TYPE.GAME.MODE.INIT
        };
    },
    user: function user() {
        return {
            type: _action_type.ACTION_TYPE.GAME.MODE.USER
        };
    },
    server: function server() {
        return {
            type: _action_type.ACTION_TYPE.GAME.MODE.SERVER
        };
    }
};

//REDUCERS
var state = exports.state = function state() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? "init" : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.GAME.MODE.INIT:
            return "init";
        case _action_type.ACTION_TYPE.GAME.MODE.USER:
            return "user";
        case _action_type.ACTION_TYPE.GAME.MODE.SERVER:
            return "server";
        default:
            return state;
    }
};