'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.research = undefined;

var _action_type = require('./../action_type.js');

var init = {
    weekly: 50,
    balance: 500
};

//REDUCERS
var research = exports.research = function research() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? init : arguments[0];
    var action = arguments[1];
    var civs = arguments[2];

    switch (action.type) {
        case _action_type.ACTION_TYPE.GAME.TURN.NEXT:
            return {
                weekly: state.weekly,
                balance: state.balance + state.weekly
            };
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.BUILDING.BUILD:
            return {
                weekly: state.weekly + Number(action.stats.research),
                balance: state.balance
            };
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
        case _action_type.ACTION_TYPE.CITY.ATTACK:
            if (state.stats.hp.current - (action.damage - state.stats.defense.block) > 0) return state;
        //Otherwise do the same as below
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
            return {
                weekly: state.weekly - Number(action.stats.research),
                balance: state.balance
            };
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
            return {
                weekly: state.weekly + Number(action.stats.research) - Number(action.old_stats.research),
                balance: state.balance
            };
        case _action_type.ACTION_TYPE.UNIT.HEAL:
        case _action_type.ACTION_TYPE.CITY.HEAL:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:
            return state;
        case _action_type.ACTION_TYPE.TECH.SET:
            return {
                weekly: state.weekly,
                balance: state.balance - Number(action.cost)
            };
        default:
            return state;
    }
};