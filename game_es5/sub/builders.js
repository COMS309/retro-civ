'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.append_set = exports.append_builders = exports.builders = undefined;

var _action_type = require('./../action_type.js');

var _tech_tree = require('./tech_tree.js');

//CREATORS
/*
build: (hex, civ) => {
    return {
        type: action.build,
        data: {
            hex: hex,
            civ: civ,
            type: data.type,
        }
    }
},
upgrade: (stat) => {
    return {
        type: action.upgrade,
        data: {
            hex: stat.hex,
            civ: stat.civ,
            type: data.type,
        }
    }
}
*/

//REDUCERS
var builders = exports.builders = function builders() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? append_builders(undefined, _tech_tree.TECH_TREE.base) : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.TECH.SET:
            return Object.assign({}, state, append_builders(state, _tech_tree.TECH_TREE[action.tech][action.level]));
        default:
            return state;
    }
};

var append_builders = exports.append_builders = function append_builders() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var tech_level = arguments[1];

    return {
        cities: append_set(state.cities, tech_level.cities),
        buildings: append_set(state.buildings, tech_level.buildings),
        units: append_set(state.units, tech_level.units)
    };
};

var append_set = exports.append_set = function append_set() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var level = arguments[1];

    var set = {};
    level.map(function (builder) {
        set[builder.builder] = builder;
    });
    return Object.assign({}, state, set);
};