'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.turn = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

//CREATORS
var CREATORS = exports.CREATORS = {
    next: function next(user) {
        return {
            type: _action_type.ACTION_TYPE.GAME.TURN.NEXT,
            civ: user
        };
    }
};

//REDUCERS
var turn = exports.turn = function turn() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? -1 : arguments[0];
    var action = arguments[1];
    var civs = arguments[2];

    switch (action.type) {
        case _action_type.ACTION_TYPE.GAME.TURN.NEXT:
            return state < civs - 1 ? state + 1 : 0;
        default:
            return state;
    }
};