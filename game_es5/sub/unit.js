'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.unit = exports.units = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _h_functions = require('./../h_functions.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//CREATORS
var CREATORS = exports.CREATORS = {
    build: function build(hex, civ, unit, builder_data) {
        return {
            type: builder_data.build,
            hex: hex,
            civ: civ,
            unit: unit,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    upgrade: function upgrade(obj_data, builder_data) {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            unit: obj_data.name,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    transform: function transform(obj_data, city, builder_data) {
        return {
            type: _action_type.ACTION_TYPE.UNIT.TRANSFORM,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            old_stats: obj_data.stats,
            city: city,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        };
    },
    move: function move(obj_data, destination) {
        return {
            type: _action_type.ACTION_TYPE.UNIT.MOVE,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            destination: destination,
            builder: obj_data.builder,
            level: obj_data.level,
            stats: obj_data.stats
        };
    },
    demolish: function demolish(obj_data) {
        return {
            type: _action_type.ACTION_TYPE.UNIT.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            stats: obj_data.stats
        };
    },
    attack: function attack(attack_unit, target_unit) {
        return {
            type: _action_type.ACTION_TYPE.UNIT.ATTACK,
            hex: target_unit.hex,
            civ: target_unit.civ,
            unit: target_unit.name,
            damage: attack_unit.stats.attack.damage
        };
    },
    heal: function heal(unit) {
        return {
            type: _action_type.ACTION_TYPE.UNIT.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            heal: obj_data.stats.hp.max
        };
    }
};

//REDUCERS
var units = exports.units = function units() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var action = arguments[1];

    var index = -1;
    switch (action.type) {
        case _action_type.ACTION_TYPE.UNIT.BUILD:
            return [].concat(_toConsumableArray(state), [unit(undefined, action)]);
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
            index = (0, _h_functions.getIndex)(state, action.unit);
            return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.UNIT.MOVE:
            index = (0, _h_functions.getIndex)(state, action.unit);
            if (index !== -1) {
                return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
            }
            return [].concat(_toConsumableArray(state), [unit(undefined, action)]);
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.HEAL:
            index = (0, _h_functions.getIndex)(state, action.unit);
            return [].concat(_toConsumableArray(state.slice(0, index)), [unit(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
            index = (0, _h_functions.getIndex)(state, action.unit);
            var stats = state[index].stats;
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [].concat(_toConsumableArray(state.slice(0, index)), _toConsumableArray(state.slice(index + 1)));
            }
            return [].concat(_toConsumableArray(state.slice(0, index)), [unit(state[index], action)], _toConsumableArray(state.slice(index + 1)));
        default:
            return state;
    }
};
var unit = exports.unit = function unit() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.UNIT.MOVE:
            return {
                name: action.unit,
                civ: action.civ,
                hex: action.destination || action.hex,
                obj_type: 'unit',
                builder: action.builder,
                stats: action.stats,
                level: action.level
            };
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
            return null;
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
            return Object.assign({}, state, {
                stats: action.stats || null,
                level: action.level
            });
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
            if (state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0) return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: hp
                    })
                })
            });
        case _action_type.ACTION_TYPE.UNIT.HEAL:
            return Object.assign({}, state, {
                stats: Object.assign({}, state.stats, {
                    hp: Object.assign({}, state.stats.hp, {
                        current: state.stats.hp.max
                    })
                })
            });
        default:
            return state;
    }
};