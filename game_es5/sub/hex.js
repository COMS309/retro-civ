'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.hex = exports.hexes = exports.CREATORS = undefined;

var _action_type = require('./../action_type.js');

var _h_functions = require('./../h_functions.js');

var _city = require('./city.js');

var _building = require('./building.js');

var _unit = require('./unit.js');

var _terrain = require('./terrain.js');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//CREATORS
var CREATORS = exports.CREATORS = {
    add: function add(hex, terrain) {
        return {
            type: _action_type.ACTION_TYPE.HEX.ADD,
            hex: hex,
            terrain: terrain
        };
    },
    remove: function remove(hex) {
        return {
            type: _action_type.ACTION_TYPE.HEX.REMOVE,
            hex: hex
        };
    }
};

//REDUCERS
var hexes = exports.hexes = function hexes() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? [] : arguments[0];
    var action = arguments[1];

    var hex_id = -1;
    switch (action.type) {
        case _action_type.ACTION_TYPE.HEX.ADD:
            return [].concat(_toConsumableArray(state), [hex(undefined, action)]);
        case _action_type.ACTION_TYPE.HEX.REMOVE:
            hex_id = (0, _h_functions.getIndex)(state, action.hex);
            return [].concat(_toConsumableArray(state.slice(0, hex_id)), _toConsumableArray(state.slice(hex_id + 1)));
        //Select appropriate hex
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
        case _action_type.ACTION_TYPE.CITY.ATTACK:
        case _action_type.ACTION_TYPE.CITY.HEAL:

        case _action_type.ACTION_TYPE.BUILDING.BUILD:
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:

        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
        case _action_type.ACTION_TYPE.UNIT.HEAL:
            hex_id = (0, _h_functions.getIndex)(state, action.hex);
            return [].concat(_toConsumableArray(state.slice(0, hex_id)), [hex(state[hex_id], action)], _toConsumableArray(state.slice(hex_id + 1)));
        case _action_type.ACTION_TYPE.UNIT.MOVE:
            hex_id = (0, _h_functions.getIndex)(state, action.hex);
            var tmp = [].concat(_toConsumableArray(state.slice(0, hex_id)), [hex(state[hex_id], action)], _toConsumableArray(state.slice(hex_id + 1)));
            var dest_id = (0, _h_functions.getIndex)(tmp, action.destination);
            return [].concat(_toConsumableArray(tmp.slice(0, dest_id)), [hex(tmp[dest_id], action)], _toConsumableArray(tmp.slice(dest_id + 1)));
        default:
            return state;
    }
};
var hex = exports.hex = function hex() {
    var state = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var action = arguments[1];

    switch (action.type) {
        case _action_type.ACTION_TYPE.HEX.ADD:
            return {
                name: action.hex,
                x: parseInt(action.hex.split("_")[0]),
                y: parseInt(action.hex.split("_")[1]),
                z: parseInt(action.hex.split("_")[2]),
                city: (0, _city.city)(undefined, action),
                building: (0, _building.building)(undefined, action),
                units: (0, _unit.units)(undefined, action),
                terrain: action.terrain
            };
        case _action_type.ACTION_TYPE.CITY.BUILD:
        case _action_type.ACTION_TYPE.CITY.UPGRADE:
        case _action_type.ACTION_TYPE.CITY.DEMOLISH:
        case _action_type.ACTION_TYPE.CITY.ATTACK:
        case _action_type.ACTION_TYPE.CITY.HEAL:
            return Object.assign({}, state, {
                city: (0, _city.city)(state.city, action)
            });
        case _action_type.ACTION_TYPE.BUILDING.BUILD:
        case _action_type.ACTION_TYPE.BUILDING.UPGRADE:
        case _action_type.ACTION_TYPE.BUILDING.DEMOLISH:
        case _action_type.ACTION_TYPE.BUILDING.ATTACK:
        case _action_type.ACTION_TYPE.BUILDING.HEAL:
            return Object.assign({}, state, {
                building: (0, _building.building)(state.building, action)
            });
        case _action_type.ACTION_TYPE.UNIT.BUILD:
        case _action_type.ACTION_TYPE.UNIT.UPGRADE:
        case _action_type.ACTION_TYPE.UNIT.DEMOLISH:
        case _action_type.ACTION_TYPE.UNIT.MOVE:
        case _action_type.ACTION_TYPE.UNIT.ATTACK:
        case _action_type.ACTION_TYPE.UNIT.HEAL:
            return Object.assign({}, state, {
                units: (0, _unit.units)(state.units, action)
            });
        case _action_type.ACTION_TYPE.UNIT.TRANSFORM:
            return Object.assign({}, state, {
                city: (0, _city.city)(state.city, action),
                units: (0, _unit.units)(state.units, action)
            });
        default:
            return state;
    }
};