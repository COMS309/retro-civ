'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.CREATORS = undefined;

var _hex = require('./sub/hex.js');

var _civ = require('./sub/civ.js');

var _city = require('./sub/city.js');

var _building = require('./sub/building.js');

var _unit = require('./sub/unit.js');

var _tech = require('./sub/tech.js');

var _turn = require('./sub/turn.js');

var _action_type = require('./action_type.js');

var reset = function reset() {
    return {
        type: _action_type.ACTION_TYPE.RESET
    };
};

var name = {
    set: function set(name) {
        return {
            type: _action_type.ACTION_TYPE.NAME.SET,
            name: name
        };
    }
};
//CREATORS
var CREATORS = exports.CREATORS = {
    turn: _turn.CREATORS,
    hex: _hex.CREATORS,
    civ: _civ.CREATORS,
    city: _city.CREATORS,
    building: _building.CREATORS,
    unit: _unit.CREATORS,
    tech: _tech.CREATORS,
    name: name,
    reset: reset
};