/// <reference path="../typings/tsd.d.ts" />

const express_server = require('express');   
const app = express_server();
const http_server = require('http').createServer(app);
const io = require('socket.io')(http_server);
/* Setup Database */
//const dbClient = require('./redisAPI.js').dbClient;
const dbClient = require('./redis/dbClient.js');
var client = dbClient.run(io);

/* Setup Logging */
var morgan = require('morgan');
app.use(morgan('dev'));

/*Setup Static Resource Location */
app.use(express_server["static"](__dirname + '\\..\\public'));

app.get('/login', function(req, res){
    var name = req.query.name;
    var pass = req.query.pass;
    dbClient.user.register(client, io, name, pass, (data) => {
        res.status(200).json(data).end();
    },
    (data) => {
        res.status(400).json(data).end();
    });
})

/*Http Server Setup Callback */
function http_server_callback() {
    return console.log('Listening on port 8080');
}

/*Run Server */
http_server.listen(8080, http_server_callback);
