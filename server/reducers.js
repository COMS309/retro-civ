import { combineReducers } from 'redux';
import { ACTION_TYPE } from './action_type.js';
import { ENGINE } from './engine.js';

import { hexes } from './sub/hex.js';
import { civs } from './sub/civ.js';
const name = (state = "game", action) => {
    switch (action.type) {
        case "SET GAME.name":
            return action.name;
        default:
            return state;
    }
}

const state = (state = "update", action) => {
    switch (action.type) {
        case ACTION_TYPE.STATE.SET:
            return action.state;
        default:
            return state;
    }
}
export const REDUCERS = {
    GAME: ENGINE(combineReducers({
        name,
        state,
        hexes,
        civs
    }))
};

