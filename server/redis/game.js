const createStore = require('redux').createStore;
const connect = require('./socket.js').connect;



//Incr by 1 every time used
const generateID = (client, cb) => {
    client.incr('current_game', function(err, reply) {
        cb(reply);
    })
}

// Game Key Format
const gameName = (id) => "game-" + id;

// Create a new Game
const newGame = (client, io, cb) => {
    generateID(client, (id) => {
        //var store = createStore(reducers);
        console.log(gameName(id));
        //set to default values
        client.hmset(gameName(id), {
            name: gameName(id),
            users: JSON.stringify([]),
            hist: JSON.stringify([]),
            //store: JSON.stringify(store),
            user_count: 1
        })
        //add to global games list
        client.sadd('games', gameName(id));
        //add socket listeners to this game
        connect(client, io, gameName(id));
        //return game name
        cb(null, gameName(id));
    })        
}

// Get next available game
const getNext = (client, io, cb) => {
    client.get("current_game", (err, game_id) => {
        if(err) {
            console.log("error", err);
            return cb(err);
        }
        //Create a new game if one doesn't exist
        if(game_id === -1) {
            return newGame(client, io, cb);
        }
        
        client.hgetall(gameName(game_id), (err, obj) => {
            if(err) {
                console.log("error",err);
                return cb(err);
            }
            // if less than 3 people in this game
            if(obj && obj.user_count < 3) {
                obj.user_count++;
                client.hmset(gameName(game_id), obj);
                return cb(null, gameName(game_id));
            }
            // otherwise create a new game
            return newGame(client, io, cb);
        })
    });
}

module.exports = {
    getNext: getNext
}