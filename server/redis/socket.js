const CREATORS = require('./../../../game_es5/creators.js').CREATORS
const h_functions = require('./h_functions.js');
const setHash = h_functions.setHash;
const appendList = h_functions.appendList;
const dispatch = h_functions.dispatch;
const initialize = require('./initialize.js');

const linkGames = (client, io, cb) => {
    console.log("games:");
    client.smembers('games',(err, obj) => {
        if(err) {
            cb(err);
        }
        console.log(obj);
        if(obj && obj.length > 0) {
            obj.map(game => {
                client.hgetall(game, (e, o) => {
                    console.log('   ' + game);
                    console.log('      users')
                    JSON.parse(o.users).map(user => console.log('         ' + user));
                    console.log('      count');
                    console.log('         ', o.user_count)
                    console.log('      hist')
                    JSON.parse(o.hist).map(hist => console.log('         ' + JSON.stringify(hist.msg.action.type)))
                        
                })
                connect(client, io, game);
            })
        }
        console.log('   null');
    })
}

const connect = (client, io, game) => {
    var g_space = io.of('/' + game);
    g_space.on('connection', socket => {
        console.log('someone connected');
        //Set up sockets for new connections
        socket.on('action', action_card => {
            console.log('action received');
            client.hget(game, 'hist', (err, h_obj) => {
                //Get History
                if(err) {
                    return console.log(err);
                }
                var hist = JSON.parse(h_obj);
                
                echo(g_space, socket, client, game, hist, action_card);
            })
        });
        socket.on('update', hash => {
            console.log('update request received');
            client.hget(game, 'hist', (err, h_obj) => {
                if(err) {
                    return console.log(err);
                }
                var hist = JSON.parse(h_obj);
                update(socket, hist, hash);
            })
        });
        socket.on('refresh', user => {
            client.hget(game, 'hist', (err, h_obj) => {
                //Get history
                if(err) {
                    return console.log(err);
                }
                var hist = JSON.parse(h_obj);

                client.hget(game, 'users', (err, u_obj) => {
                    //Get list of users
                    if(err) {
                        return console.log(err);
                    }
                    var users = JSON.parse(u_obj);
                    refresh(g_space, socket, client, game, hist, users, user)
                })
            })
        })
    })
    
}
const echo = (g_space, socket, client, game, hist, action_card) => {
    if(hist[hist.length - 1].hash == action_card.parent) {
        var h_msg = setHash(action_card.msg); 
    
        appendList(client, game, 'hist', hist, h_msg);
                    
        g_space.emit('action', h_msg);
    }
    else {
        socket.emit('suggest', 'update');
    }
};

const refresh = (g_space, socket, client, game, hist, users, user) => {
    //send the existing history
    hist.map(action_card => socket.emit('action', action_card));
    //socket.emit('action_group', hist);

    //If User exists, do nothing more
    if(users && users.indexOf(user) != -1) {
        console.log('refresh', user);
    }
    //Otherwise:
    else if (users.length < 3) {
        var rt = null;
        if(users.length == 0) {
            hist = initialize.createBoard(g_space, client, game, hist, 5);
        }
        //Add them to the game.users list
        users = appendList(client, game, 'users', users, user);
        //Create a new civilization
        hist = appendList(client, game, 'hist', hist, dispatch(g_space, setHash({
            user,
            action: CREATORS.civ.add(user),
        })));
        //Add Civ Objects
        rt = initialize.createCiv(g_space, client, game, users, hist, users.length - 1);
        hist = rt.hist;
        users = rt.users;

        if(users.length == 3) {
            hist = appendList(client, game, 'hist', hist, dispatch(g_space, setHash({
                server: 'server',
                action: CREATORS.turn.next()
            })))
        }
    }
    else {
        console.log('TOO MANY USERS');
    }
}

const update = (socket, hist, hash) => {
    var found = false;
    hist.map(h_msg => {
        if(found || h_msg.hash == hash) {
            found = true;
            socket.emit('update', h_msg);
        }
    })
    if(found == false) {
        socket.emit('suggest', 'refresh');
    }
}

module.exports = {
    linkGames,
    connect
}