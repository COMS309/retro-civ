const crypto = require('crypto');

const setHash = (msg) => {
    return {
        msg,
        hash: crypto.createHmac('sha256', 'retro').update(JSON.stringify(msg)).digest('hex'),
    }
}

const appendList = (client, game, attr, list, item) => {
    var t = [
        ...list,
        item
    ]
    client.hmset(game, attr, JSON.stringify(t));
    return t;
}

const dispatch = (io, h_msg) => {
    io.emit('action', h_msg);
    return h_msg;
}

module.exports = {
    setHash,
    appendList,
    dispatch
}