/* Setup Database */
const redis = require('redis');
var client;

var dbClient = {
    version: 2,
    runCount: function(cb) {
        client.incr('runNumber', function(err, reply) {
            console.log("Run: ", reply);
        });
    },
    run: function() {
        client = redis.createClient();
        client.on('connect', function() {
            console.log('connected');
            dbClient.init(function(){
                dbClient.runCount();
            });
        })
    },
    init: function(cb) {
        client.get('version', function(err,reply) {
            if(reply == null || Number(reply) != dbClient.version) {
                console.log("Init Redis data version: ", dbClient.version);
                client.multi()
                .set('version', dbClient.version)
                .set('runNumber',0)
                .set('unique',0)
                .set('ryan','I exist')
                .set('josh','I also Exist')
                .exec(cb);
            } else {
                console.log("Redis data version: ", reply)
                if(cb) {
                    cb();
                }
            }
        });
        client.exists('init', function(err,reply) {
            if(reply === 1) {
                
            }
        });
    }
};
var game = {
    new: function(user) {
        dbClient.generateID(function(gameID){
            //Create Game Object
            var GameObject
            
            client.multi()
                //Add Game Object to Reddis with given unique id
                .hmset(gameID,GameObject)
                //Push to list of active games
                .sadd(['games', gameID])
                //Push to user's Active Game
                .set([user + "_game", gameID])
            .exec();
        });
    },
    list: function(cb) {
        client.smembers('games', function(err, set){
            cb(set);
        })
    },
    getGame: function(id, cb) {
        client.hgetall(id, function(err, obj) {
            cb(obj);
        })
    }
}
var user = {
    newUser: function() {
        dbClient.generateID(function(userID){
            //Create User Object
            var UserObject
            
            client.multi()
                //Add User Object to Redis with given unique id
                .hmset(userID, UserObject)
                //Push to list of users
                .sadd(['users', userID])
            .exec();
        });
    },
    getUsers: function(cb) {
        client.smembers('users', function(err, set){
            cb(set);
        })
    },
    getUser: function(id,cb) {
        client.get(id,function(err, obj){
            cb(obj)
        })
    },
    getUserGame: function(id,cb) {
        client.get(id+"_games", function(err, gameID) {
            dbClient.OP.getGame(gameID, cb);               
        })
    }
}

module.exports = {
    dbClient
}