// SETUP Redux and Router Hooks
//      Load reducers
const Data_Creators = require('./../../../game_es5/creators.js').CREATORS;

const TERRAIN  = require('./../../../game_es5/sub/terrain.js').TERRAIN;
const units = require('./../../../game_es5/sub/TECH_TREE/obj/units.js').units;
const cities = require('./../../../game_es5/sub/TECH_TREE/obj/cities.js').cities;
const buildings = require('./../../../game_es5/sub/TECH_TREE/obj/buildings.js').buildings;
const h_functions = require('./h_functions.js');
const setHash = h_functions.setHash;
const appendList = h_functions.appendList;
const dispatch = h_functions.dispatch;

var teraform = [TERRAIN.DESERT, TERRAIN.GRASSLAND, TERRAIN.FOREST, TERRAIN.MOUNTAIN, TERRAIN.WATER];

const server = 'server';

const createBoard = (io, client, game, hist, r, cb) => {
    hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
        server,
        action: Data_Creators.name.set(game)
    })));
    
    for (var i = -r + 1; i < r; i++) {
        for (var j = -r + 1; j < r; j++) {
            if (Math.abs(i + j) < r) {
                var x = i;
                var y = j;
                var z = -(x + y);
                var name = x + "_" + y + "_" + z;

                var terrain = teraform[Math.abs((x * y + x) * z + z + x) % 5];
                // var terrain=teraform[Math.abs((x*y+x)*z+z+x+y)%5];  //Board 2
                // var terrain=teraform[1];  //Board Grass
                // var terrain=teraform[Math.abs((x-r)*(y-r)*(z-r))%5];  //Board 4

                hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                    server,
                    action: Data_Creators.hex.add(name, terrain)
                })));
            }
        }
    }
    return hist;
};

const createCiv = (io, client, game, users, hist, civ_id) => {
    var server = 'server';
    console.log(civ_id);
    switch(civ_id) {
        case 0:
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.city.build("-1_-3_4", users[0], 'city1', cities.village.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.building.build("-2_-2_4", users[0], 'building1', buildings.library.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.unit.build("-1_-3_4", users[0], "steve", units.soldier.lv0)
            })));
            break;
        case 1:
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.city.build("-3_4_-1", users[1], 'city2', cities.village.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.building.build("-2_4_-2", users[1], 'building2', buildings.library.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.unit.build("-3_4_-1", users[1], "kyle", units.soldier.lv0)
            })));
            break;
        case 2:
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.city.build("4_0_-4", users[2], 'city3', cities.village.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.building.build("4_-1_-3", users[2], 'building3', buildings.library.lv0)
            })));
            hist = appendList(client, game, 'hist', hist, dispatch(io, setHash({
                        server,
                        action: Data_Creators.unit.build("4_0_-4", users[2], "Geoff", units.soldier.lv0)
            })));
            break;
    }
    return {hist, users};
}

module.exports = {
    createBoard,
    createCiv
}