const redis = require('redis');
const linkGames = require('./socket.js').linkGames;
const listUsers = require('./user.js').listUsers;
const version = "2.2.5";
const debug = false;

const run = (io) => {
    var client = redis.createClient();
    client.on('connect', () => {
        console.log('connected');
        init(client, io, () => runCount(client));
    })
    return client;
}

const runCount = (client, cb) => {
    client.incr('runNumber', (err, reply) => {
        console.log("Run: ", reply);
    });
}

const init = (client, io, cb) => {
    client.get('version', (err, reply) => {
        if(debug || reply == null || reply != version) {
            console.log("Init Redis data version: ", version);
            client.multi()
            .flushdb()
            .set('version', version)
            .set('runNumber',0)
            .set('unique',0)
            .set('current_game', -1)
            .exec(cb);
        } else {
            console.log("Read Redis data version: ", reply)
            linkGames(client, io, (err) => {
                if(err) {
                    return console.log("Error:", err);
                }
            })
            listUsers(client, err => {
                if(err) {
                    return console.log("Error:", err);
                }
            })
            if(cb) {
                cb();
            }
        }
    });
    client.exists('init', (err,reply) => {
        if(reply === 1) {
            
        }
    });
}
module.exports = run