const redis = require('redis');
const game = require('./game.js');

const register = (client, io, name, pass, success, failure) => {
    if(!name) {
        return failure({
            msg: "Enter a valid Username and Password"
        })
    }
    client.hgetall("user-" + name, (err, obj) => {
        if(err) {
            console.log("error",err);
            return failure({msg: "" + err})
        }
        if(obj && obj.name == name) {
            console.log("user name matches", name)
            if(obj && obj.pass == pass) {
                console.log("password matches", pass)
                return success({
                    name,
                    pass,
                    game: obj.game
                })
            }
            else {
                console.log("password mismatch", obj.pass, pass);
                return failure({
                    msg: "User already registered.  Try another username"
                })
            }
        }
        else {
            console.log("create new user");
            game.getNext(client, io, (err, game_id) => {
                if(err) {
                    console.log(err);
                    failure({msg: "" + err})
                }
                client.hmset("user-" + name, {
                    name: name,
                    pass: pass,
                    game: game_id
                })
                console.log("name:",name);
                console.log("game:",game_id)
                return success({
                    name,
                    pass,
                    game: game_id 
                })
            })
        }
    })
}

const listUsers = (client, cb) => {
    
}
//const getUser = (client, name, cb) => {
//    console.log("get user");
//    client.hgetall("user-" + name, cb)
//}
module.exports = {
    register,
    listUsers
}