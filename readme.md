Prerequisites:  
* Node - https://nodejs.org/download/  
* Ruby 2.2.x (for sas compilation) - https://www.ruby-lang.org/en/downloads/  
  
From the command line:  
	gem install sass  
	npm install -g grunt-cli bower  
	npm install  
	bower install  
	grunt serve  
  
If you run into problems read "Setup.md"  