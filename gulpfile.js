var gutil = require('gulp-util');
gutil.log("LOAD DEPENDENCIES: Start")
gutil.log("\tgulp")
var gulp = require('gulp');
gutil.log("\tgulp-open")
var open = require('gulp-open');
gutil.log("\tgulp-sequence");
var gulpSequence = require('gulp-sequence');
gutil.log("\tvinyl-source-stream")
var source = require('vinyl-source-stream');
gutil.log("\tbrowserify")
var browserify = require('browserify');
gutil.log("\twatchify")
var watchify = require('watchify');
gutil.log("\treactify")
var reactify = require('reactify');
gutil.log("\tbabelify")
var babelify = require('babelify');
gutil.log("\tgulp-typescript")
var ts = require('gulp-typescript');
//var concat = require('gulp-concat');
gutil.log("\tgulp-html-replace")
var htmlreplace = require('gulp-html-replace');
gutil.log("\tnodemon")
var nodemon = require('nodemon');
gutil.log("\tdel")
var del = require('del');
gutil.log("\tgulp-sass")
var sass = require('gulp-sass');
gutil.log("\tcssnano")
var cssnano = require('gulp-cssnano');
//var browserSync = require('browser-sync').create();

gutil.log("SET PARAMS")
//PATHS
var path = {
    url: 'http://localhost:8080',
    inject: {
        dep: ["js/base.js","js/ui.js","css/mscom-grid.css","css/app.css","css/core.css"],
        theme: [
            [""             ,"css/core-light.css"   ,"light"],
            [""             ,"css/ui-light.css"     ,"light"],
            ["alternate "   ,"css/core-dark.css"    ,"dark"],
            ["alternate "   ,"css/ui-dark.css"      ,"dark"]],
        sync: "/browser-sync/browser-sync-client.2.11.1.js",
    },
    name: {
        app:    'bundle.js',
        icons:  'icons.js',
        server: 'node_app.js',
    },
    src: {
        HTML: ['./client/index.html','./client/icons.html'],
        css: 'client/assets/css/*.css',
        img: 'client/assets/img/*.*',
        data: 'client/assets/data/*.*',
        sass: 'client/app/app.scss',
        app: ['./client/app/react_app.jsx'],
        icons: ['./client/app/react_icons.jsx'],
        server: ['./server/**/*.js'],
        winjs: {
            js:    './node_modules/winjs/js/*.*',
            css:   './node_modules/winjs/css/*.*',
            fonts: './node_modules/winjs/fonts/*.*'
        }
    },
    dev: {
        clean: './.tmp/**',
        public: './.tmp/public/',
        css:    './.tmp/public/css/',
        img:    './.tmp/public/img/',
        data:   './.tmp/public/data/',
        js:     './.tmp/public/js/',
        fonts:  './.tmp/public/fonts/',
        server: './.tmp/server/'
    },
    
}
//HELPER FUNCTIONS
var logChange = function (file) {
    gutil.log('File Updated: \t' + file.relative + '');
};
var errorHandler = function(err) {
    gutil.log(err.stack);
};
//TOP LEVEL TASKS
gulp.task('clean', function() {
    gutil.log("CLEAN")
    return del(path.dev.clean);
})
gulp.task('run', function (cb) {
    gutil.log("RUN")
    var started = false;
    
    return nodemon({
        script: path.dev.server + path.name.server
    })
    .on('start', function() {
        gutil.log('starting nodemon...')
        if(!started) {
            cb();
            started = true;
        }
        gulp.src(__filename)
        .pipe(open({uri: path.url}));
    })
    .on('restart', function () {
        gutil.log('restarting nodemon...')
        //gulp.src(__filename)
        //.pipe(open({uri: path.url}));
    })
     
})

    //APP TASKS
    gulp.task('app', gulpSequence(['appCompile','winjs','img','data','css','sass'],'inject'));
        var browserifyCompile = function(src,name,output) {
            var bundler = browserify({
                entries: src,//Grab initial file
                debug: true,                            //Sourcemaps
                cache: {}, packageCache: {}, 
                fullPaths: true
            })
            .transform("babelify", {presets: ["es2015","react"]});
            var watcher = watchify(bundler)
            .on('update', function() {                  //When any file changes
                var updateStart = Date.now();
                gutil.log('Updating: ' + src);
                bundle(watcher);
                console.log('Update Complete: ', (Date.now() - updateStart) + 'ms');
            })
            
            var bundle = function(b) {
                return b
                .bundle()                        //Create initial bundle
                .on('error', errorHandler)
                .pipe(source(name))
                .pipe(gulp.dest(output));
            }
            return bundle(watcher);
        }
        //compile app code
        gulp.task('appCompile', function() {
            gutil.log("APP COMPILE")
            browserifyCompile(path.src.app   , path.name.app    , path.dev.public);
            browserifyCompile(path.src.icons , path.name.icons  , path.dev.public);
        })
        //images
        gulp.task('img', function() {
            gutil.log("APP IMG")
            return gulp.src(path.src.img)
            .pipe(gulp.dest(path.dev.img))
        })
        //data
        gulp.task('data', function() {
            gutil.log("APP DATA")
            return gulp.src(path.src.data)
            .pipe(gulp.dest(path.dev.data))
        })
        //css
        gulp.task('css', function() {
            gutil.log("APP CSS")
            return gulp.src(path.src.css)
            .pipe(cssnano())
            .pipe(gulp.dest(path.dev.css))
        })
        //sass
        gulp.task('sass', function() {
            gutil.log("APP SASS")
            return gulp.src(path.src.sass)
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest(path.dev.css));
        })
        //winjs files
        gulp.task('winjs', function() {
            gutil.log("COPY WINJS DEPENDENCIES")
            gulp.src(path.src.winjs.js)
            .pipe(gulp.dest(path.dev.js))
            .on('error', errorHandler);
            
            gulp.src(path.src.winjs.css)
            .pipe(gulp.dest(path.dev.css))
            .on('error', errorHandler);
            
            return gulp.src(path.src.winjs.fonts)
            .pipe(gulp.dest(path.dev.fonts))
            .on('error', errorHandler);
        })
        //html
        gulp.task('inject', function(){
            gutil.log("INJECT SCRIPTS INTO HTML")
            return gulp.src(path.src.HTML)
            .pipe(htmlreplace({
                'dep': path.inject.dep,
                'theme': {
                    src: path.inject.theme,
                    tpl: '<link rel="%sstylesheet" href="%s" title="%s">'
                },
                'app': path.name.app,
                'icons': path.name.icons
                //'sync': path.inject.script
            }))
            .pipe(gulp.dest(path.dev.public))
        })
    //SERVER TASKS
    gulp.task('server', ['serverCompile'])
        //compile server code
        gulp.task('serverCompile', function(){
            gutil.log("SERVER COMPILE")
            //return browserifyCompile(path.src.server, path.name.server , path.dev.server);
            return gulp.src(path.src.server)
            //.pipe(ts({
            //    noImplicitAny: false
            //}))
            .pipe(gulp.dest(path.dev.server));
        })
gulp.task('watch', function(){
    gulp.watch(path.src.img, ['img'], logChange)
    gulp.watch(path.src.data, ['data'], logChange)
    gulp.watch(path.src.css, ['css'], logChange)
    gulp.watch(path.src.sass, ['sass'], logChange)
    gulp.watch(path.src.HTML, ['inject'], logChange)
    gulp.watch([path.src.winjs.js, path.src.winjs.css, path.src.winjs.fonts], ['winjs'], logChange)
    gulp.watch(path.src.server, ['serverCompile'], logChange)
})
gulp.task('default', gulpSequence('clean',['app','server'],'watch','run'));
/**
//Refresh browser page when static files are updates
gulp.task('browser-sync', ['runServer'], function() {
    browserSync.init({
        proxy: path.inject.url,
        files: [path.dist.public + "**//*.*"],
        browser: "google chrome",
        port: 7000,
    });
});
*/
/*var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');

gulp.task('build', function () {
    browserify({
        entries: 'client/app/react_app.jsx',
        extensions: ['jsx'],
    })
    .transform(babelify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('dist'));
})

gulp.task('default', ['build']);
*/