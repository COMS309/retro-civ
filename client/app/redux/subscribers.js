import { SUBSCRIBERS as APP } from  './sub/app.js';
import { SUBSCRIBERS as USER } from './sub/user.js';
import { SUBSCRIBERS as UI } from   './sub/ui.js';

export const SUBSCRIBERS = {
    APP,
    USER,
    UI
}