import { CREATORS as APP } from './sub/app.js';
import { CREATORS as USER } from './sub/user.js';
import { CREATORS as UI } from './sub/ui.js';

export const CREATORS = {
    APP,
    USER,
    UI
}