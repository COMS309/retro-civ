import { ACTION_TYPE } from './../action_type.js';
import cookie from 'react-cookie';

//CREATORS
const login = (data) => {
    console.log(ACTION_TYPE.USER.LOGIN,data);
    return {
        type: ACTION_TYPE.USER.LOGIN,
        data
    }
}
const logout = () => {
    return {
        type: ACTION_TYPE.USER.LOGOUT
    }
}
const authFail = (msg, err) => {
    return {
        type: ACTION_TYPE.USER.ERROR,
        msg,
        err
    }
}
export const CREATORS = {
    login,
    logout,
    authFail
}

//REDUCERS
const name = (state = null, action) => {
    switch (action.type) {
        case ACTION_TYPE.USER.LOGIN:
            return action.data.name;
        case ACTION_TYPE.USER.LOGOUT:
            return "";
        default:
            return state;
    }
}
const game = (state = null, action) => {
    switch (action.type) {
        case ACTION_TYPE.USER.LOGIN:
            return action.data.game;
        case ACTION_TYPE.USER.LOGOUT:
            return "";
        default:
            return state;
    }
}
const authToken = (state = false, action) => {
    switch (action.type) {
        case ACTION_TYPE.USER.LOGIN:
            return action.data.authToken;
        case ACTION_TYPE.USER.LOGOUT:
            return false;
        case ACTION_TYPE.USER.ERROR:
            return false;
        default:
            return state;
    }
}
const errorCode = (state = false, action) => {
    switch (action.type) {
        case ACTION_TYPE.USER.LOGIN:
            return false;
        case ACTION_TYPE.USER.LOGOUT:
            return false;
        case ACTION_TYPE.USER.ERROR:
            return action.err;
        default:
            return state;
    }
}
const errorMsg = (state = false, action) => {
    switch (action.type) {
        case ACTION_TYPE.USER.LOGIN:
            return false;
        case ACTION_TYPE.USER.LOGOUT:
            return false;
        case ACTION_TYPE.USER.ERROR:
            return action.msg;
        default:
            return state;
    }
}
export const REDUCERS = {
    USER: (state = {}, action) => {
        return {
            name: name(state.name, action),
            game: game(state.game, action),
            //authToken: authToken(state, action),
            errorCode: errorCode(state.errorCode, action),
            errorMsg: errorMsg(state.errorMsg, action)
        }
    }
};

//SUBSCRIBERS
export const SUBSCRIBERS = {
    
};