import { ACTION_TYPE } from './../action_type.js';
import { hex } from './../../../../game/sub/hex.js';
import { findDistance, getNeighbors, findRange } from './../../../../game/sub/h_distance.js';
import { getIndex } from './../../../../game/h_functions.js';

//CREATORS
const select = (hex, hexes) => {
    var distances = findDistance(hex, hexes);
    var range = findRange(hex);
    return {
        type: ACTION_TYPE.UI.SELECT,
        hex,
        distances,
        range
    }
}
const deselect = (hex) => {
    return {
        type: ACTION_TYPE.UI.DESELECT
    }
}
const target = (hex) => {
    var path = null;
    return {
        type: ACTION_TYPE.UI.TARGET,
        hex
    }
}
const untarget = (hex) => {
    return {
        type: ACTION_TYPE.UI.UNTARGET
    }
}
const select_helper = (hex, hexes, selected, targeted) => {
    if(selected) {
        var id = getIndex(hexes, hex.name);
        if(selected.hex.name == hex.name) {
            return deselect(hex, hexes);
        }
        else if(targeted && targeted.hex.name == hex.name) {
            return untarget(hex);
        }
        else if(selected.range != 0 && selected.distances[id] && selected.distances[id][0] <= selected.range) {
            return target(hex, hexes);
        }
        else {
            return select(hex, hexes);
        }
    }
    else
    {
        return select(hex, hexes);
    } 
}
export const CREATORS = {
    //select,
    deselect,
    //target,
    //untarget,
    select_helper
}

//REDUCERS
export const selected_hex = (state = null, action) => {
    switch(action.type) {
        case ACTION_TYPE.UI.SELECT:
            return Object.assign(
                {},
                {
                    hex: action.hex,
                    distances: action.distances,
                    range: action.range
                }
            );
        case ACTION_TYPE.UI.DESELECT:
            return null;
        default:
            return state;
    }
}
export const targeted_hex = (state = null, action) => {
    switch(action.type) {
        case ACTION_TYPE.UI.TARGET:
            return Object.assign(
                {},
                {
                    hex: action.hex,
                }
            );
        case ACTION_TYPE.UI.SELECT:
        case ACTION_TYPE.UI.DESELECT:
        case ACTION_TYPE.UI.UNTARGET:
            return null;
        default:
            return state;
    }
}
export const REDUCERS = {
    UI: (state = {}, action) => {
        return {
            selected: selected_hex(state.selected, action),
            targeted: targeted_hex(state.targeted  , action)
        }
    }
};

//SUBSCRIBERS
export const SUBSCRIBERS = {
    
};