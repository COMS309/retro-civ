import { ACTION_TYPE } from './../action_type.js';
import cookie from 'react-cookie';

//CREATORS
const setTheme = (theme) => {
    return {
        type: ACTION_TYPE.APP.THEME.SET,
        theme
    }
}
const initTheme = (alt = 'light') => {
    let theme = cookie.load('theme');
    if(theme) {
        return CREATORS.setTheme(theme);
    }
    return CREATORS.setTheme(alt);
}
const setName = (name) => {
    return {
        type: ACTION_TYPE.APP.NAME.SET,
        name
    }
}
export const CREATORS = {
    setTheme,
    initTheme,
    setName
}

//REDUCERS
const name = (state = "APP_NAME", action) => {
    switch (action.type) {
        case ACTION_TYPE.APP.NAME.SET:
            return action.name;
        default:
            return state;
    }
};
const theme = (state = "light", action) => {
    switch (action.type) {
        case ACTION_TYPE.APP.THEME.SET:
            return action.theme;
        default:
            return state;
    }
};
export const REDUCERS = {
    APP: (state = {}, action) => {
        return {
            name: name(state.name, action),
            theme: theme(state.theme, action)
        }
    }
};

//SUBSCRIBERS
let myTheme = undefined;
const updateTheme = (c_theme) => {
    if (myTheme !== undefined && myTheme !== c_theme) {
        cookie.save('theme', c_theme)
    }
    myTheme = c_theme;
    let links = document.getElementsByTagName("link");
    for (let i = 0, len = links.length; i < len; i++) {
        let link = links[i];
        if (link.rel.indexOf("stylesheet") !== -1 && link.title) {
            if (link.title === myTheme) {
                link.disabled = false;
            } else {
                link.disabled = true;
            }
        }
    }
}
export const SUBSCRIBERS = {
    updateTheme
}