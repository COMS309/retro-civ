export const ACTION_TYPE = {
    APP: {
        NAME: {
            SET: "SET      APP.name"
        },
        THEME: {
            SET: "SET      APP.theme"
        }
    },
    USER: {
        LOGIN:   "LOGIN    USER",
        LOGOUT:  "LOGOUT   USER",
        ERROR:   "ERROR    USER"
    },
    UI: {
        SELECT:  "SELECT   UI.hex",
        DESELECT:"DESELECT UI.hex",
        TARGET:  "TARGET   UI.hex",
        UNTARGET:"UNTARGET UI.hex"
    }
}