import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { io_dispatch } from './../../../io/socket.js';
import { CREATORS as Data_Creators} from './../../../../../game/creators.js';

const Menu_Presentation = ({
    onInfoClick,
    take_turn,
    myTurn,
    gameOver,
    player
}) => {
    return (
        <div>
            <button className="win-button" onClick={onInfoClick("/game/tech")}>Research</button>
            <button className="win-button" onClick={onInfoClick("/game/civ")}>My Civ</button>
            <button className="win-button" onClick={onInfoClick("/game/board")}>Board</button>
            <button className="win-button" onClick={onInfoClick("/game/instructions")}>Instructions</button>
            { (myTurn && !gameOver) ? (<button className="win-button" onClick={take_turn}>End Turn</button>) : (<label className="win-h4">Wait..</label>)}
            <label className="win-h2">It's {player} turn</label>
        </div>
    );
};

const mapStateToProps = (
    state
) => {
    var turn = state.GAME.turn;
    //turn = turn == -1? 0 : turn;
    var aCiv = state.GAME.civs[turn];
    var user = aCiv? aCiv.name : null;
    var myTurn = false;0
    if(state.USER.name && ( turn>-1)){
        myTurn=(state.USER.name == aCiv.name);
    }
    var gameOver=(state.GAME.civs.length==1);
    var remove = false;
    var lostCiv=null;
    state.GAME.civs.map(civ =>{
        if(civ.cities.length==0 && civ.buildings.length==0 && civ.units.length==0){
            lostCiv=civ;
            remove=true;
        }
    })
    var turn = state.GAME.turn;
    var player=null;
    var civs=null;
    var name= state.USER.name;
    if(turn>-1){
        player=state.GAME.civs[turn].name;
        civs=state.GAME.civs;
        if(myTurn){
        player="your";
    }else{
        player+="'s";
    }
    }
    
    return {
        selected: state.UI.selected,
        turn: state.GAME.turn || null,
        player,
        myTurn,
        gameOver,
        remove,
        take_turn: () => {
            if(remove) {
            io_dispatch(Data_Creators.civ.remove(lostCiv.name));
                }
            io_dispatch(Data_Creators.turn.next(user));
        
        }
        
    }
}
const mapDispatchToProps = (
    dispatch
) => {
    return {
        onInfoClick: (path) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push(path);
            }
        }
    };
};
export const Menu = connect(
    mapStateToProps,
    mapDispatchToProps
)(Menu_Presentation);