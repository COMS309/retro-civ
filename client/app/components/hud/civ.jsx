import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../generic.jsx';
import { connect } from 'react-redux';
import { idLookup } from './../../../../game/h_functions.js';
import { io_dispatch } from './../../io/socket.js';
import { CREATORS as Data_Creators} from './../../../../game/creators.js';

const Civ_Presentation = ({
    civs,
    name,
    myCiv,
    subpane,
    forfeit
}) => {
    return (
        <section className="ms-grid full">
            <section className="ms-row" style={{ height: "400px" }}>
                <CenterBox>
                    {subpane}
                </CenterBox>
            </section>
            <section className="ms-row">
                <CenterBox>
                    <h3>{myCiv.name}</h3>
                    Cities: {myCiv.cities.length}
                    <br/>
                    Units: {myCiv.units.length}
                    <br/>
                    Buildings: {myCiv.buildings.length}
                    <br/>
                    Balance: {myCiv.coins.current}
                    <br/>
                    Weekly Income: {myCiv.coins.weekly}
                    <br/>
                    <button className="win-button" onClick={forfeit}>Forfeit</button>
                </CenterBox>
            </section>
        </section>
    )
}
const mapStateToProps = (
    state
) => {
    var civs = state.GAME.civs;
    var name = state.USER.name;
    var myCiv = idLookup(civs, name);
    var length = civs.length;
    return {
        civs,
        name,
        myCiv,
        forfeit: () => {
            
            io_dispatch(Data_Creators.civ.remove(myCiv.name));
            
        }
    }
}

const mapDispatchToProps = (
    dispatch
) => {
    return {
        onSelectLevel: (tree, level) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push("/game/tech/" + tree + '/' + level);
            }
        },
        onRedirect: (path) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push(path);
            }
        },
        levelUpdate: (civ, tech, level) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                io_dispatch(Data_Creators.tech.set(civ, tech, level))
            }
        }
    }
}

export const Civ = connect(
    mapStateToProps,
    mapDispatchToProps
)(Civ_Presentation)