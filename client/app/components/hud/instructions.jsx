import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../generic.jsx';

export const Instructions = () => {
    return (
        <CenterBox>
            <div style={{margin: "40px"}}>
                <font size="10" color="#9900cc" font-weight="bold" >Instructions:</font>
                <p>Retro-Civ is a turned based strategy game inspired by Civilization. Players will compete against each other by controlling all aspects of their civilizations.
                    Victory results from conquering all other foreign powers in the game.</p>
                <p>To begin the game, each player has one civilization on the grid of hexes. These hexes are composed of different types of terrain, some of which are more treacherous than others.
                    At the start of the game, each player has one unit, building, and city. A unit is a moveable pawn, whereas cities and buildings remain on the hex they were built.</p>
                <p>At the beginning of the player's turn, they can choose to do several things, which are shown to the right of the board when their civilization is selected.
                    The player can upgrade, create, demolish, or view the stats of any of these objects. The player may also choose to move a unit, attack other units, or even transform the unit into a city.</p>
                <p>At any time, a player may select the Tech tab to view information about themselves, or to see their progress to victory on the Tech Tree.
                    They may also use their money to advance on any of the three trees: city, military, or manufacturing. Advancing on the trees is benificial, as higher levels yield more benefits.</p>
                <p>The goal of the game is to achieve victory by conquest. Once a player has erradicated all other civilizations, they have won the game!</p>
            </div>
        </CenterBox>
    );
};