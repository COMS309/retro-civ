import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { browserHistory } from 'react-router';
export const city_level_info = ({city_level}) => {
    return(
            <div>
                {city_level.name}<br/>
                {city_level.description}
                <br/><br/>
                Cities Avaliable
                <br/><br/>
                {city_level.cities.map(city => {
                    return (
                        <div>
                            <button className="win-button">{city.builder} {city.level}</button>
                            <br/>
                        </div>
                    )
                })}
            </div>
    )
}