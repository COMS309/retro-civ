import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { browserHistory } from 'react-router';

export const city_stats_info = (city) => {
    return(
        <div>
            Cost: {city.stats.build_remaining} cr
            <br/><br/>
            Health: {city.stats.hp.max} hp
            <br/><br/>
            Defense: {city.stats.defense.block}
            <br/><br/>
            Bonuses:
            <br/>
            <ul>
                <li>Production: {city.stats.production}</li>
                <li>Research: {city.stats.research}</li>
                <li>Economy: {city.stats.economy}</li>
            </ul>
        </div>
    )
}