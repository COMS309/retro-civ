import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { browserHistory } from 'react-router';
import { TECH_TREE } from './../../../../../game/sub/tech_tree.js';

export const manufacturing_stats_info = (building) => {
    return (
        <div>
            Cost: {building.stats.build_remaining} cr
            <br/>
            Health: {building.stats.hp.max} hp
            <br/>
            Defense:
            <br/>
            <ul>
                <li>Block: {building.stats.defense.block}</li>
            </ul>
            Bonuses:
            <br/>
            <ul>
                <li>Production: {building.stats.production}</li>
                <li>Research: {building.stats.research}</li>
                <li>Economy: {building.stats.economy}</li>
            </ul>

        </div>
    )
}