import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { browserHistory } from 'react-router';
import { TECH_TREE } from './../../../../../game/sub/tech_tree.js';

export const military_stats_info = (unit) => {
    return(
        <div>
            Cost: {unit.stats.build_remaining} cr
            <br/><br/>
            Health: {unit.stats.hp.max} hp
            <br/><br/>
            Range: {unit.stats.range * unit.stats.speed}
            <br/><br/>
            Attack:
            <br/>
            <ul>
                <li>Range: {unit.stats.attack.range}</li>
                <li>Damage: {unit.stats.attack.damage}</li>
            </ul>
            Defense: {unit.stats.defense.block}
            <br/><br/>
            Bonuses:
            <br/>
            <ul>
                <li>Production: {unit.stats.production}</li>
                <li>Research: {unit.stats.research}</li>
                <li>Economy: {unit.stats.economy}</li>
            </ul>
        </div>
    )
}