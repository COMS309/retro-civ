import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import * as d3 from 'd3';
import * as ReactFauxDOM from 'react-faux-dom';
import $ from 'jquery';
import { CREATORS } from './../../redux/creators.js';
import { TERRAIN } from './../../../../game/sub/terrain.js';
import { basicDistance } from './../../../../game/sub/h_distance.js';

import { Loading } from './loading.jsx'
// import { createStore } from 'redux'
// let store = createStore(hexes, [ 'Use Redux' ])

let r=0;
let lowlimit=0;
let highlimit=0;

let size=25;
let w=size*2;
let h=Math.trunc(Math.sqrt(3)/2*w);

let width=300;
let height=300;

let plot=[];
let civils=[];

let colors=["#ff0000", "#ff8000", "#ffff00", "#00ff00", "#00ffff", "#0000ff", "#7f00ff", "#ff00ff", "#ff0080"];

const Bd = ({hexes, hexClick, selected, targeted, s_hex, t_hex, s_range, name}) => {
    
    var g = ReactFauxDOM.createElement('svg')
     
    var svg = d3.select(g)
        .attr('width', width)
        .attr('height', height)

    hexes.map((hex, i) => {
        var stroke=3;
        var opacity=1;
        var strokeC="black"
        if(hex.name==s_hex){
            stroke=4;
            opacity=0.6;
            strokeC="red";
        }
        if(hex.name==t_hex) {
            stroke=6;
            opacity=0.6;
            strokeC="blue";
        }
        
        var polygon = svg.append("polyline") //draw elements
            .attr("points",createPolygon(hex))
            .style("fill",hex.terrain.color)
            .attr('name',hex.name)
            .attr('x',hex.x)
            .attr('y',hex.y)
            .attr('z',hex.z)
            .attr('class', 'cell')
            .attr('id',i)
            .attr('stroke',strokeC)
            .attr('stroke-width',stroke)
            .attr("opacity",opacity)
            //.on('mouseover', function(d){console.log(hex.name);})
            .on('click', function(){hexClick(hex, hexes, selected, targeted);});
                
                    
        if(hex.city){
            var rectangle = svg.append("rect")
                .attr("x",getCenter(hex)[0]-w/4)
                .attr("y",getCenter(hex)[1]-h*2/3)
                .attr("width",w/2)
                .attr("height",h*5/6)
                .attr("fill","grey")
                .attr('stroke',function(){return getColor(hex.city);})
                .attr('stroke-width','3px')
                .attr('stroke-dasharray',function(){if(hex.city.civ==name){return "0";}else{return "5,3";}})
                .attr("hexName",hex.name)
                .on('click', function(){hexClick(hex, hexes, selected, targeted);}); 
        }       
        
        if(hex.building){
            var rectangle = svg.append("rect")
                .attr("x",getCenter(hex)[0]-w/4)
                .attr("y",getCenter(hex)[1]-h/3)
                .attr("width",w/2)
                .attr("height",h/2)
                .attr("fill","brown")
                .attr('stroke',function(){return getColor(hex.building);})
                .attr('stroke-width','3px')
                .attr('stroke-dasharray',function(){if(hex.building.civ==name){return "0";}else{return "5,3";}})
                .attr("hexName",hex.name)
                .on('click', function(){hexClick(hex, hexes, selected, targeted);});
        }       
    
                    
        if(hex.units[0]){
            hex.units.map(unit =>{
                var circle = svg.append("circle") //draw elements
                    .attr("cx",getCenter(hex)[0])
                    .attr("cy",getCenter(hex)[1])
                    .attr("r",size/2)
                    .style("fill","orange")
                    .attr('name',unit.name)
                    .attr('x',hex.x)
                    .attr('y',hex.y)
                    .attr('z',hex.z)
                    .attr('stroke',function(){return getColor(hex.units[0]);})
                    .attr('stroke-width','3px')
                    .attr('stroke-dasharray',function(){if(hex.units[0].civ==name){return "0";}else{return "5,3";}})
                    .attr("hexName",hex.name)
                    //.on('mouseover', function(d){console.log(hex.name);})
                    .on('click', function(){hexClick(hex, hexes, selected, targeted)});
                
        })} 
        
        if(s_range != 0 && hex.name != s_hex && (targeted? hex.name != t_hex: true)){
            if(selected.distances[i] && selected.distances[i][0]<=s_range){
                var polygon = svg.append("polyline") //draw elements
                    .attr("points",createPolygon(hex))
                    .style("fill","red")
                    .attr('name',hex.name)
                    .attr('x',hex.x)
                    .attr('y',hex.y)
                    .attr('z',hex.z)
                    .attr('class', 'cell')
                    .attr('id',i)
                    .attr('stroke',"red")
                    .attr('stroke-width',3)
                    .attr("opacity",0.5)
                    //.on('mouseover', function(d){console.log(hex.name);})
                    .on('click', function(){hexClick(hex, hexes, selected, targeted);});
            }
        }
    })
    
    

    return g.toReact();
}


const GameBoard = ({
    name,
    hexes,
    hexClick,
    selected,
    targeted,
    s_hex,
    t_hex,
    s_range,
    user,
    civs
}) => {
    if(hexes) {
        initializeBoard(hexes, selected, civs);
        
        return (
            <Bd height="1000" width="1000" hexes={hexes} hexClick={hexClick} selected={selected} targeted={targeted} s_hex={s_hex} t_hex={t_hex} s_range={s_range} name={user}/>
        );
    }
    return (
        <Loading/>
    )
};

const mapStateToProps = (
    state
) => {
    var selected = state.UI.selected;
    var s_unit, s_city, s_building, s_obj, s_civ, s_hex;
    var s_range = 0;
    var civs = state.GAME.civs;
    if(selected) {
        s_hex = selected.hex.name;
        s_unit = selected.hex.units[0];
        s_city = selected.hex.city;
        s_building = selected.hex.building;
        s_obj = s_unit || s_city || s_building;
        s_civ = (s_obj && s_obj.civ);
        s_range = selected.range;
    }
    
    var targeted = state.UI.targeted;
    var t_unit, t_city, t_building, t_obj, t_civ, t_hex;
    if(targeted) {
        t_hex = targeted.hex.name;
        t_unit = targeted.hex.units[0];
        t_city = targeted.hex.city;
        t_building = targeted.hex.building;
        t_obj = t_unit || t_city || t_building;
        t_civ = (t_obj && t_obj.civ); 
    }
    var isMyCiv = !t_civ || t_civ == s_civ;
    var isAttackable = t_obj && !isMyCiv && basicDistance(s_hex, t_hex) <= s_obj.stats.attack.range;
    return {
        name: state.APP.name,
        user: state.USER.name,
        hexes: state.GAME.hexes,
        selected,
        s_hex,
        targeted,
        t_hex,
        s_range,
        civs
    }
}
const mapDispatchToProps = (
    dispatch,
    state
    
) => {
    
    return {
        hexClick: (hex, hexes, selected, targeted) => {
            dispatch(CREATORS.UI.select_helper(hex, hexes, selected, targeted));
        }
        
    };
};
export const Board = connect(
    mapStateToProps,
    mapDispatchToProps
)(GameBoard);

const initializeBoard = (
    hexes,
    selected,
    civs
) => {
    var area=hexes.length;
    r=Math.abs(-1-Math.sqrt(1-4*(1-area)/3))/2;
    lowlimit=r-1;
    highlimit=lowlimit*3;
    plot=hexes;
    civils=civs;
    width=w*2*r;
    height=h*2*r;
};

const getCenter = (
    hex
) => {
    var x=hex.x;
    var y=hex.y;
    var dx=width/2+x*3/4*w;
    var dy=height/2+y*h+x*1/2*h;
        return [dx,dy];
    
    
};
const createPolygon = (
    hex
) => {
    var point=hex;
    var center=getCenter(point);
    var p1=[center[0]-w/2,center[1]];
        var p2=[center[0]-w/4,center[1]+h/2];
        var p3=[center[0]+w/4,center[1]+h/2];
        var p4=[center[0]+w/2,center[1]];
        var p5=[center[0]+w/4,center[1]-h/2];
        var p6=[center[0]-w/4,center[1]-h/2];
            return [p1,p2,p3,p4,p5,p6,p1];
    
    
};

let lineColor = ["DarkRed", "Blue", "Green", "Black"];

const getColor = (
    obj
) => {
    var index=3;
    civils.map(civ =>{
        if(civ.name==obj.civ){
            index=civ.index;
        }
    } )
    return lineColor[index];
}
