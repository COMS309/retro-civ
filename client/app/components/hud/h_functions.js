export const isMyCiv = (s_obj, t_obj) => {
    var t_civ = getCiv(t_obj);
    return !t_civ || t_civ == getCiv(s_obj);
}

export const getCiv = (obj) => {
    return obj && obj.civ;
}

export const getObj = (hex) => {
    return getUnit(hex) || getBuilding(hex) || getCity(hex);
}
export const getObjL = (u, b, c) => {
    return u || b || c;
}

export const getCity = (hex) => {
    return hex && hex.city;
}

export const getUnit = (hex) => {
    return hex && hex.units[0];
}

export const getBuilding = (hex) => {
    return hex && hex.building;
}

export const getHex = (st) => {
    return st && st.hex;
}

export const getName = (obj) => {
    return obj && obj.name;
}
export const getStats = (obj) => {
    return obj && obj.stats;
}