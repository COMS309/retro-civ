import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../generic.jsx';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { io_dispatch } from './../../io/socket.js';
import { CREATORS as Data_Creators} from './../../../../game/creators.js';
import { idLookup } from './../../../../game/h_functions.js';

const Tech_Presentation = ({
    onSelectLevel,
    onRedirect,
    subpane,
    levelUpdate,
    thisCiv,
    research,
    thisCity,
    cityCost,
    thisMili,
    miliCost,
    thisManu,
    manuCost,
    TECH_TREE
}) => {
    return (
        <section className="ms-grid full">
            <section className="ms-row" style={{ height: "400px" }}>
                <CenterBox>
                    {subpane}
                </CenterBox>
            </section>
            <section className="ms-row">
                <CenterBox>
                    <h2 className="win-h2">Research: {research.balance} rp</h2>
                    <br/>
                    <h4 className="win-h4">{research.weekly} rp / week </h4>
                    <br/>
                    <center>
                        <table>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    {TECH_TREE.city.map((city_level, i) => {
                                        return (
                                            <td><center><label className="win-h3">Level {i}</label></center></td>
                                        )
                                    }) }
                                </tr>
                                <tr>
                                    <td>{(research.balance >= cityCost && cityCost !== -1)? (
                                            <button className="win-button" onClick={levelUpdate(thisCiv, "city", thisCity + 1, cityCost) }>Upgrade {cityCost} rp</button>
                                        ) : cityCost !== -1? (
                                            <center><label className="win-h5">Upgrade {cityCost} rp</label></center>
                                        ) : (
                                            <center><label className="win-h4">Max</label></center>
                                        )}</td>
                                    <td><center><label className="win-h4">City:</label></center></td>
                                    {TECH_TREE.city.map((city_level, i) => {
                                        if(i === thisCity)
                                            return (<td><button className="win-button" onClick={onSelectLevel("city", city_level.name) }>{city_level.name}</button></td>);
                                        return (<td><center><label className="win-h4">{city_level.name}</label></center></td>)
                                    }) }
                                </tr>
                                <tr>
                                    <td>{(research.balance >= miliCost && miliCost !== -1)? (
                                            <button className="win-button" onClick={levelUpdate(thisCiv, "military", thisMili + 1, miliCost) }>Upgrade {miliCost} rp</button>
                                        ) : miliCost !== -1? (
                                            <center><label className="win-h5">Upgrade {miliCost} rp</label></center>
                                        ) : (
                                            <center><label className="win-h4">Max</label></center>
                                        )}</td>
                                    <td> <center><label className="win-h4">Military:</label></center></td>
                                    {TECH_TREE.military.map((military_level, i) => {
                                        if(i === thisMili)
                                            return (<td> <button className="win-button" onClick={onSelectLevel("military", military_level.name) }>{military_level.name}</button></td>);
                                        return (<td><center><label className="win-h4">{military_level.name}</label></center></td>)
                                    }) }
                                </tr>
                                <tr>
                                    <td>{(research.balance >= manuCost && manuCost !== -1)? (
                                            <button className="win-button" onClick={levelUpdate(thisCiv, "manufacturing", thisManu + 1, manuCost) }>Upgrade {manuCost} rp</button>
                                        ) : manuCost !== -1? (
                                            <center><label className="win-h5">Upgrade {manuCost} rp</label></center>
                                        ) : (
                                            <center><label className="win-h4">Max</label></center>
                                        )}</td>
                                    <td> <center><label className="win-h4">Manufacturing:</label></center></td>
                                    {TECH_TREE.manufacturing.map((manufacturing_level, i) => {
                                        if(i === thisManu)
                                            return (<td><button className="win-button" onClick={onSelectLevel("manufacturing", manufacturing_level.name) }>{manufacturing_level.name}</button></td>);
                                        return (<td><center><label className="win-h4">{manufacturing_level.name}</label></center></td>)
                                    }) }
                                </tr>
                            </tbody>
                        </table>
                    </center>
                    <br/><br/>
                </CenterBox>
            </section>
        </section>
    )
}

const mapStateToProps = (
    state
) => {
    var civs = state.GAME.civs;
    var name = state.USER.name;
    var TECH_TREE = state.GAME.TECH_TREE; 
    
    var civ = idLookup(civs, name);
    var research = civ.research;
    console.log(research)
    var thisCiv  = civ.name;
    
    var thisCity = civ.techs.city;
    var cityCost = (TECH_TREE.city.length > (thisCity + 1)) ? TECH_TREE.city[thisCity + 1].cost : -1;
    var thisMili = civ.techs.military;
    var miliCost = (TECH_TREE.military.length > (thisMili + 1)) ? TECH_TREE.military[thisMili + 1].cost : -1;
    var thisManu = civ.techs.manufacturing;
    var manuCost = (TECH_TREE.manufacturing.length > (thisManu + 1)) ? TECH_TREE.manufacturing[thisManu + 1].cost : -1;
    return {
        thisCiv,
        thisCity,
        research,
        cityCost,
        thisMili,
        miliCost,
        thisManu,
        manuCost,
        TECH_TREE
    }
}

const mapDispatchToProps = (
    dispatch
) => {
    return {
        onSelectLevel: (tree, level) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push("/game/tech/" + tree + '/' + level);
            }
        },
        onRedirect: (path) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push(path);
            }
        },
        levelUpdate: (civ, tech, level, cost) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                io_dispatch(Data_Creators.tech.set(civ, tech, level, cost))
            }
        }
    }
}

export const Tech = connect(
    mapStateToProps,
    mapDispatchToProps
)(Tech_Presentation)