import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { idLookup } from './../../../../../game/h_functions.js';
import { TECH_TREE } from './../../../../../game/sub/tech_tree.js';
import { city_stats_info } from './../info/city_stats_info.jsx';
import { military_stats_info } from './../info/military_stats_info.jsx';
import { manufacturing_stats_info } from './../info/manufacturing_stats_info.jsx';

{var i = -1}

const Panel_Display = ({
    selected,
    hex,
    city,
    unit,
    build,
    onRedirect,
    object,
    url
}) => {
    return (
        <div>
            {/*object.builder*/}
            <h3>Stats</h3>
            
            <h5 hidden>{i=-1}</h5>
            {TECH_TREE.military.map(level => {
                return (
                        <div>
                            {level.name.substring(0,3) === url.substring(20,23)? (
                                <div>
                                    {level.units.map(unit => {
                                        {i ++}
                                        return(
                                            <div>
                                                {unit.builder === url.substring(20 + level.name.length + 1)? (
                                                    <div>
                                                        {military_stats_info(level.units[i])}
                                                    </div>
                                                ):("")}
                                            </div>
                                        )
                                    })}
                                </div>
                            ):("")}
                        </div>
                        )
            })}
            
            <h5 hidden>{i=-1}</h5>
            {TECH_TREE.city.map(level => {
                return (
                        <div>
                            {level.name.substring(0,2) === url.substring(16,18)? (
                                <div>
                                    {level.cities.map(city => {
                                        {i ++}
                                        return(
                                            <div>
                                                {city.builder === url.substring(16 + level.name.length + 1)? (
                                                    <div>
                                                        {city_stats_info(level.cities[i])}
                                                    </div>
                                                ):("")}
                                            </div>
                                        )
                                    })}
                                </div>
                            ):("")}
                        </div>
                        )
            })}   
            
            <h5 hidden>{i=-1}</h5>
            {TECH_TREE.manufacturing.map(level => {
                return (
                        <div>
                            {level.name.substring(0,1) === url.substring(25,26)? (
                                <div>
                                    {level.buildings.map(building => {
                                        {i ++}
                                        return(
                                            <div>
                                                {building.builder === url.substring(25 + level.name.length + 1)? (
                                                    <div>
                                                        {manufacturing_stats_info(level.buildings[i])}
                                                    </div>
                                                ):("")}
                                            </div>
                                        )
                                    })}
                                </div>
                            ):("")}
                        </div>
                        )
            })}    
        </div>
    )
}

const mapStateToProps = (
    state,
    props
) => {
        return {
            url: state.routing.locationBeforeTransitions.pathname
            /*object: idLookup(TECH_TREE[props.params.tree], props.params.builder, "error message")*/
        }
}
    
const mapDispatchToProps = (
    dispatch
) => { 
        return {
            onRedirect: (path) => {
                return (e) => {
                    if (e != undefined) {
                        e.preventDefault();
                    }
                    browserHistory.push(path);
                }
            } 
        }
}

export const Object_Panel = connect(
    mapStateToProps,
    mapDispatchToProps
)(Panel_Display)