import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

const Panel_Display = ({
    selected,
    hex,
    city,
    onInfoClick//,
    //unit,
    //build
}) => {
    //var bd = () => {
    //    e.PreventDefault;
    //    build(selected, hex, city, unit, build);
    //}
    return (
        //html here
        <div>
            <button className="win-button" onClick={onInfoClick("/city")}>City</button>
            <button className="win-button" onClick={onInfoClick("/unit")}>Military</button>
        </div>
        
    )
}

/*
const mapStateToProps(state) => {
    var selected = state.UI.selected;
    var hex = state.GAME.hexes[selected];
    var city = hex.city;
    //var unit = hex.units[0];
    return {
        selected,
        hex,
        city//,
        //unit
    }
}*/

const mapStateToProps = (
    state
) => {
    return {
        name: state.APP.name,
        hexes: state.GAME.hexes
    }
}

const mapDispatchToProps = (
    dispatch
) => {
    return {
        onInfoClick: (path) => {
            return (e) => {
                if (e != undefined) {
                    e.preventDefault();
                }
                browserHistory.push(path);
            }
        }
    };
};

export const Panel = connect(
    mapStateToProps,
    mapDispatchToProps
)(Panel_Display)