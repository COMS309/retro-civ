import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { idLookup } from './../../../../../game/h_functions.js';
import { TECH_TREE } from './../../../../../game/sub/tech_tree.js';
//import { military_level_info } from './../info/military_level_info.jsx';

const Panel_Display = ({
    selected,
    hex,
    city,
    unit,
    build,
    onStats,
    onRedirect,
    level
}) => {
    return (
        <div style={{margin: "40px"}}>
            <h2 className="win-h2">{level.name}</h2>
            <h3 className="win-h4">{level.cost} rp</h3>
            <p>{level.description}</p>
            <br/>
            <h3 className="win-h3">Avaliable Builds:</h3>
            <br/>
            
            {level.cities.map(city => {
                    return (
                            <button className="win-button" onClick={onStats("city",level.name, city.builder, city.level)}>level {city.level} {city.builder} </button>
                    )
                })}
            
            {level.units.map(unit => {
                    return (
                            <button className="win-button" onClick={onStats("military",level.name, unit.builder, unit.level)}>level {unit.level} {unit.builder} </button>
                    )
                })}
            
            {level.buildings.map(building => {
                    return (
                            <button className="win-button" onClick={onStats("manufacturing",level.name, building.builder, building.level)}>level {building.level} {building.builder} </button>
                    )
                })}
        </div>
    )
}

const mapStateToProps = (
    state,
    props
) => {
    console.log(props);
        return {
            selected: state.UI.selected,
            level: idLookup(TECH_TREE[props.params.tree], props.params.level, "error message")
        }
}
    
const mapDispatchToProps = (
    dispatch
) => { 
        return {
            onStats: (tree, name, builder, level) => {
                return () => {browserHistory.push("/game/tech/" + tree + '/' + name + '/' + builder);}
            },
            onRedirect: (path) => {
                return (e) => {
                    if (e != undefined) {
                        e.preventDefault();
                    }
                    browserHistory.push(path);
                }
            } 
        }
}

export const Level_Panel = connect(
    mapStateToProps,
    mapDispatchToProps
)(Panel_Display)