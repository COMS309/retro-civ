import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';

import { browserHistory } from 'react-router';
import { CREATORS as Data_Creators } from './../../../../../game/creators.js';
import { io_dispatch } from './../../../io/socket.js';
import { getHex, getName, getStats, getCity, getUnit, getBuilding } from './../h_functions.js';
import { idLookup } from './../../../../../game/h_functions.js';

import { SimpleStats, CompareStats } from './stat.jsx';

let name = "";

export const Build_Info = ({
    ConstructIt,
    builder,
    canBuild
}) => {
    return(
        <div className="col-1-4">
            <h3>{builder.builder} - {builder.level}</h3>
            {canBuild? (
                <button className="win-button" onClick={ConstructIt}>Build {builder.stats.build_remaining} cr</button>
            ) : (
                <label className="win-h5">Build {builder.stats.build_remaining} cr</label>
            )}
            <br/>
            <SimpleStats stats={builder.stats}/>
        </div>
    )
}
const Upgrade_Info = ({
    ConstructIt,
    builder,
    c_stats,
    canBuild
}) => {
    return(
        <div className="col-1-4">
            <h3>{builder.builder} - {builder.level}</h3>
            {canBuild? (
                <button className="win-button" onClick={ConstructIt}>Upgrade {builder.stats.build_remaining - c_stats.build_remaining} cr</button>
            ) : (
                <label className="win-h5">Upgrade {builder.stats.build_remaining - c_stats.build_remaining} cr</label>
            )}
            <br/>
            <CompareStats o_stats={c_stats} stats={builder.stats}/>
        </div>
    )
}

export const Creation_Presentation = ({
    obj_type,
    isBuild,
    ConstructIt,
    balance,
    b_collection,
    c_stats,
    onNameChange
}) => {

    if(isBuild){
        return(
            <section className="ms-grid">
                <section className="ms-row">
                    <div className="col-1-1">
                        <h1 className="win-h1">Build {obj_type}</h1>
                        <label className="win-h2">Name: </label><input type="text" id="objName" name="objName" className="win-textbox" onChange={onNameChange}/>
                    </div>
                </section>
                <section className="ms-row">
                    {Object.keys(b_collection).map(key => {
                        var builder = b_collection[key];
                        var canBuild = (builder.stats.build_remaining <= balance)
                        return (<Build_Info key={key} builder={builder} ConstructIt={ConstructIt(builder)} canBuild={canBuild}/>)
                            
                    })}
                </section>
            </section>
        )
    } else {
        return(
            <section className="ms-grid">
                <section className="ms-row">
                    <div className="col-1-1">
                        <h1 className="win-h1">Upgrade {obj_type}</h1>
                    </div>
                </section>
                <section className="ms-row">
                    {Object.keys(b_collection).map(key => {
                        var builder = b_collection[key];
                        var canBuild = ((builder.stats.build_remaining - c_stats.build_remaining) <= balance)
                        return ( <Upgrade_Info key={key} builder={builder} c_stats={c_stats} ConstructIt={ConstructIt(builder)} canBuild={canBuild}/>)
                    })}
                </section>
            </section>
        )
    }
}
const mapStateToProps = (
    state,
    props
) => {
    var selected = state.UI.selected;
    var targeted = state.UI.targeted;
    var s_hex = getHex(selected);
    var t_name = getName(getHex(targeted));
    var s_city = getCity(s_hex);
    var s_unit = getUnit(s_hex);
    var s_building = getBuilding(s_hex);
    var civ_name = state.USER.name;
    var civs = state.GAME.civs;
    var my_civ = idLookup(civs, civ_name)
    var builders = my_civ.builders;
    var balance = my_civ.coins.current; 
    
    var obj_type = props.params.obj;
    var action = props.params.action;
    
    var isBuild = (action == "build")? true : false;
    
    var b_collection, ConstructIt, c_stats;
    
    if(obj_type == "city") {
        b_collection = builders.cities
        c_stats = getStats(s_city)
        ConstructIt = isBuild? (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.unit.transform(s_unit, name, builder_data));
            }
        } : (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.city.upgrade(s_city, builder_data));
            }
        };
    }
    else if(obj_type == "unit"){
        if(t_name==null && !s_unit){
            t_name= s_hex.name;
        }
        b_collection = builders.units
        c_stats = getStats(s_unit)
        ConstructIt = isBuild? (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.unit.build(t_name, civ_name, name, builder_data));
            }
        } : (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.unit.upgrade(s_unit, builder_data));
            }
        };
    }
    else if(obj_type == "building"){
        b_collection = builders.buildings
        c_stats = getStats(s_building)
        ConstructIt = isBuild? (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.building.build(t_name, civ_name, name, builder_data));
            }
        } : (builder_data) => {
            return () => {
                io_dispatch(Data_Creators.building.upgrade(s_building, builder_data));
            }
        };
    }
    return {
        obj_type,
        isBuild,
        ConstructIt,
        balance,
        b_collection,
        c_stats,
        onNameChange: (event) => {
            name = event.target.value;
        }
    }
}
export const Creation_Panel = connect(
    mapStateToProps
)(Creation_Presentation)