import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';

import {browserHistory} from 'react-router';
import {CREATORS as Data_Creators} from './../../../../../game/creators.js';
import { io_dispatch } from './../../../io/socket.js';
import { CREATORS } from './../../../redux/creators.js';
import { basicDistance } from './../../../../../game/sub/h_distance.js';
import { getHex, getObj, getCity, getUnit, getBuilding, isMyCiv, getName } from './../h_functions.js';
import { idLookup } from './../../../../../game/h_functions.js';

import { SimpleStats, CompareStats } from './stat.jsx';

let myTurn = false;
let civNames = [];

let actions={
    "turn":"-1",
    "cities":[],
    "buildings":[],
    "units":[]
};
export const resetActions = (turn, name, civs) => {
    if (actions.turn!=turn && getIndex(name,civs)>-1){
        actions={
    "turn":"-1",
    "cities":[],
    "buildings":[],
    "units":[]
};
        actions.turn=turn;
        var civ=civs[getIndex(name,civs)];
        for(var i=0; i<civ.cities.length; i++){
            actions.cities.push({
                "name":civ.cities[i].name,
                "upgrade":"true",
                "build":"true"
            })
        }
        for(var i=0; i<civ.buildings.length; i++){
            actions.buildings.push({
                "name":civ.buildings[i].name,
                "upgrade":"true"
            })
        }
        for(var i=0; i<civ.units.length; i++){
            actions.units.push({
                "name":civ.units[i].name,
                "move":"true",
                "attack":"true"
            })
        }
    }
    
};


const canBuildIt = (name) => {
    var ind=getIndex(name, actions.units);
    if(ind<0){
        return false;
    }
    return (actions.units[ind].move && actions.units[ind].attack);
};


const canMoveIt = (name) => {
    var ind=getIndex(name, actions.units);
    if(ind<0){
        return false;
    }
    return actions.units[ind].move;
};

const MoveIt = (name) => {
    var ind=getIndex(name, actions.units);
    if(ind<0){
        return false;
    }
    actions.units[ind].move = false;
    return;
};

const canAttackIt = (name) => {
    var ind=getIndex(name, actions.units);
    if(ind<0){
        return false;
    }
    return actions.units[ind].attack;
};

const AttackIt = (name) => {
    var ind=getIndex(name, actions.units);
    if(ind<0){
        return false;
    }
    actions.units[ind].attack = false;
    return;
};

const getIndex = ( name, array) => {
    for(var i=0; i<array.length; i++){
        if(array[i].name==name){
            return i;
        }
    }
    return -1;
}

//HEX_PANEL




const Panel_Display = ({
    hex,
    hex_name,
    terrain_type,
    construct,
    turn,
    name,
    civs
}) => {
    resetActions(turn, name, civs);
    civNames = civs.map((civil)=> civil.name);
    return (
        <div>
            <h2 className="win-h2">Hex: {hex_name}</h2>
            <h3 className="win-h3">Terrain: {terrain_type}</h3>
            <CityDisplay city={getCity(hex)} construct={construct}/>
            <BuildingDisplay building={getBuilding(hex)} construct={construct}/>
            <br/>
            <UnitDisplay unit={getUnit(hex)} construct={construct}/>
        </div>
    )
};
const mapStateToProps = (
    state
) => {
    var selected = state.UI.selected;
    var civs = state.GAME.civs
    var name = state.USER.name;
    var myCiv = idLookup(civs, name);
    var hex, hex_name, terrain_type;
    if(selected) {
        hex = selected.hex;
        hex_name = hex.name;
        terrain_type = hex && hex.terrain.type
    }
    var turn = state.GAME.turn;
    myTurn = (name == civs[turn].name);
    return {
        hex,
        hex_name,
        terrain_type,
        turn,
        name,
        civs,
        construct: (obj, action) => {
            return () => {
                browserHistory.push('/game/construct/' + obj + '/' + action);
            }
        },
    }
};
export const Hex_Panel = connect(
    mapStateToProps
)(Panel_Display)

//CITY_PANEL
const CityPanel = ({
    city,
    isEmpty,
    hasTarget,
    construct,
    city_destroy,
    access,
    remove
}) => {
    if(!city)
        return ( <div></div> );
    // if(remove)
    //     return (<button className="win-button" onClick={city_destroy}>remove</button>);
    var name=city.name;
    if(name==""){
        name=city.builder;
    }
    if(!access || remove)
        return <h4 className="win-h4">City: {name} </h4>
    return (
        <div>
            <h4 className="win-h4">City: {name} </h4>
            <br/>
            <button className="win-button" onClick={city_destroy}>Destroy</button>
            <button className="win-button" onClick={construct('city','upgrade')}>Upgrade</button>
            <br/>
            { isEmpty? <button className="win-button" onClick={construct('unit','build')}>Create Unit</button> : undefined }
            { hasTarget? <button className="win-button" onClick={construct('building','build')}>Create Building</button> : undefined}
            <br/><br/>
            <SimpleStats stats={city.stats}/>
        </div>
    )
};
const mapStateToCity = (
    state,
    ownProps
) => {
    var selected = state.UI.selected;
    var targeted = state.UI.targeted
    var s_hex = getHex(selected);
    var t_hex = getHex(targeted);
    var s_unit = getUnit(s_hex);
    var s_city = getCity(s_hex);
    var hasTarget;
    if(t_hex) {
        hasTarget = ( basicDistance(getName(s_hex), getName(t_hex)) == 1 ) && !(t_hex.city || t_hex.building);
    }
    var name = state.USER.name;
    var access = false;
    var remove = false;
    if(s_city) {
        access = (s_city.civ == name) && myTurn;
        if(civNames.indexOf(s_city.civ)==-1){
       remove = true; 
    }
    }
    
    return {
        isEmpty: !s_unit,
        hasTarget,
        access,
        remove,
        city_destroy: () => {
            io_dispatch(Data_Creators.city.demolish(ownProps.city));
        }
    }
};
const CityDisplay = connect(
    mapStateToCity
)(CityPanel);

//BUILDING_PANEL
const BuildingPanel = ({
    building,
    construct,
    building_destroy,
    access,
    remove
}) => {
    if(!building)
        return ( <div></div> );
    // if(remove)
    //     return (<button className="win-button" onClick={building_destroy}>remove</button>);
    var name=building.name;
    if(building.name==""){
        name=building.builder;
    }
    if(!access || remove)
        return <h4 className="win-h4">Building: {name}</h4>
    return (
        <div>
            <h4 className="win-h4">Building: {name}</h4><br/>
            <br/>
            <button className="win-button" onClick={building_destroy}>Destroy</button>
            <button className="win-button" onClick={construct('building','upgrade')}>Upgrade</button>
            <br/><br/>
            <SimpleStats stats={building.stats}/>
        </div>
    )
};
const mapStateToBuilding = (
    state,
    ownProps
) => {
    var selected = state.UI.selected;
    var s_hex = getHex(selected);
    var s_building = getBuilding(s_hex);
    var name = state.USER.name;

    var access = false;
    var remove = false;
    if(s_building) {
        access = (s_building.civ == name) && myTurn;
        if(civNames.indexOf(s_building.civ)==-1){
       remove = true; 
    }
    }
    return {
        access,
        remove,
        building_destroy: () => {
            io_dispatch(Data_Creators.building.demolish(ownProps.building));
        }
    }
};
const BuildingDisplay = connect(
    mapStateToBuilding
)(BuildingPanel)

//UNIT_PANEL
const UnitPanel = ({
    unit,
    isEmpty,
    hasTargeted,
    canMove,
    isAttackable,
    construct,
    unit_destroy,
    unit_move,
    unit_attack,
    access,
    remove,
    t_remove,
    t_destroy
}) => {
    if(!unit)
        return ( <div></div> );
    // if(remove)
    //     return (<button className="win-button" onClick={unit_destroy}>remove</button>);
    
    var name = unit.name;
    if(name==""){
        name=unit.builder;
    }
    if(!access || remove)
        return <h4 className="win-h4">Unit: {name}</h4>;
        
    var moveable= canMove && canMoveIt(name);
    var attackable = isAttackable && canAttackIt(name);
    var buildable = isEmpty && canBuildIt(name);
    
    return ( 
        <div>
            <h4 className="win-h4">Unit: {name}</h4>
            <br/>
            <button className="win-button" onClick={unit_destroy}>Destroy</button>
            <button className="win-button" onClick={construct('unit','upgrade')}>Upgrade</button>
            <br/>
            {buildable? (<button className="win-button" onClick={construct('city','build')}>Create City</button>) : undefined }
            {hasTargeted ? (
                <div>
                {t_remove ? (<button className="win-button" onClick={t_destroy}>remove unit</button>) :
                    <div>
                    { moveable ? ( <button className="win-button" onClick={unit_move}>Move</button> ) : undefined }
                    { attackable ? ( <button className="win-button" onClick={unit_attack}>Attack Unit</button> ) : undefined }
                    </div>}
                </div>
            ):(<div></div>)}
            <br/><br/>
            <SimpleStats stats={unit.stats}/>
        </div>
    )
};
const mapStateToUnit = (
    state,
    ownProps
) => {
    var selected = state.UI.selected;
    var targeted = state.UI.targeted;
    var s_hex = getHex(selected);
    var s_unit = getUnit(s_hex);
    var s_obj = getCity(s_hex) || getBuilding(s_hex);
    var t_hex = getHex(targeted);
    var t_obj = getObj(t_hex);
    var t_isMyCiv = isMyCiv(ownProps.unit, t_obj)
    var canMove = !getUnit(t_hex) && t_isMyCiv;
    var isAttackable 
    if(ownProps.unit){
        isAttackable= t_obj && !t_isMyCiv && basicDistance(getName(s_hex), getName(t_hex)) <= ownProps.unit.stats.attack.range;
    }
    var name = state.USER.name;

    var access = false;
    var remove = false;
    var t_remove=false;
    if(s_unit) {
        access = (s_unit.civ == name) && myTurn;
        if(civNames.indexOf(s_unit.civ)==-1){
       remove = true; 
    }
    }
    if(isAttackable && civNames.indexOf(t_obj.civ)==-1){
       t_remove = true;
    }
    
    return {
        isEmpty: !s_obj,
        hasTargeted: t_hex? true : false,
        access,
        remove,
        t_remove,
        canMove,
        isAttackable,
        show_stats: () => {},
        unit_destroy: () => {
            io_dispatch(Data_Creators.unit.demolish(ownProps.unit));
        },
        t_destroy: () => {
            if(t_obj.obj_type == 'unit') {
                io_dispatch(Data_Creators.unit.demolish(t_obj));
            }
            else if(t_obj.obj_type == 'building') {
                io_dispatch(Data_Creators.building.demolish(t_obj));
            }
            else if(t_obj.obj_type == 'city') {
                io_dispatch(Data_Creators.city.demolish(t_obj));
            }
        },
        unit_move: () => {
            io_dispatch(Data_Creators.unit.move(ownProps.unit, t_hex.name));
            MoveIt(ownProps.unit.name);
        },
        unit_attack: () => {
            var attack_creator = null;
            if(t_obj.obj_type == 'unit') {
                io_dispatch(Data_Creators.unit.attack(s_unit, t_obj));
            }
            else if(t_obj.obj_type == 'building') {
                io_dispatch(Data_Creators.building.attack(s_unit, t_obj));
            }
            else if(t_obj.obj_type == 'city') {
                io_dispatch(Data_Creators.city.attack(s_unit, t_obj));
            }
            AttackIt(ownProps.unit.name);
        }
    }
};
const UnitDisplay = connect(
    mapStateToUnit
)(UnitPanel)
