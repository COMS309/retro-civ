import * as React from 'react';
import * as ReactWinJS from 'react-winjs';

export const CompareStats = ({
    o_stats,
    stats
}) => {
    return (
        <div>
            Health: {o_stats.hp.max} + {stats.hp.max - o_stats.hp.max} hp
            <br/>
            Speed: {o_stats.range * o_stats.speed} + {(stats.range * stats.speed) - (o_stats.range * o_stats.speed) } mtr/wk
            <br/>
            Attack Damage: {o_stats.attack.damage} + {stats.attack.damage - o_stats.attack.damage} hp
            <br/>
            Attack Range: {o_stats.attack.range} + {stats.attack.range - o_stats.attack.range} hex
            <br/>
            Defense: {o_stats.defense.block} + {stats.defense.block - o_stats.defense.block} hp
            <br/>
            Production: {stats.production} cr/wk
            <br/>
            Economy: {stats.production} %
            <br/>
            Research: {stats.production} rp/wk
        </div>
    )
}

export const SimpleStats = ({
    stats
}) => {
    return (
        <div>
            Health: {stats.hp.current} / {stats.hp.max} hp
            <br/>
            Speed: {stats.range * stats.speed} mtr/wk
            <br/>
            Attack Damage: {stats.attack.damage} hp
            <br/>
            Attack Range: {stats.attack.range} hex
            <br/>
            Defense: {stats.defense.block} hp
            <br/>
            Production: {stats.production} cr/wk
            <br/>
            Economy: {stats.production} %
            <br/>
            Research: {stats.production} rp/wk
        </div>
    )
}