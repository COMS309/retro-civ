import * as React from 'react';

export const Loading = () => {
    return (
        <h1 className="win-h1">Waiting for other Users</h1>
    )
} 

export const Victory = () => {
    return (
        <h1 className="win-h1">Congratulations, you win!</h1>
    )
} 

export const Loss = () => {
    return (
        <h1 className="win-h1">Sorry, you lost.</h1>
    )
} 