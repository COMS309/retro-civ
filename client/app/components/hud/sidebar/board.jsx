import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../../generic.jsx';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Hex_Panel, resetActions } from './../panels/hex_panel.jsx';

const Board_Sidebar_Presentation = ({
    panel,
    selected,
    turn,
    player
}) => {
    
        return (
            <div>
            
            <div style={{height:"100%"}}>
                <div style={{height:"400px"}}>
            {selected ? <Hex_Panel/>:<div></div>}
                </div>
                {panel}
            </div>
            </div>
        )
    
}

const mapStateToProps = (
    state
) => {
    
    var turn = state.GAME.turn
    var player=null;
    var civs=null;
    var name= state.USER.name;
    if(turn>-1){
        player=state.GAME.civs[turn].name;
        civs=state.GAME.civs;
    }
    if(turn && name && civs){
    resetActions(turn, name, civs);
    }
        return {
            selected: state.UI.selected,
            turn,
            player,
            civs,
            name
        }
}  

export const Board_Sidebar = connect(
    mapStateToProps
)(Board_Sidebar_Presentation)
