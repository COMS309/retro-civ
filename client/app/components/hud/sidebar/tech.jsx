import * as React from 'react';

export const Tech_Sidebar = ({
    panel
}) => (
    <div style={{height:"100%"}}>
        {panel}
    </div>
)