import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Auth_IO } from "./../../io.js"

let name = "";
const CreateAccount_View = ({
    onNameChange,
    onCreateAccount
}) => {
    return (
        <div>
            <h4 className="win-h4">Username: </h4>
            <input type="text" id="user" name="username" onChange={onNameChange} className="win-textbox"/><br/>
            <button type="button" className="win-button" onClick={onCreateAccount}>Create Account</button>
        </div>
    );
};

const mapStateToProps = (
    state
) => {
    return {
        //Add Something Here
    }
}
const mapDispatchToProps = (
    dispatch,
    ownProps
) => {
    return {
        onNameChange: (event) => {
            name = event.target.value;
        },
        onCreateAccount: () => Auth_IO.createAccount(dispatch, name, () => browserHistory.push(ownProps.redirect))
    }
}

const CreateAccount = connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateAccount_View);

module.exports = {
    CreateAccount
};