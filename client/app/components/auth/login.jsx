import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Auth_IO } from "./../../io"

let name = "";
let pass = "";
const Login_View = ({
    onNameChange,
    onPassChange,
    onLogin,
    onEnter,
    errorCode,
    errorMsg
}) => {
    return (
        <div>
            <label className="win-h4">Nickname: </label><input type="text" id="user" name="username" onChange={onNameChange} onKeyUp={onEnter} className="win-textbox"/><br/>
            <label className="win-h4">Passcode: </label><input type="text" id="user" name="password" onChange={onPassChange} onKeyUp={onEnter} className="win-textbox"/><br/>
            <button type="button" className="win-button" onClick={onLogin}>Login</button>
            <h3 className="win-h3">{errorCode}</h3>
            <h4 className="win-h4">{errorMsg}</h4>
        </div>
    );
};

const mapStateToProps = (
    state
) => {
    return {
        errorMsg: state.USER.errorMsg,
        errorCode: state.USER.errorCode
    }
}
const mapDispatchToProps = (
    dispatch,
    ownProps
) => {
    return {
        onNameChange: (event) => {
            name = event.target.value;
        },
        onPassChange: (event) => {
            pass = event.target.value;
        },
        onEnter: (e) => {
            if(e.keyCode == 13) {
                Auth_IO.login(dispatch, name, pass, () => browserHistory.push(ownProps.redirect));
            }
        },
        onLogin: () => Auth_IO.login(dispatch, name, pass, () => browserHistory.push(ownProps.redirect))
    }
}

const Login = connect(
    mapStateToProps,
    mapDispatchToProps
)(Login_View);

module.exports = {
    Login
};