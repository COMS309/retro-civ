import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Auth_IO } from "./../../io.js"

let name = "";
const Logout_View = ({
    loggedIn,
    onLogout
}) => {
    if (!loggedIn) {
        return (<div></div>);
    }
    return (
        <button type="button" className="win-button" onClick={onLogout}>Logout</button>
    )
}

const mapStateToProps = (
    state
) => {
    name = state.USER.name;
    return {
        loggedIn: state.USER.authToken? true: false
    };
}
const mapDispatchToProps = (
    dispatch,
    ownProps
) => {
    return {
        onLogout: () => Auth_IO.logout(dispatch, name, () => browserHistory.push(ownProps.redirect))
    }
}

const Logout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Logout_View);

module.exports = {
    Logout
};

