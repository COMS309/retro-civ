import * as React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { Auth_IO } from './../io.js'

let name = "";
const Header = ({
    app_name,
    loggedIn,
    onLogout,
    onClickNavigate
}) => {
    return (
        <div>
            <div className="header-img">
            </div>
            <div className="header">
                <h2 className="win-h2 title" onClick={() => onClickNavigate('/')}>{app_name}</h2>
                <div className="links">
                    <a className="link" href="#" onClick={e => onClickNavigate('/', e) }>Home</a>
                    {loggedIn?
                        (<a className="link" href="#" onClick={e => onLogout() }>Logout</a>):
                        (<a className="link" href="#" onClick={e => onClickNavigate('/login', e) }>Login</a>)}
                    <a className="link" href="#" onClick={e => onClickNavigate('/settings', e) }>Settings</a>
                </div>
            </div>
            {/**<div className="header-hero"></div>**/}
        </div>
    );
};
/*
    <ReactWinJS.Menu ref="menu">
        <ReactWinJS.Menu.Button
            label="Popout"/>
    </ReactWinJS.Menu>
*/

const mapStateToProps = (
    state
) => {
    name = state.USER.name;
    return {
        app_name: state.APP.name,
        loggedIn: state.USER.authToken? true: false
    }
}
const mapDispatchToProps = (
    dispatch
) => {
    return {
        onClickNavigate: (url, e) => {
            if (e != undefined) {
                e.preventDefault();
            }
            browserHistory.push(url);
        },
        onLogout: (e) => {
            if (e != undefined) {
                e.preventDefault();
            }
            Auth_IO.logout(dispatch, name, () => browserHistory.push("/"), () => browserHistory.push("/"));
        }
    };
};
const HeaderBar = connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

module.exports = {
    HeaderBar
};