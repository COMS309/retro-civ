import { Login } from './auth/login.jsx';
import { Logout } from './auth/logout.jsx';
import { CreateAccount } from './auth/createAccount.jsx';

module.exports = {
    Auth: {
        Login,
        Logout,
        CreateAccount
    }
}