import * as React from 'react';

//      PeopleList Presentation Component
const List = {
    create: (Presentation) => {
        return ({
            items,
            onClick
        }) => (
            <ul>
                {items.map(item =>
                    <Presentation
                        key={item.id}
                        {...item}
                        onClick={() => onClick(item.id)}
                    />
                    )
                }
            </ul>
        )
    }
}

module.exports = {
    List
}