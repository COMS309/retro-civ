import * as React from 'react';
import { connect } from 'react-redux';

const FilterLink = {
    mapStateToProps: (isActive) => {
        return (state, ownProps) => {
            return {
                active: isActive(state, ownProps)
            };
        }
    },
    mapDispatchToProps: (onClick) => {
        return (dispatch, ownProps) => {
            return {
                onClick: onClick(dispatch, ownProps)
            };
        }
    },
    create: (isActive, onClick, presentation) => {
        return connect(
            FilterLink.mapStateToProps(isActive),
            FilterLink.mapDispatchToProps(onClick)
        )(presentation);
    }
}

module.exports = {
    FilterLink
}