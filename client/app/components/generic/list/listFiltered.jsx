import * as React from 'react';
import { connect } from 'react-redux';

const ListFiltered = {
    mapStateToProps: (filter) => {
        return (state) => {
            return {
                items: filter(state)
            }
        }
    },
    mapDispatchToProps: (onClick) => {
        return (state) => {
            return {
                onClick: (id) => {
                    if(onClick) {
                        onClick(dispatch,id);
                    }
                }
            }
        }
    },
    create: (filter, onClick, presentation) => {
        return connect(
            ListFiltered.mapStateToProps(filter),
            ListFiltered.mapDispatchToProps(onClick)
        )(presentation)
    }
}

//      Exports
module.exports = {
    ListFiltered,
}