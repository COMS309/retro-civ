import * as React from 'react';

const ownProps = (theme) => {
    return {
        type: 'UPDATE_APP_THEME',
        theme
    }
}
const RadioButton = ({
    checked,
    onClick,
    label
}) => {
    return (
        <span>
            <input
                className="win-radio"
                type="radio"
                name="placement"
                checked={checked}
                onChange={onClick}/>
            <label>{label}</label>
            <br />
        </span>
    )
}

module.exports = {
    RadioButton
}