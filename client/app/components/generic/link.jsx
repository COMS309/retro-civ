import * as React from 'react';
//      Link Presentation Component
const Link = ({
    active,
    children,
    onClick,
    filter
}) => {
    if (active) {
        return <span>{children}</span>
    }
    return (
        <a href='#'
            onClick={e => {
                e.preventDefault();
                onClick(filter);
            } }
            >
            {children}
        </a>
    );
};

module.exports = {
    Link
}