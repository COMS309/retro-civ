import * as React from 'react';
import * as ReactWinJS from 'react-winjs';

export const CenterBox = ({children}) => (
    <div style={{height: "100%", width: "100%", display: "table"}}>
        <div style={{display: "table-cell", verticalAlign: "middle"}}>
            <div style={{margin:"auto",width:"auto",textAlign: "center"}}>
                {children}
            </div>
        </div>
    </div>
)