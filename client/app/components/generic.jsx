import { CenterBox } from './generic/centerBox.jsx';
import { RadioButton } from './generic/radioButton.jsx';
import { List } from './generic/list.jsx';
import { Link } from './generic/link.jsx';

module.exports = {
    CenterBox,
    RadioButton,
    List,
    Link
};