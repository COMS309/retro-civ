import { RadioButton } from './generic.jsx';
import { connect } from 'react-redux';
import { CREATORS } from './../redux/creators.js';

const mapStateToProps = (
    state,
    ownProps
) => {
    return {
        checked: ownProps.theme === state.APP.theme
    };
}
const mapDispatchToProps = (
    dispatch,
    ownProps
) => {
    return {
        onClick: () => {
            dispatch(CREATORS.APP.setTheme(ownProps.theme));
        }
    };
}
const ThemeRadioButton = connect(
    mapStateToProps,
    mapDispatchToProps
)(RadioButton)

module.exports = {
    ThemeRadioButton
}