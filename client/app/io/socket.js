import io from 'socket.io-client';
import { CREATORS } from './../redux/creators.js';
import { CREATORS as Data_Creator } from './../../../game/creators';
import { browserHistory } from 'react-router';

let socket = null;
let store = null;
let parent = 0;
let user = null;

export const registerStore = (myStore) => {
    store = myStore;
}
export const connect = (me, game) => {
    user = me
    socket = io('/'+game);
    socket.on('action', (action_card) => {
        console.log('received action_card', action_card.msg.action.type, action_card);
        parent = action_card.hash;
        store.dispatch(action_card.msg.action);
        if(store.getState().UI.selected) {
            store.dispatch(CREATORS.UI.deselect());
        }
    });
    socket.on('action_group', (action_group) => {
        action_group.map(action_card => {
            parent = action_card.hash;
            store.dispatch(action_card.msg.action);
        })
    })
    socket.on('suggest', action => {
        switch(action) {
            case 'refresh':
                store.dispatch(Data_Creator.reset());
                socket.emit('refresh', user);
                break;
            case 'update':
                socket.emit('update', parent);
                break;
        }
    });
    socket.emit('refresh', user);
}
export const io_dispatch = (action) => {
    var action_card = {
        msg: {
            user,
            action
        },
        parent
    }
    socket.emit('action', action_card);
    store.dispatch(CREATORS.UI.deselect());
    browserHistory.push('/game/board');
}
