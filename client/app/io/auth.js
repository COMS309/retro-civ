import * as $ from 'jquery';
import { CREATORS } from './../redux/creators.js';
import { connect } from './socket.js';
export const login = (dispatch, name, pass, success, failure) => {
    $.ajax({
        url: '/login',
        dataType: 'json',
        cache: false,
        data: {
            name,
            pass
        },
        success: (data) => {
            dispatch(CREATORS.USER.login(data));
            connect(data.name, data.game);
            success? success(): null;
        },
        error: (xhr, status, err) => {
            console.log(xhr, status, err);
            dispatch(CREATORS.USER.authFail(xhr.responseJSON.msg, err));
            failure? failure(): null;
        }
    });
}

export const logout = (dispatch, name, success, failure) => {
    $.ajax({
        url: '/logout/',
        dataType: 'json',
        type: 'POST',
        data: {
            name
        },
        success: () => {
            dispatch(CREATORS.USER.logout());
            success? success(): null;
        },
        error: () => {
            dispatch(CREATORS.USER.logout());
            failure? failure(): null;
        }
    });
}

export const createAccount = (dispatch, name, success, failure) => {
    $.ajax({
        url: '/createUser',
        dataType: 'json',
        type: 'POST',
        data: {
            username: ownProps.name
        },
        success: (data) => {
            dispatch(CREATORS.USER.login(data));
            success? success(): null;
        },
        error: (xhr, status, err) => {
            failure? failure(): null;
        }
    });
}

export default {
    login,
    logout,
    createAccount
}