import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../components/generic.jsx';
import { ThemeRadioButton } from './../components/themeRadioButton.jsx';

var Settings = React.createClass({
    render: function(){
        return (
            <CenterBox>
                <h2 className="win-h2">Settings</h2>
                <div>
                    <ThemeRadioButton theme="light" label="Light"/>
                    <ThemeRadioButton theme="dark" label="Dark"/>
            </div>
            </CenterBox>
        )
    }
});

module.exports = {
    Settings
};