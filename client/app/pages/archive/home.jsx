import * as React from 'react';
import * as ReactWinJS from 'react-winjs' ;
import { CenterBox } from './../components/generic.jsx';
var browserHistory = require('react-router').browserHistory;

var Home = React.createClass({
    handleLogin: function(){
        browserHistory.push('/login');
    },
    render: function(){
        return (
            <CenterBox>
                <h2 className="win-h2">Welcome to Retro-Civ</h2>
                <br/>
                <button className="win-button" onClick={this.handleLogin}>Sign-In</button>
            </CenterBox>
        )
    }
});

module.exports = {
    Home
};
