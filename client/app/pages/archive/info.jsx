import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../components/generic.jsx';
import { browserHistory } from 'react-router';
import { TECH_TREE } from './../../../game/sub/tech_tree.js';

var i = 0;
var Info = React.createClass({
    //jsonContents: fs.readFileSync("testJson.json"),
    handleNavigate: function(){
        browserHistory.push('/game');
    },
    handleShowDialog: function(name) {
        console.log(this.refs[name]);
        this.refs[name].winControl.show().then(function (eventObject) {
            this.setState({ dialogResult: eventObject.result });
        }.bind(this));
    },
    getInitialState: function () {
        return {
            dialogResult: null
        };
    },
    handleChangeRating: function (eventObject) {
        var ratingControl = eventObject.currentTarget.winControl;
        this.setState({ rating: ratingControl.userRating });
    },
    handleAddToRating: function (amount) {
        this.setState({ rating: this.state.rating + amount });
    },
    getInitialState: function () {
        return {
            rating: 0
        };
    },
    render: function(){  
        return (
            <CenterBox id="home-page">
                <ReactWinJS.ContentDialog
                    ref="popBonus"
                    title="Population Bonuses"
                    primaryCommandText="Close" >
                    <div>
                        Bonuses gained from building cities and other buildings that increase population size.
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="cityList"
                    title="City List"
                    primaryCommandText="Close" >
                    <div>
                        List of the players current cities.
                        <br/><br/>
                        <td><button className="win-button">City {1}</button></td><br/>
                        <td><button className="win-button">City {2}</button></td>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="buildCity"
                    title="Build Cities"
                    primaryCommandText="Close" >
                    <div>
                        List of avaliable cities a player can build.
                        <br/><br/>
                        <td><button className="win-button">City {1}</button></td><br/>
                        <td><button className="win-button">City {2}</button></td>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="buildBuilding"
                    title="Build Buildings"
                    primaryCommandText="Close" >
                    <div>
                        List of avaliable buildings a player can build.
                        <br/><br/>
                        <td><button className="win-button">Building {1}</button></td><br/>
                        <td><button className="win-button">Building {2}</button></td>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="population"
                    title="Population"
                    primaryCommandText="Close" >
                    <div>
                        Current Population
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                <ReactWinJS.ContentDialog
                    ref="unitList"
                    title="Unit List"
                    primaryCommandText="Close" >
                    <div>
                        List of avaliable units a player can build.
                        <br/><br/>
                        <td><button className="win-button">Unit {1}</button></td><br/>
                        <td><button className="win-button">Unit {2}</button></td>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="buildUnit"
                    title="Build Units"
                    primaryCommandText="Close" >
                    <div>
                        List of avaliable units a player can build.
                        <br/><br/>
                        <td><button className="win-button">Unit {1}</button></td><br/>
                        <td><button className="win-button">Unit {2}</button></td>
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                <ReactWinJS.ContentDialog
                    ref="resBonus"
                    title="Research Bonuses"
                    primaryCommandText="Close" >
                    <div>
                        Bonuses gained from building cities and other buildings that increase research output.
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                <ReactWinJS.ContentDialog
                    ref="ecoBonus"
                    title="Economic Bonuses"
                    primaryCommandText="Close" >
                    <div>
                        Bonuses gained from building cities and other buildings that increase economic output.
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="balance"
                    title="Balance"
                    primaryCommandText="Close" >
                    <div>
                        Current Economic Balance
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                
                
                <ReactWinJS.ContentDialog
                    ref="military"
                    title="Military"
                    primaryCommandText="Close" >
                    <div>
                        The military branch is used to advance unit building.
                        Advance this tree for increased firepower.
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="manufacturing"
                    title="Manufacturing"
                    primaryCommandText="Close" >
                    <div>
                        The manufacturing branch is used to advance building building. 
                        Advance this tree for increased resources and reasearch capabilities.
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="city-header"
                    title="City"
                    primaryCommandText="Close" >
                    <div>
                        The city branch is used to advance city building. 
                        Advance this tree for increased population sizes and economic benefits.
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                
                
                
                <ReactWinJS.ContentDialog
                    ref="unit0"
                    title={TECH_TREE.military[0].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.military[0].description}
                        <br/><br/>
                        Units Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[0].units[0].stats.rank} {TECH_TREE.military[0].units[0].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="unit1"
                    title={TECH_TREE.military[1].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.military[1].description}
                        <br/><br/>
                        Units Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[1].units[0].stats.rank} {TECH_TREE.military[1].units[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[1].units[1].stats.rank} {TECH_TREE.military[1].units[1].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="unit2"
                    title={TECH_TREE.military[2].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.military[2].description}
                        <br/><br/>
                        Units Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[2].units[0].stats.rank} {TECH_TREE.military[2].units[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[2].units[1].stats.rank} {TECH_TREE.military[2].units[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[2].units[2].stats.rank} {TECH_TREE.military[2].units[2].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="unit3"
                    title={TECH_TREE.military[3].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.military[3].description}
                        <br/><br/>
                        Units Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[3].units[0].stats.rank} {TECH_TREE.military[3].units[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[3].units[1].stats.rank} {TECH_TREE.military[3].units[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[3].units[2].stats.rank} {TECH_TREE.military[3].units[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[3].units[3].stats.rank} {TECH_TREE.military[3].units[3].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="unit4"
                    title={TECH_TREE.military[4].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.military[4].description}
                        <br/><br/>
                        Units Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[4].units[0].stats.rank} {TECH_TREE.military[4].units[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[4].units[1].stats.rank} {TECH_TREE.military[4].units[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[4].units[2].stats.rank} {TECH_TREE.military[4].units[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[4].units[3].stats.rank} {TECH_TREE.military[4].units[3].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.military[4].units[4].stats.rank} {TECH_TREE.military[4].units[4].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                
                
                
                <ReactWinJS.ContentDialog
                    ref="building0"
                    title={TECH_TREE.manufacturing[0].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.manufacturing[0].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[0].buildings[0].stats.rank} {TECH_TREE.manufacturing[0].buildings[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[0].buildings[1].stats.rank} {TECH_TREE.manufacturing[0].buildings[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[0].buildings[2].stats.rank} {TECH_TREE.manufacturing[0].buildings[2].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="building1"
                    title={TECH_TREE.manufacturing[1].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.manufacturing[1].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[1].buildings[0].stats.rank} {TECH_TREE.manufacturing[1].buildings[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[1].buildings[2].stats.rank} {TECH_TREE.manufacturing[1].buildings[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[1].buildings[3].stats.rank} {TECH_TREE.manufacturing[1].buildings[3].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[1].buildings[4].stats.rank} {TECH_TREE.manufacturing[1].buildings[4].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[1].buildings[5].stats.rank} {TECH_TREE.manufacturing[1].buildings[5].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="building2"
                    title={TECH_TREE.manufacturing[2].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.manufacturing[2].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[0].stats.rank} {TECH_TREE.manufacturing[2].buildings[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[1].stats.rank} {TECH_TREE.manufacturing[2].buildings[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[3].stats.rank} {TECH_TREE.manufacturing[2].buildings[3].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[4].stats.rank} {TECH_TREE.manufacturing[2].buildings[4].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[5].stats.rank} {TECH_TREE.manufacturing[2].buildings[5].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[2].buildings[6].stats.rank} {TECH_TREE.manufacturing[2].buildings[6].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="building3"
                    title={TECH_TREE.manufacturing[3].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.manufacturing[3].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[0].stats.rank} {TECH_TREE.manufacturing[3].buildings[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[1].stats.rank} {TECH_TREE.manufacturing[3].buildings[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[3].stats.rank} {TECH_TREE.manufacturing[3].buildings[3].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[4].stats.rank} {TECH_TREE.manufacturing[3].buildings[4].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[5].stats.rank} {TECH_TREE.manufacturing[3].buildings[5].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[3].buildings[6].stats.rank} {TECH_TREE.manufacturing[3].buildings[6].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="building4"
                    title={TECH_TREE.manufacturing[4].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.manufacturing[4].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[0].stats.rank} {TECH_TREE.manufacturing[4].buildings[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[1].stats.rank} {TECH_TREE.manufacturing[4].buildings[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[2].stats.rank} {TECH_TREE.manufacturing[4].buildings[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[4].stats.rank} {TECH_TREE.manufacturing[4].buildings[4].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[5].stats.rank} {TECH_TREE.manufacturing[4].buildings[5].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[6].stats.rank} {TECH_TREE.manufacturing[4].buildings[6].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.manufacturing[4].buildings[7].stats.rank} {TECH_TREE.manufacturing[4].buildings[7].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                
                
                
                
                <ReactWinJS.ContentDialog
                    ref="city0"
                    title={TECH_TREE.city[0].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.city[0].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[0].cities[0].stats.rank} {TECH_TREE.city[0].cities[0].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="city1"
                    title={TECH_TREE.city[1].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.city[1].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[1].cities[0].stats.rank} {TECH_TREE.city[1].cities[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[1].cities[1].stats.rank} {TECH_TREE.city[1].cities[1].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="city2"
                    title={TECH_TREE.city[2].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.city[2].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[2].cities[0].stats.rank} {TECH_TREE.city[2].cities[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[2].cities[1].stats.rank} {TECH_TREE.city[2].cities[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[2].cities[2].stats.rank} {TECH_TREE.city[2].cities[2].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="city3"
                    title={TECH_TREE.city[3].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.city[3].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[3].cities[0].stats.rank} {TECH_TREE.city[3].cities[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[3].cities[1].stats.rank} {TECH_TREE.city[3].cities[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[3].cities[2].stats.rank} {TECH_TREE.city[3].cities[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[3].cities[3].stats.rank} {TECH_TREE.city[3].cities[3].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <ReactWinJS.ContentDialog
                    ref="city4"
                    title={TECH_TREE.city[4].name}
                    primaryCommandText="Close" >
                    <div>
                        {TECH_TREE.city[4].description}
                        <br/><br/>
                        Buidlings Avaliable
                        <br/><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[4].cities[0].stats.rank} {TECH_TREE.city[4].cities[0].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[4].cities[1].stats.rank} {TECH_TREE.city[4].cities[1].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[4].cities[2].stats.rank} {TECH_TREE.city[4].cities[2].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[4].cities[3].stats.rank} {TECH_TREE.city[4].cities[3].stats.name}
                        </button><br/>
                        <button className="win-button">
                            level {TECH_TREE.city[4].cities[4].stats.rank} {TECH_TREE.city[4].cities[4].stats.name}
                        </button><br/>
                    </div>
                </ReactWinJS.ContentDialog>
                
                <h1 className="win-h1">Game Management</h1>
                            
                <ReactWinJS.Pivot className="win-type-body" style={{height: "30%"}}>
                
                    <ReactWinJS.Pivot.Item key="itemA" header="Settlement">
                        <div>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"popBonus")}>Bonuses</button>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"cityList")}>Cities List</button> 
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"buildCity")}>Build Cities</button> 
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"buildBuilding")}>Build Buildings</button>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"population")}>Population</button>  
                        </div>
                    </ReactWinJS.Pivot.Item>
                    
                    <ReactWinJS.Pivot.Item key="itemA" header="Military">
                        <div>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"unitList")}>Units List</button> 
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"buildUnit")}>Build Units</button> 
                        </div>
                    </ReactWinJS.Pivot.Item>
                
                    <ReactWinJS.Pivot.Item key="itemB" header="Technology">
                        <div>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"resBonus")}>Bonuses</button>
                        </div>
                    </ReactWinJS.Pivot.Item>
                
                    <ReactWinJS.Pivot.Item key="itemC" header="Economy">
                        <div>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"ecoBonus")}>Bonuses</button>
                            <button className="win-button" onClick={this.handleShowDialog.bind(null,"balance")}>Balance</button>
                        </div>
                    </ReactWinJS.Pivot.Item>
                </ReactWinJS.Pivot>
                <br/><br/>
                <h3 className="win-h3">Progression</h3>
                <br/>
                <center>
                <table>
                <tr>
                    <td></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"military")}>Military</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"manufacturing")}>Manufacturing</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city-header")}>City</button></td>
                </tr>
                
                <tr>
                    <td><button className="win-button">Level {1}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"unit0")}>{TECH_TREE.military[0].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"building0")}>{TECH_TREE.manufacturing[0].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city0")}>{TECH_TREE.city[0].name}</button></td>
                </tr>
                <tr>
                    <td><button className="win-button">Level {2}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"unit1")}>{TECH_TREE.military[1].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"building1")}>{TECH_TREE.manufacturing[1].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city1")}>{TECH_TREE.city[1].name}</button></td>
                </tr>
                <tr>
                    <td><button className="win-button">Level {3}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"unit2")}>{TECH_TREE.military[2].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"building2")}>{TECH_TREE.manufacturing[2].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city2")}>{TECH_TREE.city[2].name}</button></td>
                </tr>
                <tr>
                    <td><button className="win-button">Level {4}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"unit3")}>{TECH_TREE.military[3].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"building3")}>{TECH_TREE.manufacturing[3].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city3")}>{TECH_TREE.city[3].name}</button></td>
                </tr>
                <tr>
                    <td><button className="win-button">Level {5}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"unit4")}>{TECH_TREE.military[4].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"building4")}>{TECH_TREE.manufacturing[4].name}</button></td>
                    <td><button className="win-button" onClick={this.handleShowDialog.bind(null,"city4")}>{TECH_TREE.city[4].name}</button></td>
                </tr>
                </table>
                </center>

                <br/>
                <button className="win-button" onClick={this.handleNavigate}>Back</button>
            </CenterBox>
            
        )
    }
})


module.exports = {
    Info
}