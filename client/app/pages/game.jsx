import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { CenterBox } from './../components/generic.jsx';
import { browserHistory } from 'react-router';
import { Menu } from './../components/hud/toolbar/menu.jsx';
import { Loading, Victory, Loss } from './../components/hud/loading.jsx';
import { connect } from 'react-redux';

const Game_Pane = ({ main, sidebar, gameOver, winner, started }) => {
    if(!started) {
        return (
            <section className="ms-row" style={{height:"99%"}}>
                <div className="col-3-4" style={{height:"99%"}}>
                    <section className="ms-row" style={{height:"calc(100% - 30px)"}}>
                        <CenterBox>
                            <Loading/>
                        </CenterBox>
                    </section>
                    <section className="ms-row" style={{height:"30px"}}>
                        <div className="col-1-1">
                            <Menu/>
                        </div>
                    </section>
                </div>
                <div className="col-1-4" style={{height:"99%",backgroundColor:"gray"}}>
                </div>
            </section>
        )
    }else if(!gameOver){
    return (
        <section className="ms-row" style={{height:"99%"}}>
            <div className="col-3-4" style={{height:"99%"}}>
                <section className="ms-row" style={{height:"calc(100% - 30px)"}}>
                    <CenterBox>
                        {main}
                    </CenterBox>
                </section>
                <section className="ms-row" style={{height:"30px"}}>
                    <div className="col-1-1">
                        <Menu/>
                    </div>
                </section>
            </div>
            <div className="col-1-4" style={{height:"99%",backgroundColor:"gray"}}>
                {sidebar}
            </div>
        </section>
    )}
    else if(winner){
        return (
            <section className="ms-row" style={{height:"99%"}}>
            <div className="col-3-4" style={{height:"99%"}}>
                <section className="ms-row" style={{height:"calc(100% - 30px)"}}>
                    <CenterBox>
                        <Victory/>
                    </CenterBox>
                </section>
                <section className="ms-row" style={{height:"30px"}}>
                    <div className="col-1-1">
                        <Menu/>
                    </div>
                </section>
            </div>
            <div className="col-1-4" style={{height:"99%",backgroundColor:"gray"}}>
                {sidebar}
            </div>
        </section>
        )
    }
    return (
        <section className="ms-row" style={{height:"99%"}}>
            <div className="col-3-4" style={{height:"99%"}}>
                <section className="ms-row" style={{height:"calc(100% - 30px)"}}>
                    <CenterBox>
                        <Loss/>
                    </CenterBox>
                </section>
                <section className="ms-row" style={{height:"30px"}}>
                    <div className="col-1-1">
                        <Menu/>
                    </div>
                </section>
            </div>
            <div className="col-1-4" style={{height:"99%",backgroundColor:"gray"}}>
                {sidebar}
            </div>
        </section>
    )
}
const mapStateToProps = (
    state
) => {
    var winner=false;
    var gameOver=(state.GAME.civs.length==1);
    if(gameOver && state.USER.name==state.GAME.civs[0].name){
       winner=true; 
    }
    return {
        gameOver,
        winner,
        started: state.GAME.turn !== -1
    }
}
export const Game = connect(
    mapStateToProps
)(Game_Pane);