import * as React from 'react';
import * as ReactWinJS from 'react-winjs';
import { Auth } from './../components/auth.jsx'
import { CenterBox } from './../components/generic.jsx';
import { browserHistory } from 'react-router';

const Login = () => {
    return (
        <CenterBox>
            <font size="10" color="#9900cc" font-weight="bold" >Retro-Civ</font>
            <Auth.Login redirect="/game" name=""/>
        </CenterBox>
    );
};

module.exports = {
    Login
}
