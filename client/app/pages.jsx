import { Game } from './pages/game.jsx';
import { Login } from './pages/login.jsx';
import { Settings } from './pages/settings.jsx';

module.exports = {
    Game,
    Login,
    Settings
}