/// <reference path="../../typings/tsd.d.ts" />

// IMPORT 3rd Party Dependencies
//      React
import * as React from 'react';
import * as ReactDOM from 'react-dom';
//      Redux
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
//      Router
import { Router, Route, IndexRoute, IndexRedirect, Link, browserHistory, hashHistory } from 'react-router';
import { routerMiddleware, syncHistoryWithStore, routerReducer } from 'react-router-redux';
//      WinJs
import * as ReactWinJS from 'react-winjs';
//      Socket.IO
import { registerStore } from './io/socket.js';
// SETUP Redux and Router Hooks
//      Load reducers
import { CREATORS }    from './redux/creators.js';
import { REDUCERS as APP }    from './redux/sub/app.js';
import { REDUCERS as USER }    from './redux/sub/user.js';
import { REDUCERS as UI }    from './redux/sub/ui.js';
import { SUBSCRIBERS } from './redux/subscribers.js';
import { REDUCERS as GAME } from './../../game/reducers.js';
import { CREATORS as Data_Creators } from './../../game/creators.js';
//      Load Redux Routing Middleware
const middleware = routerMiddleware(browserHistory)
const appReducers = combineReducers(
    Object.assign(
        {}, 
        APP,
        USER,
        UI,
        GAME,
        { routing: routerReducer })
    // ES2017 {...Reducers, routing: routerReducer}
);
//      Create Redux Store
const store = createStore(
    appReducers,
    applyMiddleware(middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)

registerStore(store);

import { TERRAIN } from './../../game/sub/terrain.js';
import { units } from './../../game/sub/TECH_TREE/obj/units.js';

store.subscribe(() => SUBSCRIBERS.APP.updateTheme(store.getState().APP.theme));
setTimeout(
    () => {
        store.dispatch(CREATORS.APP.initTheme());
        store.dispatch(CREATORS.APP.setName("Retro-Civ"));
    },
    100
)

// SETUP React App
//      Top Level Design
import { HeaderBar } from "./components/header.jsx";
 
const App = ({ children }) => {
    return(
        <div>
            <div className="app-content">
                {/**For Menu**/}
                {/**<HeaderBar />**/}
                <section className="ms-grid full" style={{height: "100%"}}>
                    {children}
                </section>
            </div>
        </div>
    )
}

// SETUP Router
//      Load Subpages of React App
import { Settings, Login, Game } from './pages.jsx';
import { Board } from './components/hud/board.jsx';
import { Board_Sidebar } from './components/hud/sidebar/board.jsx';
import { Tech } from './components/hud/tech.jsx';
import { Tech_Sidebar } from './components/hud/sidebar/tech.jsx';
import { Instructions } from './components/hud/instructions.jsx';
import { Civ } from './components/hud/civ.jsx';

import { Level_Panel } from './components/hud/panels/level_panel.jsx';
import { Object_Panel } from './components/hud/panels/object_panel.jsx';
import { Hex_Panel } from './components/hud/panels/hex_panel.jsx';
import { Creation_Panel } from './components/hud/panels/creation_panel.jsx';
//      Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);
//      Render to DOM
ReactDOM.render((
    <Provider store={store}>
        <Router history={history}>
        <Route path="/" component={App}>
            <IndexRedirect to="login"/>
            <Route path="game" component={Game}>
                <IndexRedirect to="board"/>
                <Route      path="board"                    components={{main:Board, sidebar:Board_Sidebar}}>
                    <Route  path="city/:stats"              components={{panel:Object_Panel}}/>
                    <Route  path="unit/:stats"              components={{panel:Object_Panel}}/>
                </Route>
                <Route      path="construct/:obj/:action"   components={{main:Creation_Panel, sidebar:Board_Sidebar}}/>
                <Route      path="tech"                     components={{main:Tech , sidebar:Tech_Sidebar}}>
                    <Route  path=":tree/:level"             components={{subpane:Level_Panel}}/>
                    <Route  path=":tree/:level/:builder"    components={{subpane:Level_Panel, panel:Object_Panel}}/>
                </Route>
                <Route      path="civ"                      components={{main:Civ}}/>
                <Route      path="instructions"             components={{main:Instructions}}/>
            </Route>
            <Route path="settings" component={Settings}/>
            <Route path="login" component={Login}/>
        </Route>
    </Router>
    </Provider>
), document.body);