export class DeprecatedActionError extends Error {
    constructor(action = "action", ref, alt) {
        super();
        this.name = "DeprecatedActionError";
        this.message = "Deprecated Action: " + action + " is used" + (ref? " in " + ref: "") + ";" + (alt? " Replace With:" + alt : "");
    }
}

export class LookupError extends Error {
    constructor(msg) {
        super();
        this.name = "LookupError";
        this.mesage = msg;
    }
}

export class ExistsError extends Error {
    constructor(msg) {
        super();
        this.name = "ExistsError";
        this.mesage = msg;
    }
}

export class CivilizationMismatchError extends Error {
    constructor(msg) {
        super();
        this.name = "CivilizationMismatchError";
        this.mesage = msg;
    }
}

export class ImplicitActionError extends Error {
    constructor(action, ref) {
        super("Must Explicitly Declare the behavior of \"" + action + "\" in " + ref);
        this.name = "ImplicitActionError";
        this.mesage = "Must Explicitly Declare the behavior of \"" + action + "\" in " + ref;
    }
}