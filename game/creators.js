import { CREATORS as hex } from './sub/hex.js';
import { CREATORS as civ } from './sub/civ.js';
import { CREATORS as city } from './sub/city.js';
import { CREATORS as building } from './sub/building.js';
import { CREATORS as unit } from './sub/unit.js';
import { CREATORS as tech } from './sub/tech.js';
import { CREATORS as turn } from './sub/turn.js';
import { ACTION_TYPE } from './action_type.js';

const reset = () => {
    return {
        type: ACTION_TYPE.RESET
    }
}

const name = {
    set: (name) => {
        return {
            type: ACTION_TYPE.NAME.SET,
            name
        }
    }
}
//CREATORS
export const CREATORS = {
    turn,
    hex,
    civ,
    city,
    building,
    unit,
    tech,
    name,
    reset
}
