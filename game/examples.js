        store.dispatch(Creators.APP.initTheme());
        store.dispatch(Creators.APP.setName("Retro-Civ"));
        //Testing HEX ADD
        store.dispatch(Data_Creators.hex.add("1_1_1"));
        store.dispatch(Data_Creators.hex.add("1_2_1"));
        store.dispatch(Data_Creators.hex.add("1_3_1"));
        store.dispatch(Data_Creators.hex.add("1_4_1"));
        store.dispatch(Data_Creators.hex.add("1_5_1"));
        store.dispatch(Data_Creators.hex.add("1_6_1"));
        store.dispatch(Data_Creators.hex.add("1_7_1"));
        store.dispatch(Data_Creators.hex.add("1_8_1"));
        store.dispatch(Data_Creators.hex.add("2_1_1"));
        store.dispatch(Data_Creators.hex.add("2_2_1"));
        store.dispatch(Data_Creators.hex.add("2_1_2"));
        //TESTING HEX REMOVE
        store.dispatch(Data_Creators.hex.remove("1_8_1"));
        
        store.dispatch(Data_Creators.state.server());
        //TESTING CIV ADD
        store.dispatch(Data_Creators.civ.add("r")); //civ[0]
        store.dispatch(Data_Creators.civ.add("w")); //civ[1]
        store.dispatch(Data_Creators.civ.add("z")); //civ[2]
        //TESTING CIV REMOVE
        store.dispatch(Data_Creators.civ.remove("z"));
        /*
        //TESTING CITIES ADD
        store.dispatch(Data_Creators.city.add("1_1_1","r","London"));
        store.dispatch(Data_Creators.city.add("1_3_1","r","Brussels"));
        store.dispatch(Data_Creators.city.add("1_2_1","w","Paris"));
        //TESTING BUILDINGS ADD
        store.dispatch(Data_Creators.building.add("1_4_1","r","Tower of London"));
        store.dispatch(Data_Creators.building.add("1_5_1","r","Coover"));
        store.dispatch(Data_Creators.building.add("1_6_1","r","Eifel Tower"));
        store.dispatch(Data_Creators.building.add("2_2_1","w","Durham"));
        //TESTING UNITS ADD
        store.dispatch(Data_Creators.unit.add("1_3_1","r","Winston Churchill"));
        store.dispatch(Data_Creators.unit.add("1_3_1","r","Chance the Rapper"));
        store.dispatch(Data_Creators.unit.add("1_4_1","r","Queen Victoria"));
        store.dispatch(Data_Creators.unit.add("1_2_1","w","Thomas Edison"));
        store.dispatch(Data_Creators.unit.add("2_1_2","w","Rae Stremmurd", null, 0, null));
        //TESTING CITY REMOVE
        store.dispatch(Data_Creators.city.remove("1_1_1","r","London"));
        //TESTING BUILDING REMOVE
        store.dispatch(Data_Creators.building.remove("1_4_1","r","Eifel Tower"));
        //TESTING UNIT REMOVE
        store.dispatch(Data_Creators.unit.remove("1_3_1","r","Winston Churchill"));
        */
        
        store.dispatch(Data_Creators.state.user());
        store.dispatch(Data_Creators.tech.set("w","military",0));
        
        //TESTING BUILDERS
        let my_civ = store.getState().GAME.civs[1];     //w
        let my_civ1 = store.getState().GAME.civs[0];    //r
        let object_data = my_civ.units[0];  //THOMAS EDISON
        let object_data1 = my_civ1.units[0];    //QUEEN VICTORIA
        let object_data2 = my_civ1.units[1];    //CHANCE THE RAPPER
        let object_data_unit = my_civ.units[1]; //RAE STREMMURD
        let object_data3 = my_civ.cities[0]; //PARIS
        let object_data4 = my_civ1.buildings[0]; //Tower of London
        let object_data5 = my_civ.buildings[0]; //Durham
        let builders_data = my_civ.builders.units.soldier;
        let builders_data1 = my_civ1.builders.units.soldier;
        let builders_data2 = my_civ.builders.cities.village;
        let builders_data3 = my_civ1.builders.buildings.library;
        //console.log(object_data);
        //console.log(builders_data);
        
        //TESING UNIT MOVE
        store.dispatch(Data_Creators.unit.move(object_data,"1_4_1",{})) 
        store.dispatch(Data_Creators.unit.move(object_data2,"1_4_1",{})) 
        //TESTING CITY UPGRADE
        store.dispatch(Data_Creators.city.upgrade(object_data3, builders_data2));
        //TESTING CITY BUILD
        store.dispatch(Data_Creators.city.build("1_7_1","w","Paris", builders_data2)); 
        //TESTING CITY DEMOLISH
        store.dispatch(Data_Creators.city.demolish(object_data));
        //TESTING BUILDING UPGRADE
        store.dispatch(Data_Creators.building.upgrade(object_data5, builders_data3));
        //TESTING BUILDING BUILD
        store.dispatch(Data_Creators.building.build("2_1_1","w","Atanasoff", builders_data3));
        //TESTING BUILDING DEMOLISH
        store.dispatch(Data_Creators.building.demolish(object_data5)); 
        //TESTING UNIT UPGRADE
        store.dispatch(Data_Creators.unit.upgrade(object_data, builders_data)); 
        //TESTING UNIT BUILD
        store.dispatch(Data_Creators.unit.build("1_7_1","w","J Cole", builders_data));
        //TESTING UNIT DEMOLISH
        //not sure if build is adding to array of units
        let object_data_unit1 = my_civ.units[3]; //J Cole
        console.log("object_data"); //builder = null, level = null, stats = null
        console.log(object_data);
        store.dispatch(Data_Creators.unit.demolish(object_data));    //fix this LookupError
        //TESTING UNIT  TRANSFORM
        store.dispatch(Data_Creators.unit.transform(object_data, "San Jose", builders_data));