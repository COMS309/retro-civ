import { combineReducers } from 'redux';
import { ACTION_TYPE } from './action_type.js';

import { hexes, hex } from './sub/hex.js';
import { civs } from './sub/civ.js';
import { turn } from './sub/turn.js';
import { TECH_TREE } from './sub/tech_tree.js';

const name = (state = "game", action) => {
    switch (action.type) {
        case "SET GAME.name":
            return action.name;
        default:
            return state;
    }
}

const init = () => {
    return {
        name:   name(   undefined, {type:null}),
        turn:   turn(   undefined, {type:null}),
        hexes:  hexes(  undefined, {type:null}),
        civs:   civs(   undefined, {type:null}),
        TECH_TREE
    }
}

const state_manager = (state = init(), action) => {
    switch (action.type) {
        case ACTION_TYPE.RESET:
            return init();
        default:
            return {
                name:   name(   state.name  , action),
                turn:   turn(   state.turn  , action, state.civs? state.civs.length: 0),
                hexes:  hexes(  state.hexes , action),
                civs:   civs(   state.civs  , action),
                TECH_TREE
            }
    }
}

export const REDUCERS = {
    GAME: state_manager
};

