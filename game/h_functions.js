import { LookupError, ExistsError } from './error.js';

export const getIndex = (list, val) => {
    if(!list)
        list = [];
    let name_list = list.map((item) => item.name);
    return name_list.indexOf(val);
};

export const idLookup = (list, val, err_msg) => {
    let index = getIndex(list, val);
    if (index === -1) { throw new LookupError(err_msg + " > \"" + val + "\" not found in list " + list); }
    return list[index];
}
export const idFree = (list, val, err_msg) => {
    let index = getIndex(list, val);
    if (index !== -1) { throw new ExistsError(err_msg + " > \"" + val + "\" found in list " + list); }
    return;
}

export const isString = (obj) => {
    if (typeof(obj) === "string" || obj instanceof String) {
        return true;
    }
    return false;
}

export const flatten = (obj) => {
    if (isString(obj)) {
        return obj;
    }
    let arr = [];
    Object.keys(obj).map((item) => {
        let f = flatten(obj[item]);
        if (isString(f)) {
            arr = [
                ...arr,
                f
            ]
        }
        else {
            arr = [
                ...arr,
                ...f
            ];
        }
    })
    return arr;
}

import { expect } from 'chai';

export const expectFunction = (fn) => {
    return {
        to: {
            throw: (err) => {
                let name = "";
                try {
                    fn();
                }
                catch (e) {
                    name = e.name;
                }
                expect(name).to.equal(err.name);
            },
            not: {
                throw: (err) => {
                    let name = "";
                    try {
                        fn();
                    }
                    catch (e) {
                        name = e.name;
                    }
                    expect(name).to.not.equal(err.name);
                }
            }
        }
    }
}