import { capital }      from './cities/capital.js';
import { city }         from './cities/city.js';
import { metropolis }   from './cities/metropolis.js';
import { town }         from './cities/town.js';
import { village }      from './cities/village.js';

export const cities = {
    capital,
    city,
    metropolis,
    town,
    village
}