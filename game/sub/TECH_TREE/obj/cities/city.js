import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0: base
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 200, accuracy: 0.5 },
        defense: { block: 150, accuracy: 0.5 },
        hp: { max: 300, current: 300 },
        abilities: { mountaineering: false },
        production: 300,
        research: 300,
        population: 2.0,
        economy: 2.0,
        build_remaining: 600
    },
    //lv1: 250 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 250, accuracy: 0.5 },
        defense: { block: 175, accuracy: 0.5 },
        hp: { max: 350, current: 350 },
        abilities: { mountaineering: false },
        production: 350,
        research: 350,
        population: 2.1,
        economy: 2.1,
        build_remaining: 700
    },
    //lv2: 300 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 300, accuracy: 0.5 },
        defense: { block: 200, accuracy: 0.5 },
        hp: { max: 400, current: 400 },
        abilities: { mountaineering: false },
        production: 400,
        research: 400,
        population: 2.2,
        economy: 2.2,
        build_remaining: 800
    }
]

export const city = obj_generate("city", "city", 3, ACTION_TYPE.CITY, stats);