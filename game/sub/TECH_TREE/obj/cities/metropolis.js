import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0: base
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 250, accuracy: 0.5 },
        defense: { block: 200, accuracy: 0.5 },
        hp: { max: 400, current: 400 },
        abilities: { mountaineering: false },
        production: 400,
        research: 400,
        population: 2.5,
        economy: 2.5,
        build_remaining: 800
    },
    //lv1: 300 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 300, accuracy: 0.5 },
        defense: { block: 225, accuracy: 0.5 },
        hp: { max: 450, current: 450 },
        abilities: { mountaineering: false },
        production: 450,
        research: 450,
        population: 2.6,
        economy: 2.6,
        build_remaining: 900
    }
]

export const metropolis = obj_generate("city", "metropolis", 2, ACTION_TYPE.CITY, stats);