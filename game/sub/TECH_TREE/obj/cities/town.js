import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0: base
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 150, accuracy: 0.5 },
        defense: { block: 100, accuracy: 0.5 },
        hp: { max: 200, current: 200 },
        abilities: { mountaineering: false },
        production: 200,
        research: 200,
        population: 1.5,
        economy: 1.5,
        build_remaining: 400
    },
    //lv1: 200 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 200, accuracy: 0.5 },
        defense: { block: 125, accuracy: 0.5 },
        hp: { max: 250, current: 250 },
        abilities: { mountaineering: false },
        production: 250,
        research: 250,
        population: 1.6,
        economy: 1.6,
        build_remaining: 500
    },
    //lv2: 250 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 250, accuracy: 0.5 },
        defense: { block: 150, accuracy: 0.5 },
        hp: { max: 300, current: 300 },
        abilities: { mountaineering: false },
        production: 300,
        research: 300,
        population: 1.7,
        economy: 1.7,
        build_remaining: 600
    },
    //lv3: 300 dmg, +25 block, +50 hp, +50 prod/reser, +0.1 eco/pop, +50 resources
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 300, accuracy: 0.5 },
        defense: { block: 175, accuracy: 0.5 },
        hp: { max: 350, current: 350 },
        abilities: { mountaineering: false },
        production: 350,
        research: 350,
        population: 1.8,
        economy: 1.8,
        build_remaining: 700
    }
]

export const town = obj_generate("city", "town", 4, ACTION_TYPE.CITY, stats);