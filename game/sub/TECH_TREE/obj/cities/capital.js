import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0: base
    {
        range: 0,
        speed: 0,
        attack: { range: 2, damage: 300, accuracy: 0.5 },
        defense: { block: 250, accuracy: 0.5 },
        hp: { max: 500, current: 500 },
        abilities: { mountaineering: false },
        production: 500,
        research: 500,
        population: 3.0,
        economy: 3.0,
        build_remaining: 1000
    }
]

export const capital = obj_generate("city", "capitol", 1, ACTION_TYPE.CITY, stats);