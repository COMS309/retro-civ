import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 40, accuracy: 0.5 },
        hp: { max: 80, current: 80 },
        abilities: { mountaineering: false },
        production: 100,
        research: 0,
        population: 0,
        economy: 1.25,
        build_remaining: 400
    },
    //lv1
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 60, accuracy: 0.5 },
        hp: { max: 120, current: 120 },
        abilities: { mountaineering: false },
        production: 150,
        research: 0,
        population: 0,
        economy: 1.5,
        build_remaining: 600
    },
    //lv2
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 80, accuracy: 0.5 },
        hp: { max: 160, current: 160 },
        abilities: { mountaineering: false },
        production: 200,
        research: 0,
        population: 0,
        economy: 1.75,
        build_remaining: 800
    },
    //lv3
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 100, accuracy: 0.5 },
        hp: { max: 200, current: 200 },
        abilities: { mountaineering: false },
        production: 250,
        research: 0,
        population: 0,
        economy: 2.0,
        build_remaining: 1000
    }
]

export const treasury = obj_generate("building", "treasury", 4, ACTION_TYPE.BUILDING, stats);