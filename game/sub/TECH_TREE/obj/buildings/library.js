import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 20, accuracy: 0.5 },
        hp: { max: 40, current: 40 },
        abilities: { mountaineering: false },
        production: 0,
        research: 50,
        population: 0,
        economy: 0,
        build_remaining: 100
    },
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 40, accuracy: 0.5 },
        hp: { max: 80, current: 80 },
        abilities: { mountaineering: false },
        production: 0,
        research: 100,
        population: 0,
        economy: 0,
        build_remaining: 200
    },
    //lv2: +20 hp, +50 r/p, +100 build
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 60, accuracy: 0.5 },
        hp: { max: 120, current: 120 },
        abilities: { mountaineering: false },
        production: 0,
        research: 150,
        population: 0,
        economy: 0,
        build_remaining: 300
    },
    //lv3: +20 hp, +50 r/p, +100 build
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 80, accuracy: 0.5 },
        hp: { max: 160, current: 160 },
        abilities: { mountaineering: false },
        production: 0,
        research: 200,
        population: 0,
        economy: 0,
        build_remaining: 400
    },
    //lv4: +20 hp, +50 r/p, +100 build
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 100, accuracy: 0.5 },
        hp: { max: 200, current: 200 },
        abilities: { mountaineering: false },
        production: 0,
        research: 250,
        population: 0,
        economy: 0,
        build_remaining: 500
    }
]

export const library = obj_generate("building", "library", 5, ACTION_TYPE.BUILDING, stats);