import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 15, accuracy: 0.5 },
        hp: { max: 30, current: 30 },
        abilities: { mountaineering: false },
        production: 75,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 150
    },
    //lv1
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 30, accuracy: 0.5 },
        hp: { max: 60, current: 60 },
        abilities: { mountaineering: false },
        production: 150,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 300
    },
    //lv2
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 45, accuracy: 0.5 },
        hp: { max: 90, current: 90 },
        abilities: { mountaineering: false },
        production: 225,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 450
    },
    //lv3
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 60, accuracy: 0.5 },
        hp: { max: 120, current: 120 },
        abilities: { mountaineering: false },
        production: 300,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 600
    },
    //lv4
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 75, accuracy: 0.5 },
        hp: { max: 150, current: 150 },
        abilities: { mountaineering: false },
        production: 375,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 750
    }
]

export const mine = obj_generate("building", "mine", 5, ACTION_TYPE.BUILDING, stats);