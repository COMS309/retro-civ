import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 1, damage: 40, accuracy: 0.5 },
        defense: { block: 40, accuracy: 0.5 },
        hp: { max: 80, current: 80 },
        abilities: { mountaineering: false },
        production: 150,
        research: 0,
        population: 1.25,
        economy: 0,
        build_remaining: 400
    },
    //lv1
    {
        range: 0,
        speed: 0,
        attack: { range: 1, damage: 80, accuracy: 0.5 },
        defense: { block: 60, accuracy: 0.5 },
        hp: { max: 120, current: 120 },
        abilities: { mountaineering: false },
        production: 225,
        research: 0,
        population: 1.5,
        economy: 0,
        build_remaining: 600
    },
    //lv2
    {
        range: 0,
        speed: 0,
        attack: { range: 1, damage: 120, accuracy: 0.5 },
        defense: { block: 80, accuracy: 0.5 },
        hp: { max: 160, current: 160 },
        abilities: { mountaineering: false },
        production: 300,
        research: 0,
        population: 1.75,
        economy: 0,
        build_remaining: 800
    },
    //lv3
    {
        range: 0,
        speed: 0,
        attack: { range: 1, damage: 160, accuracy: 0.5 },
        defense: { block: 100, accuracy: 0.5 },
        hp: { max: 200, current: 200 },
        abilities: { mountaineering: false },
        production: 375,
        research: 0,
        population: 2.0,
        economy: 0,
        build_remaining: 1000
    }
]

export let barraks = obj_generate("building", "barraks", 4, ACTION_TYPE.BUILDING, stats);