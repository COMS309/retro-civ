import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 75, accuracy: 0.5 },
        hp: { max: 150, current: 150 },
        abilities: { mountaineering: false },
        production: 375,
        research: 375,
        population: 1.25,
        economy: 1.25,
        build_remaining: 1000
    }
]

export const monument = obj_generate("building", "monument", 1, ACTION_TYPE.BUILDING, stats);