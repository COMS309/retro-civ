import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 10, accuracy: 0.5 },
        hp: { max: 20, current: 20 },
        abilities: { mountaineering: false },
        production: 50,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 100
    },
    //lv1
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 20, accuracy: 0.5 },
        hp: { max: 40, current: 40 },
        abilities: { mountaineering: false },
        production: 100,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 200
    },
    //lv2
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 30, accuracy: 0.5 },
        hp: { max: 60, current: 60 },
        abilities: { mountaineering: false },
        production: 150,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 300
    },
    //lv3
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 40, accuracy: 0.5 },
        hp: { max: 80, current: 80 },
        abilities: { mountaineering: false },
        production: 200,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 400
    },
    //lv4
    {
        range: 0,
        speed: 0,
        attack: { range: 0, damage: 0, accuracy: 0.0 },
        defense: { block: 50, accuracy: 0.5 },
        hp: { max: 100, current: 100 },
        abilities: { mountaineering: false },
        production: 250,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 500
    }
]

export const farm = obj_generate("building", "farm", 5, ACTION_TYPE.BUILDING, stats);