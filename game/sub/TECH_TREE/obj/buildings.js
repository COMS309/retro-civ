import { barraks }      from './buildings/barraks.js';
import { farm }         from './buildings/farm.js';
import { library }      from './buildings/library.js';
import { mill }         from './buildings/mill.js';
import { mine }         from './buildings/mine.js';
import { monument }     from './buildings/monument.js';
import { port }         from './buildings/port.js';
import { treasury }     from './buildings/treasury.js';

export const buildings = {
    barraks,
    farm,
    library,
    mill,
    mine,
    monument,
    port,
    treasury
}