import { archer }       from './units/archer.js';
import { freelancer }   from './units/freelancer.js';
import { horseman }     from './units/horseman.js';
import { soldier }      from './units/soldier.js';
import { warlord }      from './units/warlord.js';

export const units = {
    archer,
    freelancer,
    horseman,
    soldier,
    warlord
}