import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 1,
        speed: 1,
        attack: { range: 1, damage: 20, accuracy: 0.5 },
        defense: { block: 10, accuracy: 0.5 },
        hp: { max: 30, current: 30 },
        abilities: { mountaineering: false },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 60
    },
    //lv1
    {
        range: 1,
        speed: 1,
        attack: { range: 1, damage: 30, accuracy: 0.5 },
        defense: { block: 23, accuracy: 0.5 },
        hp: { max: 45, current: 45 },
        abilities: { mountaineering: false },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 90
    },
    //lv2
    {
        range: 2,
        speed: 2,
        attack: { range: 1, damage: 40, accuracy: 0.5 },
        defense: { block: 30, accuracy: 0.5 },
        hp: { max: 60, current: 60 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 120
    },
    //lv3
    {
        range: 2,
        speed: 2,
        attack: { range: 1, damage: 50, accuracy: 0.5 },
        defense: { block: 38, accuracy: 0.5 },
        hp: { max: 75, current: 75 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 150
    },
    //lv4
    {
        range: 2,
        speed: 3,
        attack: { range: 2, damage: 60, accuracy: 0.5 },
        defense: { block: 45, accuracy: 0.5 },
        hp: { max: 90, current: 90 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 180
    }
]

export const freelancer = obj_generate("unit", "freelancer", 5, ACTION_TYPE.UNIT, stats);