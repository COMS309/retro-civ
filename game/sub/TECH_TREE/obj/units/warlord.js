import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 1,
        speed: 1,
        attack: { range: 2, damage: 200, accuracy: 0.5 },
        defense: { block: 300, accuracy: 0.25 },
        hp: { max: 300, current: 300 },
        abilities: { mountaineering: false },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 500
    }
]

export const warlord = obj_generate("unit", "warlord", 1, ACTION_TYPE.UNIT, stats);