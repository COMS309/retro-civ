import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 3,
        speed: 4,
        attack: { range: 1, damage: 75, accuracy: 0.5 },
        defense: { block: 50, accuracy: 0.5 },
        hp: { max: 100, current: 100 },
        abilities: { mountaineering: false },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 250
    },
    //lv1
    {
        range: 3,
        speed: 4,
        attack: { range: 1, damage: 90, accuracy: 0.5 },
        defense: { block: 63, accuracy: 0.5 },
        hp: { max: 125, current: 125 },
        abilities: { mountaineering: false },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 300
    }
]

export const horseman = obj_generate("unit", "horseman", 2, ACTION_TYPE.UNIT, stats);