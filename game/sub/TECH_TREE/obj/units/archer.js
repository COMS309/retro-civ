import { obj_generate } from './../../h_functions.js';
import { ACTION_TYPE } from './../../../../action_type.js';

const stats = [
    //lv0
    {
        range: 2,
        speed: 2,
        attack: { range: 3, damage: 60, accuracy: 0.5 },
        defense: { block: 30, accuracy: 0.5 },
        hp: { max: 60, current: 60 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 150
    },
    //lv1
    {
        range: 2,
        speed: 2,
        attack: { range: 3, damage: 75, accuracy: 0.5 },
        defense: { block: 38, accuracy: 0.5 },
        hp: { max: 75, current: 75 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 200
    },
    //lv2
    {
        range: 2,
        speed: 2,
        attack: { range: 3, damage: 90, accuracy: 0.5 },
        defense: { block: 45, accuracy: 0.5 },
        hp: { max: 90, current: 90 },
        abilities: { mountaineering: true },
        production: 0,
        research: 0,
        population: 0,
        economy: 0,
        build_remaining: 250
    }
]

export const archer = obj_generate("unit", "archer", 3, ACTION_TYPE.UNIT, stats);