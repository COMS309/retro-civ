import { cities } from './obj/cities.js';
import { buildings } from './obj/buildings.js'
import { units } from './obj/units.js';

export const base = {
    cities: [
        cities.village.lv0
    ],
    buildings: [
        buildings.library.lv0
    ],
    units: [
        units.soldier.lv0
    ]
}