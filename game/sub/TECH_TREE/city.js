import { level } from './h_functions.js';
import { cities } from './obj/cities.js';

const level4 = level({
    name: "King",
    description: "The most powerful man on the land needs an intimidating city to rule from." +
                 " The King is able to create capital cities that spark the envy of his rivals.",
    cost: 6000,
    cities: [
        cities.capital.lv0,
        cities.metropolis.lv1,
        cities.city.lv2,
        cities.town.lv3,
        cities.village.lv5
    ],
    buildings: [],
    units: []
});

const level3 = level({
    name: "Prince",
    description: "At the height of power the Prince is able to create grand metropolises." +
                 " These large city centers push productivity to unseen levels.",
    cost: 3000,
    cities: [
        cities.metropolis.lv0,
        cities.city.lv1,
        cities.town.lv2,
        cities.village.lv4
    ],
    buildings: [],
    units: []
});

const level2 = level({
    name: "Baron",
    description: "A booming town needs a leader to guide them. Thats where the Baron steps in." +
                 " With their leadership, the Baron is able to sustain successful cities.",
    cost: 1500,
    cities: [
        cities.city.lv0,
        cities.town.lv1,
        cities.village.lv3
    ],
    buildings: [],
    units: []
});

const level1 = level({
    name: "Minister",
    description: "A civilization needs to grow somehow. The Minister is able to make" +
                 " towns to provide support for larger communities.",
    cost: 1000,
    cities: [
        cities.town.lv0,
        cities.village.lv2
    ],
    buildings: [],
    units: []
});

const level0 = level({
    name: "Settler",
    description: "Stating somewhere is difficult, but the Settler has everything figured out." +
                 " A basic village can be built as the start of a grand civilization.",
    cost: 500,
    cities: [
        cities.village.lv1
    ],
    buildings: [],
    units: []
});

export const city = [
    level0,
    level1,
    level2,
    level3,
    level4
];