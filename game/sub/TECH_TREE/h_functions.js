//HELPER FUNCTIONS
export const level = ({name, description, cost, cities = [], buildings = [], units = []}) => {
    return {
        name,
        description,
        cost,
        cities,
        buildings,
        units
    }
}

export const obj_generate = (obj_type, obj_class, levels, obj_actions, stats) => {
    let arr = {};
    for (let i = 0; i < levels; i++) {
        Object.assign(
            arr,
            {
                ["lv" + i]: {
                    obj: obj_type,
                    builder: obj_class,
                    level: i,
                    build: obj_actions.BUILD,
                    upgrade: obj_actions.UPGRADE,
                    stats: stats[i]
                }
            }
        );
    }
    return arr;
}