import { level } from './h_functions.js';
import { buildings } from './obj/buildings.js'

const level4 = level({
    name: "Innovator",
    description: "The Innovator is a thinker unlike any other who's ingenuity brings architectural" +
                 " dominance to his land. As a testament to the kingdom, monuments can be constructed to rally" +
                 " the population.",
    cost: 6000,
    cities: [],
    buildings: [
        buildings.monument.lv0,
        buildings.port.lv2,
        buildings.treasury.lv3,
        buildings.barraks.lv3,
        buildings.mine.lv4,
        buildings.mill.lv4,
        buildings.farm.lv4,
        buildings.library.lv4
    ],
    units: []
});

const level3 = level({
    name: "Architect",
    description: "With their cunning intellect, the Architect uses their knowledge of" +
                 " building to create structures much mightier than any other in the land.",
    cost: 3000,
    cities: [],
    buildings: [
        buildings.port.lv1,
        buildings.treasury.lv2,
        buildings.barraks.lv2,
        buildings.mine.lv3,
        buildings.mill.lv3,
        buildings.farm.lv3,
        buildings.library.lv3
    ],
    units: []
});

const level2 = level({
    name: "Forger",
    description: "Bodies of water have many unclaimed resources at their disposal." +
                 " The Forger is able to create ports to tap into these unclaimed treasures.",
    cost: 1500,
    cities: [],
    buildings: [
        buildings.port.lv0,
        buildings.treasury.lv1,
        buildings.barraks.lv1,
        buildings.mine.lv2,
        buildings.mill.lv2,
        buildings.farm.lv2,
        buildings.library.lv2
    ],
    units: []
});

const level1 = level({
    name: "Mason",
    description: "A city needs a way to fund and defend itself. The Mason paves way" +
                 " for the creation of barracks and treasuries.",
    cost: 1000,
    cities: [],
    buildings: [
        buildings.treasury.lv0,
        buildings.barraks.lv0,
        buildings.mine.lv1,
        buildings.mill.lv1,
        buildings.farm.lv1,
        buildings.library.lv1
    ],
    units: []
});

const level0 = level({
    name: "Craftsman",
    description: "The Craftsman makes sure his city has all its basic needs. Using" +
                 " ,mines, farmland, and mills to collect resources, and libraries for" +
                 " extensive research, the Craftsman sets their city on the path of success.",
    cost: 500,
    cities: [],
    buildings: [
        buildings.mine.lv0,
        buildings.mill.lv0,
        buildings.farm.lv0,
    ],
    units: []
});

export const manufacturing = [
    level0,
    level1,
    level2,
    level3,
    level4
];