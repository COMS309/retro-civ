import { level } from './h_functions.js';
import { units } from './obj/units.js';

const level4 = level({
    name: "General",
    description: "As the highest rank on the battlefield, the General deserves fear and respect." +
                 " With the Warlord an army becomes nothing less than unstoppable.",
    cost: 6000,
    cities: [],
    buildings: [],
    units: [
        units.warlord.lv0,
        units.horseman.lv1,
        units.archer.lv2,
        units.soldier.lv3,
        units.freelancer.lv4
    ]
});

const level3 = level({
    name: "Colonel",
    description: "Speed is key. The Colonel can use horseback men to strike quickly on" +
                 " ill-prepared opponents.",
    cost: 3000,
    cities: [],
    buildings: [],
    units: [
        units.horseman.lv0,
        units.archer.lv1,
        units.soldier.lv2,
        units.freelancer.lv3
    ]
});

const level2 = level({
    name: "Commander",
    description: "Traditional hand on hand combat isn't the only way to win a fight. The " +
                 " Commander is able to call upon archers to clear dangers from a safe distance.",
    cost: 1500,
    cities: [],
    buildings: [],
    units: [
        units.archer.lv0,
        units.soldier.lv1,
        units.freelancer.lv2
    ]
});

const level1 = level({
    name: "Major",
    description: "The Major has a command on the battlefield, able to rally his loyal troops. " +
                 "Now well trained and equiped soldiers are at the ready.",
    cost: 1000,
    cities: [],
    buildings: [],
    units: [
        units.soldier.lv0,
        units.freelancer.lv1
    ]
});

const level0 = level({
    name: "Captain",
    description: "The lowest ranking military leader, the Captain sets the stage for " +
                 "conquest and capture of foreign lands with freelance soldiers.",
    cost: 500,
    cities: [],
    buildings: [],
    units: [
        units.freelancer.lv0
    ]
});

export const military = [
    level0,
    level1,
    level2,
    level3,
    level4
];