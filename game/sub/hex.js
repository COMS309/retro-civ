import { ACTION_TYPE } from './../action_type.js';
import { idLookup, getIndex } from './../h_functions.js';

import { city } from './city.js';
import { building } from './building.js';
import { units } from './unit.js';
import { TERRAIN } from './terrain.js';

//CREATORS
export const CREATORS = {
    add: (hex, terrain) => {
        return {
            type: ACTION_TYPE.HEX.ADD,
            hex,
            terrain
        }
    },
    remove: (hex) => {
        return {
            type: ACTION_TYPE.HEX.REMOVE,
            hex
        }
    }
}

//REDUCERS
export const hexes = (state = [], action) => {
    let hex_id = -1;
    switch (action.type) {
        case ACTION_TYPE.HEX.ADD:
            return [
                ...state,
                hex(undefined, action)
            ];
        case ACTION_TYPE.HEX.REMOVE:
            hex_id = getIndex(state, action.hex);
            return [
                ...state.slice(0, hex_id),
                ...state.slice(hex_id + 1)
            ]
        //Select appropriate hex
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.CITY.ATTACK:
        case ACTION_TYPE.CITY.HEAL:
        
        case ACTION_TYPE.BUILDING.BUILD:
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.BUILDING.DEMOLISH:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.BUILDING.HEAL:
        
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.UNIT.TRANSFORM:
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.UNIT.HEAL:
            hex_id = getIndex(state, action.hex);
            return [
                ...state.slice(0, hex_id),
                hex(state[hex_id], action),
                ...state.slice(hex_id + 1)
            ];
        case ACTION_TYPE.UNIT.MOVE:
            hex_id = getIndex(state, action.hex);
            var tmp = [
                ...state.slice(0, hex_id),
                hex(state[hex_id], action),
                ...state.slice(hex_id + 1)
            ]
			var dest_id = getIndex(tmp, action.destination);
            return [
                ...tmp.slice(0, dest_id),
                hex(tmp[dest_id], action),
                ...tmp.slice(dest_id + 1)
            ]
        default:
            return state;
    }
}
export const hex = (state = {}, action) => {
    switch (action.type) {
        case ACTION_TYPE.HEX.ADD:
            return {
                name: action.hex,
                x: parseInt(action.hex.split("_")[0]),
                y: parseInt(action.hex.split("_")[1]),
                z: parseInt(action.hex.split("_")[2]),
                city: city(undefined, action),
                building: building(undefined, action),
                units: units(undefined, action),
                terrain: action.terrain
            }
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.CITY.ATTACK:
        case ACTION_TYPE.CITY.HEAL:
            return Object.assign(
                {},
                state,
                {
                    city: city(state.city, action)
                }
            )
        case ACTION_TYPE.BUILDING.BUILD:
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.BUILDING.DEMOLISH:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.BUILDING.HEAL:
            return Object.assign(
                {},
                state,
                {
                    building: building(state.building, action)
                }
            )
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.UNIT.MOVE:
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.UNIT.HEAL:
            return Object.assign(
                {},
                state,
                {
                    units: units(state.units, action)
                }
            )
        case ACTION_TYPE.UNIT.TRANSFORM:
            return Object.assign(
                {},
                state,
                {
                    city: city(state.city, action),
                    units: units(state.units, action)
                }
            )
        default:
            return state;
    }
}


