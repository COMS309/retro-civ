import { ACTION_TYPE } from './../action_type.js';
import { idLookup, getIndex } from './../h_functions.js';

//CREATORS
export const CREATORS = {
    build: (hex, civ, building, builder_data) => {
        return {
            type: builder_data.build,
            hex,
            civ,
            building,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    upgrade: (obj_data, builder_data) => {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            building: obj_data.name,   
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    demolish: (obj_data) => {
        return {
            type: ACTION_TYPE.BUILDING.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            building: obj_data.name,
            stats: obj_data.stats
        }
    },
    attack: (attack_unit, target_building) => {
        return {
            type: ACTION_TYPE.BUILDING.ATTACK,
            hex: target_building.hex,
            civ: target_building.civ,
            building: target_building.name,
            damage: attack_unit.stats.attack.damage
        }
    },
    heal: (obj_data) => {
        return {
            type: ACTION_TYPE.BUILDING.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            building: obj_data.name,
            heal: obj_data.stats.hp.max
        }
    }
}

//REDUCERS
export const buildings = (state = [], action) => {
    let index = -1;
    switch (action.type) {
        case ACTION_TYPE.BUILDING.BUILD:
            return [
                ...state,
                building(undefined, action)
            ];
        case ACTION_TYPE.BUILDING.DEMOLISH:
            index = getIndex(state, action.building);
            return [
                ...state.slice(0, index),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.BUILDING.HEAL:
            index = getIndex(state, action.building);
            return [
                ...state.slice(0, index),
                building(state[index], action),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.BUILDING.ATTACK:
            index = getIndex(state, action.building);
            var stats = state[index].stats
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1)
                ];
            }
            return [
                ...state.slice(0, index),
                building(state[index], action),
                ...state.slice(index + 1)
            ];
        default:
            return state;
    }
}
export const building = (state = null, action) => {
    switch (action.type) {
        case ACTION_TYPE.BUILDING.BUILD:
            return {
                name: action.building,
                civ: action.civ,
                hex: action.hex,
                obj_type: 'building',
                builder: action.builder,
                level: action.level,
                stats: action.stats
            }
        case ACTION_TYPE.BUILDING.DEMOLISH:
            return null;
        case ACTION_TYPE.BUILDING.UPGRADE:
            return Object.assign(
                {},
                state,
                {
                    stats: action.stats,
                    level: action.level
                }
            )
         case ACTION_TYPE.BUILDING.ATTACK:
            if(state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0)
                return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: hp
                                }
                            )
                        }
                    )
                }
            )
        case ACTION_TYPE.BUILDING.HEAL:
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: state.stats.hp.max
                                }
                            )
                        }
                    )
                }
            )
        default:
            return state;
    }
}