import { ACTION_TYPE } from './../action_type.js';

//CREATORS
export const CREATORS = {
    next: (user) => {
        return {
            type: ACTION_TYPE.GAME.TURN.NEXT,
            civ: user
        };
    }
}

//REDUCERS
export const turn = (state = -1, action, civs) => {
    switch (action.type) {
        case ACTION_TYPE.GAME.TURN.NEXT:
            return (state < (civs - 1)) ? state + 1 : 0;
        case ACTION_TYPE.CIV.REMOVE:
            return (state < (civs - 1)) ? state : 0;
        default:
            return state;
    }
}