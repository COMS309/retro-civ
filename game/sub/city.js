import { ACTION_TYPE } from './../action_type.js';
import { idLookup, getIndex } from './../h_functions.js';

//CREATORS
export const CREATORS = {
    build: (hex, civ, city, builder_data) => {
        return {
            type: builder_data.build,
            hex,
            civ,
            city,  
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats,
        }
    },
    upgrade: (obj_data, builder_data) => {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            city: obj_data.name,
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    demolish: (obj_data) => {   
        return {
            type: ACTION_TYPE.CITY.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            city: obj_data.name,
            stats: obj_data.stats
        }
    },
    attack: (attack_unit, target_city) => {
        return {
            type: ACTION_TYPE.CITY.ATTACK,
            hex: target_city.hex,
            civ: target_city.civ,
            city: target_city.name,
            damage: attack_unit.stats.attack.damage
        }
    },
    heal: (obj_data) => {
        return {
            type: ACTION_TYPE.CITY.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            city: obj_data.name,
            heal: obj_data.stats.hp.max
        }
    }
}

//REDUCERS
export const cities = (state = [], action) => {
    let index = -1;
    switch (action.type) {
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.UNIT.TRANSFORM:
            return [
                ...state,
                city(undefined, action)
            ];
        case ACTION_TYPE.CITY.DEMOLISH:
            index = getIndex(state, action.city);
            return [
                ...state.slice(0, index),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.CITY.HEAL:
            index = getIndex(state, action.city);
            return [
                ...state.slice(0, index),
                city(state[index], action),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.CITY.ATTACK:
            index = getIndex(state, action.city);
            var stats = state[index].stats
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1)
                ];
            }
            return [
                ...state.slice(0, index),
                city(state[index], action),
                ...state.slice(index + 1)
            ];
        default:
            return state;
    }
}
export const city = (state = null, action) => {
    switch (action.type) {
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.UNIT.TRANSFORM:   
            return {
                name: action.city,
                civ: action.civ,
                hex: action.hex,
                obj_type: 'city',
                builder: action.builder,
                level: action.level,
                stats: action.stats
            }
        case ACTION_TYPE.CITY.DEMOLISH:
            return null;
        case ACTION_TYPE.CITY.UPGRADE:
            return Object.assign(
                {},
                state,
                {
                    stats: action.stats,
                    level: action.level
                }
            )
        case ACTION_TYPE.CITY.ATTACK:
            console.log('attack city');
            if(state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0)
                return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: hp
                                }
                            )
                        }
                    )
                }
            )
        case ACTION_TYPE.CITY.HEAL:
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: state.stats.hp.max
                                }
                            )
                        }
                    )
                }
            )
        default:
            return state;
    }
}