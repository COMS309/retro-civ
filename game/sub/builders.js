import { ACTION_TYPE } from './../action_type.js';
import { TECH_TREE } from './tech_tree.js';

//CREATORS
/*
build: (hex, civ) => {
    return {
        type: action.build,
        data: {
            hex: hex,
            civ: civ,
            type: data.type,
        }
    }
},
upgrade: (stat) => {
    return {
        type: action.upgrade,
        data: {
            hex: stat.hex,
            civ: stat.civ,
            type: data.type,
        }
    }
}
*/

//REDUCERS
export const builders = (state = append_builders(undefined, TECH_TREE.base), action) => {
    switch (action.type) {
        case ACTION_TYPE.TECH.SET:
            return Object.assign(
                {},
                state,
                append_builders(state, TECH_TREE[action.tech][action.level])
            );
        default:
            return state;
    }
}

export const append_builders = (state = {}, tech_level) => {
    return { 
        cities: append_set(state.cities, tech_level.cities),
        buildings: append_set(state.buildings, tech_level.buildings),
        units: append_set(state.units, tech_level.units)
    }
}

export const append_set = (state = {}, level) => {
    let set = {};
    level.map((builder) => {
        set[builder.builder] = builder;
    })
    return Object.assign(
        {},
        state,
        set
    )
}