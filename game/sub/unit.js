import { ACTION_TYPE } from './../action_type.js';
import { idLookup, getIndex } from './../h_functions.js';

//CREATORS
export const CREATORS = {
    build: (hex, civ, unit, builder_data) => {
        return {
            type: builder_data.build,
            hex,
            civ,
            unit,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    upgrade: (obj_data, builder_data) => {
        return {
            type: builder_data.upgrade,
            hex: obj_data.hex,
            civ: obj_data.civ,
            old_stats: obj_data.stats,
            unit: obj_data.name,
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    transform: (obj_data, city, builder_data) => {
        return {
            type: ACTION_TYPE.UNIT.TRANSFORM,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            old_stats: obj_data.stats,
            city,
            builder: builder_data.builder,
            level: builder_data.level,
            stats: builder_data.stats
        }
    },
    move: (obj_data, destination) => {
        return {
            type: ACTION_TYPE.UNIT.MOVE,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            destination,
            builder: obj_data.builder,
            level: obj_data.level,
            stats: obj_data.stats
        }
    },
    demolish: (obj_data) => {
        return {
            type: ACTION_TYPE.UNIT.DEMOLISH,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            stats: obj_data.stats
        }
    },
    attack: (attack_unit, target_unit) => {
        return {
            type: ACTION_TYPE.UNIT.ATTACK,
            hex: target_unit.hex,
            civ: target_unit.civ,
            unit: target_unit.name,
            damage: attack_unit.stats.attack.damage
        }
    },
    heal: (unit) => {
        return {
            type: ACTION_TYPE.UNIT.HEAL,
            hex: obj_data.hex,
            civ: obj_data.civ,
            unit: obj_data.name,
            heal: obj_data.stats.hp.max
        }
    }
}

//REDUCERS
export const units = (state = [], action) => {
    let index = -1;
    switch (action.type) {
        case ACTION_TYPE.UNIT.BUILD:
            return [
                ...state,
                unit(undefined, action)
            ];
        case ACTION_TYPE.UNIT.TRANSFORM:
        case ACTION_TYPE.UNIT.DEMOLISH:
            index = getIndex(state, action.unit);
            return [
                ...state.slice(0, index),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.UNIT.MOVE:
            index = getIndex(state, action.unit);
            if (index !== -1) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1)
                ];
            }
            return [
                ...state,
                unit(undefined, action)
            ];
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.UNIT.HEAL:
            index = getIndex(state, action.unit);
            return [
                ...state.slice(0, index),
                unit(state[index], action),
                ...state.slice(index + 1)
            ];
        case ACTION_TYPE.UNIT.ATTACK:
            index = getIndex(state, action.unit);
            var stats = state[index].stats
            if (stats.hp.current - (action.damage - stats.defense.block) <= 0) {
                return [
                    ...state.slice(0, index),
                    ...state.slice(index + 1)
                ];
            }
            return [
                ...state.slice(0, index),
                unit(state[index], action),
                ...state.slice(index + 1)
            ];
        default:
            return state;
    }
}
export const unit = (state = null, action) => {
    switch (action.type) {
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.UNIT.MOVE:
            return {
                name: action.unit,
                civ: action.civ,
                hex: action.destination || action.hex,
                obj_type: 'unit',
                builder: action.builder,
                stats: action.stats,
                level: action.level
            }
        case ACTION_TYPE.CITY.DEMOLISH:
            return null;
        case ACTION_TYPE.UNIT.UPGRADE:
            return Object.assign(
                {},
                state,
                {
                    stats: action.stats || null,
                    level: action.level
                }
            )
        case ACTION_TYPE.UNIT.ATTACK:
            if(state.stats.hp.current - (action.damage - state.stats.defense.block) <= 0)
                return null;
            var hp = state.stats.hp.current - (action.damage - state.stats.defense.block);
            var hp = hp < state.stats.hp.current ? hp : state.stats.hp.current;
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: hp
                                }
                            )
                        }
                    )
                }
            )
        case ACTION_TYPE.UNIT.HEAL:
            return Object.assign(
                {},
                state,
                {
                    stats: Object.assign(
                        {},
                        state.stats,
                        {
                            hp: Object.assign(
                                {},
                                state.stats.hp,
                                {
                                    current: state.stats.hp.max
                                }
                            )
                        }
                    )
                }
            )
        default:
            return state;
    }
}