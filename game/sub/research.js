import { ACTION_TYPE } from './../action_type.js';

const init = {
    weekly: 50,
    balance: 500,
}

//REDUCERS
export const research = (state = init, action, civs) => {
    switch (action.type) {
        case ACTION_TYPE.GAME.TURN.NEXT:
            return {
                weekly: state.weekly,
                balance: state.balance + state.weekly
            }
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.BUILDING.BUILD:
            return {
                weekly: state.weekly + Number(action.stats.research),
                balance: state.balance
            }
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.CITY.ATTACK:
            //if(state.stats.hp.current - (action.damage - state.stats.defense.block) > 0)
                return state;
            //Otherwise do the same as below
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.BUILDING.DEMOLISH:
            return {
                weekly: state.weekly - Number(action.stats.research),
                balance: state.balance
            }
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.UNIT.TRANSFORM:
            return {
                weekly: state.weekly + Number(action.stats.research) - Number(action.old_stats.research),
                balance: state.balance
            }
        case ACTION_TYPE.UNIT.HEAL:
        case ACTION_TYPE.CITY.HEAL:
        case ACTION_TYPE.BUILDING.HEAL:
            return state;
        case ACTION_TYPE.TECH.SET:
            return {
                weekly: state.weekly,
                balance: state.balance - Number(action.cost)
            }
        default:
            return state;
    }
}