
export const TERRAIN = {
    DESERT:{
        type: "desert",
        distance: 1,
        color: "yellow"
    },
    GRASSLAND:{
        type: "grassland",
        distance: 1,
        color: "lawngreen"
    },
    FOREST:{
        type: "forest",
        distance: 2,
        color: "forestgreen"
    },
    MOUNTAIN:{
        type: "mountain",
        distance: 4,
        color: "mediumorchid"
    },
    WATER:{
        type: "water",
        distance: 90,
        color: "dodgerblue"
    }
}