import { ACTION_TYPE } from './../action_type.js';
import { idLookup, getIndex } from './../h_functions.js';

import { cities } from './city.js';
import { buildings } from './building.js';
import { units } from './unit.js';
import { techs } from './tech.js';
import { builders } from './builders.js';
import { coins } from './coins.js';
import { research } from './research.js';

let index = -1;
//CREATORS
export const CREATORS = {
    add: (civ) => {
        return {
            type: ACTION_TYPE.CIV.ADD,
            production_capacity: 100,
            civ
        }
    },
    remove: (civ) => {
        return {
            type: ACTION_TYPE.CIV.REMOVE,
            civ
        }
    }
}

//REDUCERS
export const civs = (state = [], action) => {
    let civ_index = -1;
    switch (action.type) {
        case ACTION_TYPE.CIV.ADD:
            return [
                ...state,
                civ(undefined, action)
            ];
        case ACTION_TYPE.CIV.REMOVE:
            civ_index = getIndex(state, action.civ);
            return [
                ...state.slice(0, civ_index),
                ...state.slice(civ_index + 1)
            ]
        //Select appropriate hex
        case ACTION_TYPE.GAME.TURN.NEXT:
            if(!action.civ) {
                return state;
            }
            //otherwise do like the rest of the fine actions below.
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.CITY.ATTACK:
        case ACTION_TYPE.CITY.HEAL:

        case ACTION_TYPE.BUILDING.BUILD:
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.BUILDING.DEMOLISH:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.BUILDING.HEAL:

        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.UNIT.MOVE:
        case ACTION_TYPE.UNIT.TRANSFORM:
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.UNIT.HEAL:

        case ACTION_TYPE.TECH.SET:
            civ_index = getIndex(state, action.civ);
            if(civ_index==-1){
                return state;
            }
            return [
                ...state.slice(0, civ_index),
                civ(state[civ_index], action),
                ...state.slice(civ_index + 1)
            ];
        default:
            return state;
    }
}

const civ = (state = {}, action) => {
    switch (action.type) {
        case ACTION_TYPE.CIV.ADD:
            index++;
            return {
                name: action.civ,
                cities: cities(undefined, action),
                buildings: buildings(undefined, action),
                units: units(undefined, action),
                builders: builders(undefined, action),
                techs: techs(undefined, action),
                coins: coins(undefined, action),
                research: research(undefined, action),
                index: index
            }
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.CITY.ATTACK:
        case ACTION_TYPE.CITY.HEAL:
            return Object.assign(
                {},
                state,
                {
                    cities: cities(state.cities, action),
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        case ACTION_TYPE.BUILDING.BUILD:
        case ACTION_TYPE.BUILDING.UPGRADE:
        case ACTION_TYPE.BUILDING.DEMOLISH:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.BUILDING.HEAL:
            return Object.assign(
                {},
                state,
                {
                    buildings: buildings(state.buildings, action),
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.UNIT.HEAL:
            return Object.assign(
                {},
                state,
                {
                    units: units(state.units, action),
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        case ACTION_TYPE.UNIT.MOVE:
            //repeat action. once to add. once to remove
            var u = units(state.units, action);
            return Object.assign(
                {},
                state,
                { 
                    units: units(u, action)
                }
            )
        case ACTION_TYPE.UNIT.TRANSFORM:
            return Object.assign(
                {},
                state,
                {
                    cities: cities(state.cities, action),
                    units: units(state.units, action),
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        case ACTION_TYPE.TECH.SET:
            return Object.assign(
                {},
                state,
                {
                    techs: techs(state.techs, action),
                    builders: builders(state.builders, action),
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        case ACTION_TYPE.GAME.TURN.NEXT:
            return Object.assign(
                {},
                state,
                {
                    coins: coins(state.coins, action),
                    research: research(state.research, action)
                }
            )
        default:
            return state;
    }
}