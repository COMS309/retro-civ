import { ACTION_TYPE } from './../action_type.js';

const init = {
    weekly: 50,
    current: 500,
}

//REDUCERS
export const coins = (state = init, action, civs) => {
    switch (action.type) {
        case ACTION_TYPE.GAME.TURN.NEXT:
            return {
                weekly: state.weekly,
                current: state.current + state.weekly
            }
        case ACTION_TYPE.CITY.BUILD:
        case ACTION_TYPE.UNIT.BUILD:
        case ACTION_TYPE.BUILDING.BUILD:
            return {
                weekly: state.weekly + Number(action.stats.production),
                current: state.current - Number(action.stats.build_remaining)
            }
        case ACTION_TYPE.UNIT.ATTACK:
        case ACTION_TYPE.BUILDING.ATTACK:
        case ACTION_TYPE.CITY.ATTACK:
            //if(state.stats.hp.current - (action.damage - state.stats.defense.block) > 0)
                return state;
            //Otherwise do the same as below
        case ACTION_TYPE.CITY.DEMOLISH:
        case ACTION_TYPE.UNIT.DEMOLISH:
        case ACTION_TYPE.BUILDING.DEMOLISH:
            return {
                weekly: state.weekly - Number(action.stats.production),
                current: state.current
            }
        case ACTION_TYPE.CITY.UPGRADE:
        case ACTION_TYPE.UNIT.UPGRADE:
        case ACTION_TYPE.BUILDING.UPGRADE:
            return {
                weekly: state.weekly + Number(action.stats.production) - Number(action.old_stats.production),
                current: state.current + Number(action.old_stats.build_remaining) - Number(action.stats.build_remaining)
            }
        case ACTION_TYPE.UNIT.TRANSFORM:
            return {
                weekly: state.weekly + Number(action.stats.production) - Number(action.old_stats.production),
                current: state.current - Number(action.stats.build_remaining)
            }
        case ACTION_TYPE.UNIT.HEAL:
        case ACTION_TYPE.CITY.HEAL:
        case ACTION_TYPE.BUILDING.HEAL:
            return state;
        default:
            return state;
    }
}