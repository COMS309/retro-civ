import { base } from './TECH_TREE/base.js';
import { military } from './TECH_TREE/military.js';
import { city } from './TECH_TREE/city.js';
import { manufacturing } from './TECH_TREE/manufacturing.js';

//**NOTE: build cost is usually twice as much as the production or research income**
export const TECH_TREE = {
    base,
    military,
    city,
    manufacturing,
    commerce: [],
    research: [],
    colonization: []
}