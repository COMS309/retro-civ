import { ACTION_TYPE } from './../action_type.js';
import { TECH_TREE } from './tech_tree.js';

//CREATORS
export const CREATORS = {
    set: (civ, tech, level, cost) => {
        return {
            type: ACTION_TYPE.TECH.SET,
            civ,
            tech,
            level,
            cost
        }
    }
}

//REDUCERS
export const tech_tree = (state, action) => {
    return TECH_TREE;
}

const tech_init = {
    military: -1,
    manufacturing: -1,
    commerce: -1,
    research: -1,
    city: -1,
    colonization: -1
}

export const techs = (state = tech_init, action) => {
    switch (action.type) {
        case ACTION_TYPE.TECH.SET:
            return Object.assign(
                {},
                state,
                //NOTE: this should be replaced with something similar to how hexes lookup the correct hex and civs lookup the correct civ.
                //That would get rid of the tech_init part as another reducer would set each category to 0.  This should work for now though.
                {
                    [action.tech]: action.level
                }
            );
        default:
            return state;
    }
}
