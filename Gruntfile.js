// Generated on 2015-07-15 using generator-angular-fullstack 2.1.0
'use strict';

module.exports = function (grunt) {
  var localConfig = {};
  //var os = require('os');
  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server',
  });

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);
  // Load End Of Line
  grunt.loadNpmTasks('grunt-eol');
  // Load typescript compiler
  grunt.loadNpmTasks('grunt-ts');
  // Define the configuration for all the tasks
  grunt.initConfig({
    // Project settings
    pkg: grunt.file.readJSON('package.json'),
    path_config: {
      // configurable paths
      client: require('./bower.json').appPath || 'client',
      server: 'server',
      dist: 'dist'
    },
    express: {
      options: {
        port: 80//process.env.PORT || 9000
      },
      dev: {
        options: {
          script: '.tmp/server/node_app.ts/node_app.js',
          debug: true
        }
      },
      prod: {
        options: {
          script: 'dist/server/node_app.ts/node_app.js'
        }
      }
    },
    open: {
      server: {
        url: 'http://localhost:<%= express.options.port %>'
      }
    },
    watch: {
      index: {
        files: [
          '<%= path_config.client %>/*.html'
        ],
        tasks: ['copy:index']
      },
      templates: {
        files: [
          '<%= path_config.client %>/app/**/*.html',
          '<%= path_config.client %>/templates/**/*.html'
        ],
        tasks: ['copy:templates']
      },
      injectJS: {
        files: [
          '<%= path_config.client %>/app/**/*.js',
          '!<%= path_config.client %>/app/**/*.spec.js',
          '!<%= path_config.client %>/app/**/*.mock.js',
          '!<%= path_config.client %>/app/app.js'],
        tasks: ['injector:scripts','copy:scripts','copy:index']
      },
      injectAssets: {
        files: [
          '<%= path_config.client %>/assets/vendor/**/*.js',
        ],
        tasks: ['injector:assets']
      },
      injectCss: {
        files: [
          '<%= path_config.client %>/{app,assets}/**/*.css'
        ],
        tasks: ['injector:css','copy:styles','copy:index']
      },
      injectSass: {
        files: [
          '<%= path_config.client %>/app/**/*.{scss,sass}'],
        tasks: ['injector:sass','copy:styles','copy:index']
      },
      sass: {
        files: [
          '<%= path_config.client %>/app/**/*.{scss,sass}'],
        tasks: ['sass', 'autoprefixer']
      },
      coffee: {
        files: [
          '<%= path_config.client %>/app/**/*.{coffee,litcoffee,coffee.md}',
          '!<%= path_config.client %>/app/**/*.spec.{coffee,litcoffee,coffee.md}'
        ],
        tasks: ['newer:coffee', 'injector:scripts','copy:scripts']
      },
      ts: {
        files: [
          '<%= path_config.client %>/app/**/*.ts',
          '!<%= path_config.client %>/app/**/*.spec.ts'
        ],
        tasks: ['newer:ts', 'injector:scripts', 'copy:scripts']
      },
      coffeeServer: {
        files: [
          '<%= path_config.server %>/**/*.{coffee,litcoffee,coffee.md}',
          '!<%= path_config.server %>/**/*.spec.{coffee,litcoffee,coffee.md}'
        ],
        tasks: ['newer:coffee']
      },
      tsServer: {
        files: [
          '<%= path_config.client %>/app/**/*.ts',
          '!<%= path_config.client %>/app/**/*.spec.ts'
        ],
        tasks: ['newer:ts']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        files: [
          '{.tmp,<%= path_config.client %>}/{app,assets}/**/*.css',
          '{.tmp,<%= path_config.client %>}/app/**/*.html',
          
          '{.tmp,<%= path_config.client %>}/{app,assets}/**/*.js',
          
          '!{.tmp,<%= path_config.client %>}/app/**/*.spec.js',
          '!{.tmp,<%= path_config.client %>}/app/**/*.mock.js',
          '<%= path_config.client %>/assets/img/{,*//*}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: true
        }
      },
      express: {
        files: [
          '.tmp/server/**/*.{js,json}',
          '.tmp/python/**/*'
        ],
        tasks: ['express:dev', 'wait'],
        options: {
          livereload: true,
          nospawn: true //Without this option specified express won't be reloaded
        }
      }
    },
    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= path_config.dist %>/*',
            '!<%= path_config.dist %>/.git*',
            '!<%= path_config.dist %>/.openshift',
            '!<%= path_config.dist %>/Procfile'
          ]
        }]
      },
      server: '.tmp'
    },
    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/',
          src: '{,*/}*.css',
          dest: '.tmp/public/'
        }]
      }
    },
    // Debugging with node inspector
    'node-inspector': {
      custom: {
        options: {
          'web-host': 'localhost'
        }
      }
    },
    nodemon: {
      debug: {
        script: '.tmp/server/node_app.ts/node_app.js',
        options: {
          nodeArgs: ['--debug-brk'],
          env: {
            PORT: process.env.PORT || 9000
          },
          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            // opens browser on initial server start
            nodemon.on('config:update', function () {
              setTimeout(function () {
                require('open')('http://localhost:8080/debug?port=5858');
              }, 500);
            });
          }
        }
      }
    },
    // Automatically inject Bower components into the app
    wiredep: {
      target: {
        src: [
          '<%= path_config.client %>/index.html',
          '<%= path_config.client %>/icons.html'
        ],
        ignorePath: '<%= path_config.client %>/',
        exclude: [/bootstrap-sass-official/, /bootstrap.js/, '/json3/', '/es5-shim/', /bootstrap.css/, /font-awesome.css/ ]
      }
    },
    // Copies remaining files to places other tasks can use
    copy: {
      templates: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= path_config.client %>',
          dest: '.tmp/public',
          src: [
            'templates/**/*',
            'app/**/*.html',  
          ]
        }]
      },
      index: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= path_config.client %>',
          dest: '.tmp/public',
          src: [
            'index.html',
            'icons.html',
            'popout.html'
          ]
        }]
      },
      styles: {
        files: [{
          expand: true,
          cwd: '<%= path_config.client %>',
          dest: '.tmp/public',
          src: [
            'app/**/*.css',
            'assets/css/**/*'
          ]
        }]
      },
      scripts: {
        files: [{
          expand: true,
          cwd: '.tmp',
          dest: '.tmp/public',
          src: ['app/**/*.js']
        }]
      },
      prod: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= path_config.client %>',
          dest: '.tmp/public',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'bower_components/**/*',
            'assets/img/**/*',
            'assets/fonts/**/*',
            'assets/vendor/**/*'
          ]
        },
        {
          expand: true,
          dest: '.tmp',
          src: [
            'package.json'
          ]
        }]
      },
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= path_config.client %>',
          dest: '<%= path_config.dist %>/public',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'bower_components/**/*',
            'assets/img/**/*',
            'assets/fonts/**/*',
            'assets/css/**/*',
            'assets/vendor/**/*',
            'templates/**/*',
            'app/**/*.html',
            'index.html',
            'icons.html'
          ]
        }, {
          expand: true,
          cwd: '.tmp/assets/img',
          dest: '<%= path_config.dist %>/public/assets/img',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: '.tmp/server',
          dest: '<%= path_config.dist %>/server',
          src: '**/*'
        }, {
          expand: true,
          dest: '<%= path_config.dist %>',
          src: [
            'package.json'
          ]
        }]
      },
    },
    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'coffee',
        'ts',
        'sass',
      ],
      dist: [
        'coffee',
        'ts',
        'sass',
      ]
    },
    env: {
      test: {
        NODE_ENV: 'test'
      },
      prod: {
        NODE_ENV: 'production'
      },
      all: localConfig
    },
    // Compiles Typescript to JoavaScript
    ts: {
      options: {
        fast: 'never'
      },
      light: {
        files: [{
          expand: true,
          cwd: 'server',
          src: ['**/*.ts',
                '!**/*.spec.ts'],
          dest: '.tmp/server',  
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: 'client',
          src: [
            'app/**/*.ts',
            '!app/**/*.spec.ts'
          ],
          dest: '.tmp',
        },
        {
          expand: true,
          cwd: 'server',
          src: [
            '**/*.ts',
            '!**/*.spec.ts'
          ],
          dest: '.tmp/server',
        }]
      }
    },
    // Compiles CoffeeScript to JavaScript
    coffee: {
      options: {
        sourceMap: true,
        sourceRoot: ''
      },
      light: {
        files: [{
          expand: true,
          cwd: 'server',
          src: [
            '**/*.coffee',
            '!**/*.spec.coffee'
          ],
          dest: '.tmp/server',
          ext: '.js'
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: 'client',
          src: [
            'app/**/*.coffee',
            '!app/**/*.spec.coffee'
          ],
          dest: '.tmp',
          ext: '.js'
        },
        {
          expand: true,
          cwd: 'server',
          src: [
            '**/*.coffee',
            '!**/*.spec.coffee'
          ],
          dest: '.tmp/server',
          ext: '.js'
        }]
      }
    },
    // Compiles Sass to CSS
    sass: {
      server: {
        options: {
          loadPath: [
            '<%= path_config.client %>/bower_components',
            '<%= path_config.client %>/app',
            '<%= path_config.client %>/components'
          ],
          compass: false
        },
        files: {
          '.tmp/public/app/app.css' : '<%= path_config.client %>/app/app.scss'
        }
      }
    },

    injector: {
      options: {
        lineEnding: '\n',
      },
      // Inject application script files into index.html (doesn't include bower)
      scripts: {
        options: {
          transform: function(filePath) {
            console.log(filePath);
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:js -->',
          endtag: '<!-- endjsinjector -->'
        },
        files: [{
          '<%= path_config.client %>/index.html' : [                 
                 '{.tmp,<%= path_config.client %>}/app/**/*.js',
                 '!{.tmp,<%= path_config.client %>}/app/app.js',               
                 '!{.tmp,<%= path_config.client %>}/app/**/*.spec.js',
                 '!{.tmp,<%= path_config.client %>}/app/**/*.mock.js'               
               ]
        }, {
            '<%= path_config.client %>/icons.html' : [                 
                 '{.tmp,<%= path_config.client %>}/app/**/*.js',
                 '!{.tmp,<%= path_config.client %>}/app/app.js',               
                 '!{.tmp,<%= path_config.client %>}/app/**/*.spec.js',
                 '!{.tmp,<%= path_config.client %>}/app/**/*.mock.js'               
               ]
        }]
      },
      // Inject asset script files into index.html (doesn't include bower or other asset scripts)
      assets: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:assets -->',
          endtag: '<!-- endassetsinjector -->'
        },
        files: [{
          '<%= path_config.client %>/index.html' : [
                 '{.tmp,<%= path_config.client %>}/assets/vendor/**/*.js',
          ]
        }, {
          
           '<%= path_config.client %>/icons.html' : [
                 '{.tmp,<%= path_config.client %>}/assets/vendor/**/*.js',
           ] 
        }]
      },
      // Inject component scss into app.scss
      sass: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/app/', '');
            filePath = filePath.replace('/client/components/', '');
            return '@import \'' + filePath + '\';';
          },
          starttag: '// injector',
          endtag: '// endinjector'
        },
        files: {
          '<%= path_config.client %>/app/app.scss': [
            '<%= path_config.client %>/{app,components}/**/*.{scss,sass}',
            '!<%= path_config.client %>/app/app.{scss,sass}'
          ]
        }
      },

      // Inject component css into index.html
      css: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<link rel="stylesheet" href="' + filePath + '">';
          },
          starttag: '<!-- injector:css -->',
          endtag: '<!-- endinjector -->'
        },
        files: [{
          '<%= path_config.client %>/index.html' : [
            '<%= path_config.client %>/{app,components}/**/*.css'
          ]
        }, {
           '<%= path_config.client %>/icons.html' : [
            '<%= path_config.client %>/{app,components}/**/*.css'
           ]
        }]
      }
    },
    eol: {
      index: {
        options: {
          eol: 'lf',
          replace: true
        },
        files: {
          src: ['<%= path_config.client %>/index.html',
           '<%= path_config.client %>/icons.html'
          ]  
        }
      }
    }
  });
  // Used for delaying livereload until after server has restarted
  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
    this.async();
  });
  grunt.registerTask('serve', function (target) {
    if (target === 'dist') {
      return grunt.task.run([
        'build',
        'env:all',
        'env:prod',
        'express:prod',
        'wait',
        'open',
        'express-keepalive']);
    }
    if (target === 'light') {
      return grunt.task.run([
        'clean:server',
        'env:all',
        'coffee:light',
        'ts:light',
        'express:dev',
        'wait',
        'express-keepalive'
      ]);
    }
    grunt.task.run([
      'clean:server',
      'env:all',
      'injector:sass', 
      'concurrent:server',
      'copy:templates',
      'copy:styles',
      'copy:scripts',
      'injector',
      'wiredep',
      'eol:index',
      'autoprefixer',
      'copy:index',
      'copy:prod',
      'express:dev',
      'wait',
      'open',
      'watch'
    ]);
  });

  grunt.registerTask('server', function () {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve']);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'injector:sass', 
    'concurrent:dist',
    'injector',
    'wiredep',
    'eol:index',
    'autoprefixer',
    'copy:dist',
  ]);

  grunt.registerTask('default', [
    'build'
  ]);
};
